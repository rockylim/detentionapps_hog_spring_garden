module(..., package.seeall);

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

local rand = math.random;
local randSeed = math.randomseed;
local randTime = os.time;
rand(); rand(); rand();

local SCORE_PER_ITEM;

local group;
local optionsGroup;

local listHUDImages;
local optionsMenu;

local listItemsToFind;

local disableInput;

local numHints;
local hintsImage;

local numScore;
local scoreImage;

local levelTime;
local timeImage;

local subMenuOpen;

local handleClockTimer;

local listZoom;

local openOptions = nil;
local closeOptions = nil;
local handleOptionsHit = nil;
local handleQuitHit = nil;
local itemTouched = nil;
local handleHintHit = nil;
local handleHintUsed = nil;
local checkItemTouched = nil;
local isCoronaSim = false;

local function chooseFrame(gameType)
  listHUDImages["HUD_frame1"].isVisible = false;
  listHUDImages["HUD_frame2"].isVisible = false;
  listHUDImages["HUD_frame3"].isVisible = false;
  
  if (gameType == "picture") then
    listHUDImages["HUD_frame3"].isVisible = true;
  else
    listHUDImages["HUD_frame1"].isVisible = true;
  end
end

local function HUDItemTouched( event )
  if (event.phase == "began") then    
  elseif (event.phase == "moved") then     
  elseif (event.phase == "ended") then
    listZoom.show(event.target.filename, event.target.width, event.target.height);
  end
  return true;
end


local function initPictureList(listItems)
  local xMod, yMod = 0,0;
  
  local initialX = (_W * 0.24);
  local xSpacing = (_W * 0.058);
  local initialY = (_H * 0.945);
  local ySpacing = (_H);
  
  local counter = 0;
  local inSlot = true;
  
  for i,v in pairs(listItems) do
    local image = display.newImage(v.filename);
    image.name = v.name;
    image.filename = v.filename;

    local ratio;
    
    if (image.width > image.height) then
      ratio = (50 / image.width);
    else
      ratio = (50 / image.height);
    end
    
    image.xScale = ratio;
    image.yScale = ratio;
    
    image.inSlot = inSlot;
    v.inSlot = inSlot;
    image.x = (initialX + (xMod * xSpacing));
    image.y = (initialY + (yMod * ySpacing));
    xMod = (xMod + 1);
    
    if (xMod == 10) then
      xMod = 0;
      yMod = 1;
      inSlot = false;
    end
    
    counter = (counter + 1);
    group:insert(image);
    listItemsToFind[v.dispatch] = image;
	--Runtime:addEventListener(v.dispatch, itemTouched);
    image:addEventListener("touch", HUDItemTouched);      
  end
  
--  for i = 1, #listItemsToFind do
--    group:insert(listItemsToFind[i]);
--  end
end

local function scrambleWord(str)
  local listIndex = {};
  for i = 1, #str do
    listIndex[i] = false;
  end
  
  local isDone = false;
  local newStr = "";
  
  while (isDone == false) do
    local index = rand(1,#str);
    
    if (listIndex[index] == false) then
      listIndex[index] = true;
      newStr = (newStr..string.sub(str,index,index));
      
      local numUsed = 0;
      
      for i = 1, #listIndex do
        if (listIndex[i] == true) then
          numUsed = (numUsed + 1);
        end
      end
      
      if (numUsed == #listIndex) then
        isDone = true;
      end
    end
  end
  
  
  return newStr;
end

local function divideWords(str)
  local listWords = {};
  
  local currentWord = "";
  table.insert(listWords,currentWord);
  
  for i = 1, #str do
    local letter = string.sub(str,i,i);
    
    if (letter ~= " ") then
      listWords[#listWords] = (listWords[#listWords] .. letter);
    elseif ((#listWords[#listWords] > 0)) then
      local newWord = "";
      table.insert(listWords, newWord);
      
    end
  end
  
  local newStr = "";
  
  for i = 1, #listWords do
    newStr = (newStr..scrambleWord(listWords[i]));
    
    if (i ~= #listWords) then
      newStr = (newStr.." ");
    end
    
  end
  
  return newStr;
end

local function hideRandomLetters(str)
  local numLetters = #str;
  local numHidden;
  
  if ((numLetters >= 3) and (numLetters <= 4)) then
    numHidden = 1;
  elseif ((numLetters >= 5) and (numLetters <= 7)) then
    numHidden = 2;
  elseif ((numLetters >= 8) and (numLetters <= 10)) then
    numHidden = 3;
  else
    numHidden = 4;
  end
  
  local isDone = false;
  
  local newStr = "";
  
  while (isDone == false) do
    local randIndex = rand(1, #str);
    
    local letter = string.sub(str, randIndex, randIndex);
    if ((letter ~= " ") and (letter ~= "-")) then
      numHidden = (numHidden - 1);
      newStr = "";
      for i = 1, #str do
        if (i == randIndex) then
          newStr = (newStr.."-");
        else
          newStr = (newStr .. string.sub(str,i,i));
        end
        
      end
      
      str = newStr;
      
      if (numHidden == 0) then
        isDone = true;
      end
      
    end
    
  end
  
  return newStr;
end

local function initTextList(listItems, modeChosen)
  local xMod, yMod = 0,0;
  
--  local initialX = (_W * 0.172);
--  local xSpacing = (_W * 0.1679);
--  local initialY = (_H * 0.865);
--  local ySpacing = (_H * 0.041);
  local initialX = (_W * 0.28);
  local xSpacing = (_W * 0.148);
  local initialY = (_H * 0.903);
  local ySpacing = (_H * 0.038);
  
  local factory = require("utility.factory");
  
  for i,v in pairs(listItems) do
    local strItem = factory.newText(v.title, 16, "Arial");
    strItem:setTextColor(0,0,0);
--    strItem:setTextColor(240/255,218/255,102/255);
    strItem.inSlot = true;
    
    local xPos = (initialX + (xMod * xSpacing));
    
    if (modeChosen == "adventure") then        
      if (xPos == initialX) then
        strItem.text = divideWords(strItem.text);
        --strItem.text = scrambleWord(strItem.text);
      elseif (xPos == (initialX + xSpacing)) then
        strItem.text = hideRandomLetters(strItem.text);
      elseif (xPos == (initialX + (xSpacing * 3))) then
        strItem:setTextColor(0, 100/255, 0);
        strItem.isBonus = true;
      end
    end
    
    strItem.x = xPos;
    
    strItem.y = (initialY + (yMod * ySpacing));
    
    xMod = (xMod + 1);
    
    if (xMod == 4) then
      xMod = 0;
      yMod = (yMod + 1);
    end
    
    group:insert(strItem);
    listItemsToFind[v.dispatch] = strItem;
	--Runtime:addEventListener(v.dispatch, itemTouched);
    --Runtime:addEventListener("touch", itemTouched);	
  end
end


local function setHints()
  if (hintsImage) then display.remove(hintsImage); end
  if (numHints < 0) then return; end
  
  hintsImage = display.newImage("assets/images/numbers/score_" .. numHints..".png");
  hintsImage.width = 61;
	hintsImage.height = 69;
  hintsImage.xScale = (0.5);  hintsImage.yScale = (0.5);
  hintsImage.x = (_W * 0.78);
  hintsImage.y = (_H * 0.838);
  group:insert(hintsImage);
end

local function setTime()
  --levelTime
  if (timeImage) then display.remove(timeImage); end
  
  local factory = require("utility.factory");
  timeImage = factory.forgeDigitsForCountdownTimer("assets/images/numbers/score_", levelTime, 27, 0.45,0.45);
  --timeImage:setReferencePoint(display.CenterRightReferencePoint);
  timeImage.anchorChildren = true;
  timeImage.anchorX = 1;
  timeImage.anchorY = 0.5;
  
  timeImage.x = (_W * 0.35);
  timeImage.y = (_H * 0.838);
  
  group:insert(timeImage);
  
  
  
end

local function setScore()
  if (scoreImage) then display.remove(scoreImage); end
  
  local factory = require("utility.factory");
  scoreImage = factory.forgeDigitsFromString("assets/images/numbers/score_", numScore, 25, 0.4, 0.4);

--  scoreImage:setReferencePoint(display.CenterRightReferencePoint);

--  scoreImage.x = (_W * 0.60);

--  scoreImage.y = (_H * 0.031);
  --scoreImage:setReferencePoint(display.CenterRightReferencePoint);
  scoreImage.anchorChildren = true;
  scoreImage.anchorX = 1;
  scoreImage.anchorY = 0.5;
  
  scoreImage.x = (_W * 0.58);
  scoreImage.y = (_H * 0.838);
  
  group:insert(scoreImage);
end

function initialize(parent, gameType, modeChosen, listItems, listZoomParam)
  listZoom = listZoomParam;
  isCoronaSim = system.getInfo("environment") == "simulator";
  group = display.newGroup();
  optionsGroup = display.newGroup();
  group:insert(optionsGroup);
  parent:insert(group);
  
  if (modeChosen ~= "adventure") then
    if (gameType == "picture") then
      SCORE_PER_ITEM = 60;
    elseif (gameType == "word") then
      SCORE_PER_ITEM = 80;
    elseif (gameType == "collector") then
      SCORE_PER_ITEM = 100;
    end
  end
  
  --check modes and assign properly when know stuff
  if (modeChosen == "traditional") then
    numHints = 4;
    levelTime = 240000;
  elseif (modeChosen == "adventure") then
    numHints = 3;
    levelTime = 300000;
    SCORE_PER_ITEM = 200;
  else
    numHints = -1;
    levelTime = -1;
    
  end
  
  numScore = 0;
  
  local assetManager = require("utility.assetManager");
  listHUDImages = assetManager.getAssets("gameplay/HUD", group);
  optionsMenu = assetManager.getAssets("gameplay/HUD_options", optionsGroup);
  
  print("MUTES",_G.SOUND_MUTED,_G.MUSIC_MUTED);
  
  if (_G.MUSIC_MUTED == true) then
    print("MUTING MUSIC");
    print(optionsMenu["button_music"].toggleOff.isVisible);
    print(optionsMenu["button_music"].toggleOn.isVisible);
    
    optionsMenu["button_music"].toggleOff.isVisible = true;
    optionsMenu["button_music"].toggleOn.isVisible = false;
    print(optionsMenu["button_music"].toggleOff.isVisible);
    print(optionsMenu["button_music"].toggleOn.isVisible);
  end
  
  if (_G.SOUND_MUTED == true) then
    print("MUTING SOUND");
    print(optionsMenu["button_sound"].toggleOff.isVisible);
    print(optionsMenu["button_sound"].toggleOn.isVisible);
    
    optionsMenu["button_sound"].toggleOff.isVisible = true;
    optionsMenu["button_sound"].toggleOn.isVisible = false;
    
    print(optionsMenu["button_sound"].toggleOff.isVisible);
    print(optionsMenu["button_sound"].toggleOn.isVisible);
    
    
  end
  
--  timer.performWithDelay(500, function()
--    print("TIMER SOUND");
--    print(optionsMenu["button_sound"].toggleOff.isVisible);
--    print(optionsMenu["button_sound"].toggleOn.isVisible);
--    print("TIMER MUSIC");
--    print(optionsMenu["button_music"].toggleOff.isVisible);
--    print(optionsMenu["button_music"].toggleOn.isVisible);
--  end);

  chooseFrame(gameType);
  
  disableInput = false;
  subMenuOpen = false;
  
  --optionsGroup:setReferencePoint(display.CenterReferencePoint);
  optionsGroup.anchorChildren = true;
  optionsGroup.anchorX = 0.5;
  optionsGroup.anchorY = 0.5;
  
  optionsGroup.x = (_W * 0.86);
  optionsGroup.y = (_H * -0.3);
  
  Runtime:addEventListener("options_hit", handleOptionsHit);
  Runtime:addEventListener("quit_hit", handleQuitHit);
  Runtime:addEventListener("hint_hit", handleHintHit);
  Runtime:addEventListener("hint_used", handleHintUsed);
  Runtime:addEventListener("checkTouchedItem", checkItemTouched);
  
  listItemsToFind = {};
  
  if (gameType == "picture") then
    initPictureList(listItems);
  else
    initTextList(listItems, modeChosen);
  end
  
  if (levelTime > 0) then
    setTime();
  end    
  
  if (numHints > 0) then
    setHints();
  end  
  
  setScore();
  
  if (levelTime > 0) then
      
    handleClockTimer = timer.performWithDelay(1000, function()
      levelTime = (levelTime - 1000);
      
      if (levelTime >= 0) then
        setTime();
      else
        timer.cancel(handleClockTimer);
        Runtime:dispatchEvent({name = "end_game", isWin = false, score = numScore});
      end
      
    end, 0);
  end
  
end

checkItemTouched = function(e)
  --if (listItemsToFind[e.target.dispatch].inSlot == false) then return; end
  
  Runtime:dispatchEvent({name = "collectItem", target = e.target});
  
  --Runtime:removeEventListener(e.name, itemTouched);
  
  local mod = 1;
      
  if (listItemsToFind[e.target.dispatch].isBonus == true) then
    mod = 2;
    Runtime:dispatchEvent({name = "bonus_item_sound"});
  else
    Runtime:dispatchEvent({name = "item_sound"});
  end
  
  numScore = (numScore + (SCORE_PER_ITEM * mod));
  setScore();
      
  local xPos, yPos = listItemsToFind[e.target.dispatch].x, listItemsToFind[e.target.dispatch].y;
  display.remove(listItemsToFind[e.target.dispatch]);
  listItemsToFind[e.target.dispatch].found = true;
  
  local wasInSlot = listItemsToFind[e.target.dispatch].inSlot;
  listItemsToFind[e.target.dispatch].inSlot = true;
  
  
  local counter = 0;
  local totalItems = 0;
  
  for i,v in pairs(listItemsToFind) do
    if (v.found == true) then
      counter = (counter + 1);
    end
    
    totalItems = (totalItems + 1);
  end
  
  if (counter == totalItems) then
    if (handleClockTimer) then
      timer.cancel(handleClockTimer);
      handleClockTimer = nil;
    end
    Runtime:dispatchEvent({name = "end_game", isWin = true, score = numScore});
  end 
  
--  if (listItemsToFind[e.target.dispatch].inSlot == false) then
--    listItemsToFind[e.name].inSlot = true;
--    return false;
--  end
  
  for i,v in pairs(listItemsToFind) do
    if ((v.inSlot == false) and (v.found ~= true) and (wasInSlot == true)) then
      print(v.name .. " placed in slot");
      v.x = xPos;
      v.y = yPos;
      v.inSlot = true;      
      return false;
    end
  end
end

itemTouched = function(e)   
end

handleHintUsed = function(e)  
  if (isCoronaSim == true) then  
    numHints = (numHints + 1);  
  end
  numHints = (numHints - 1);  
  setHints();
end

handleHintHit = function(e)
  Runtime:dispatchEvent({name = "button_sound"});
  if ((numHints == 0)) then return; end
  
  Runtime:dispatchEvent({name = "play_hint"});
end

openOptions = function()
  disableInput = true;
  transition.to(optionsGroup, {time = 200, y = _H * -0.05, onComplete = function()
    disableInput = false;
    subMenuOpen = true;
  end});
end

closeOptions = function()
  disableInput = true;
  transition.to(optionsGroup, {time = 200, y = _H * -0.3, onComplete = function()
    disableInput = false;
    subMenuOpen = false;
  end});
end

handleOptionsHit = function(e)
  Runtime:dispatchEvent({name = "button_sound"});
  if (disableInput == true) then return; end
  
  if (subMenuOpen == true) then
    closeOptions();
  else
    openOptions();
  end
end

handleQuitHit = function(e)
  Runtime:dispatchEvent({name = "button_sound"});
  changeScene("scene_menu");
end

function destroy()
  Runtime:removeEventListener("options_hit", handleOptionsHit);
  Runtime:removeEventListener("quit_hit", handleQuitHit);
  Runtime:removeEventListener("hint_hit", handleHintHit);
  Runtime:removeEventListener("hint_used", handleHintUsed);
  Runtime:removeEventListener("checkTouchedItem", checkItemTouched);
  
  if (handleClockTimer) then
    timer.cancel(handleClockTimer);
  end
  
  --[[for i,v in pairs(listItemsToFind) do
    if (v.found ~= true) then
      Runtime:removeEventListener("touch", itemTouched);
    end    
  end]]--
end