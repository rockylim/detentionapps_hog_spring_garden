module(..., package.seeall);

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);

------ ****** ------

local POPUP_ADS;
local activeIndex = 1;

local thumb;
local thumbPlace;
local title;
local icon;
local isShow = false;
local popupAdsGroup;

if (system.getInfo("platform") == "ios") then
	POPUP_ADS = 
	{
  		
		{
			IMG = "assets/images/popupads/th_photo_journey.jpg", 
			URL = "https://apps.apple.com/us/app/hidden-objects-photo-journey/id1529208632",
			IMG_ICON = "assets/images/popupads/ic_photo_journey.png", 
			TEXT = "Hidden Objects Photo Journey",
		}, 
		{
			IMG = "assets/images/popupads/th_hog_xmas.jpg", 
			URL = "https://apps.apple.com/us/app/hidden-object-christmas-puzzle/id1321575063",
			IMG_ICON = "assets/images/popupads/ic_ho_christmas.png", 
			TEXT = "Hidden Object Christmas",
		},
  		
	}
else
	if (_G.isAmazon == true) then
		POPUP_ADS = 
		{  				
			{
				IMG = "assets/images/popupads/th_hog_haloween.png",
				URL = "https://www.amazon.com/Objects-Halloween-Mystery-Haunted-Differences/dp/B075THP1MW/",
				IMG_ICON = "assets/images/popupads/ic_ho_halloween.png", 
				TEXT = "Hidden Objects Halloween",
			}, 
			{
				IMG = "assets/images/popupads/th_hog_xmas.png",
				URL = "https://www.amazon.com/Hidden-Object-Christmas-Celebration-Holiday/dp/B07856KTQF",
				IMG_ICON = "assets/images/popupads/ic_ho_christmas.png", 
				TEXT = "Hidden Objects Christmas",
			},
			{
				IMG = "assets/images/popupads/th_hog_ny.png",
				URL = "https://www.amazon.com/Hidden-Object-New-York-City/dp/B076GYCBYS/",
				IMG_ICON = "assets/images/popupads/ic_ho_ny.png", 
				TEXT = "Hidden Objects New York",
			},
			{
				IMG = "assets/images/popupads/th_photo_journey.png",
				URL = "https://www.amazon.com/Hidden-Object-Road-Trip-USA/dp/B078VHB24T/",
				IMG_ICON = "assets/images/popupads/ic_hog_roadtrip.png", 
				TEXT = "Hidden Object Road Trip USA",
			}
		}
	else
		POPUP_ADS = 
		{  				
		}
	end	
end
 

local function AddIndex()
	activeIndex = activeIndex + 1;
	if (activeIndex > #POPUP_ADS) then
		activeIndex = 1;
	end;
end;



local function InitContent(panelGroup) 
	thumb = display.newImageRect(panelGroup, POPUP_ADS[activeIndex].IMG, 350, 237);	
	thumb.y = -110;
	
	icon = display.newImageRect(panelGroup, POPUP_ADS[activeIndex].IMG_ICON, 120, 120);	
	icon.x =  -100;
	icon.y = 90;
	
	
	local options = 
	{
    	text = POPUP_ADS[activeIndex].TEXT,     
	    x = 100,
	    y = 200,
	    width = 240,
	    font = native.systemFont,   
	    fontSize = 28,
	}
	title = display.newText(options);
	panelGroup:insert(title);
	title.anchorX = 0;
	title.anchorY = 0;	
	title.x = -25;
	title.y = 30;
	title:setFillColor(0,0,0);
	thumbPlace:toFront();
end;

local function OnTap(self, event)
	if (isShow == false) then
		return;
	end;
	if (self.name == "bg") then
		return true;
	elseif (self.name == "close") then
		Hide(true);
	elseif (self.name == "dl") then
		system.openURL( POPUP_ADS[activeIndex].URL);	
		Hide(false);
	end;
	return true;
end


function Init(sceneView)
	popupAdsGroup = display.newGroup();
	sceneView:insert(popupAdsGroup);
	popupAdsGroup.alpha = 0;
	
	
	local bg = display.newRect(popupAdsGroup, _w, _h, _W, _H);
	bg:setFillColor(0,0,0);
	bg.alpha = 0.4;		
	bg.tap = OnTap;
	bg.name = "bg";
	bg:addEventListener( "tap", bg );
	
	
	local panelGroup = display.newGroup();
	panelGroup.x = _w;
	panelGroup.y = _h;
	popupAdsGroup:insert(panelGroup);
	
	local panel = display.newRect(panelGroup, 0, 0, 420, 520);	
	panel:setFillColor(1,1,1);
	
	thumbPlace = display.newImageRect(panelGroup, "assets/images/popupads/black-frame-menu.png", 350, 237);
	thumbPlace.y = -110;
	
	local downloadNow = display.newImageRect(panelGroup, "assets/images/popupads/btn-download.png", 260, 70);
	downloadNow.y = 200;
	downloadNow.tap = OnTap;
	downloadNow.name = "dl";
	downloadNow:addEventListener( "tap", downloadNow );
	
	local text = display.newText(panelGroup, "AD", 185,235, native.systemFont, 25);
	text:setFillColor(0,0,0);
	
	
	local star = display.newImageRect(panelGroup, "assets/images/popupads/icon-star-yellow.png", 130,24);
	star.y = 135;
	star.x = 40;
	
	local closeButton = display.newImageRect(panelGroup, "assets/images/popupads/Icon-Btn-circle-close.png", 50, 50);
	closeButton.x = 200;
	closeButton.y = -250;
	closeButton.tap = OnTap;
	closeButton.name = "close";
	closeButton:addEventListener( "tap", closeButton );
	
	InitContent(panelGroup);
--			local text = display.newText( "AD", textBG.x, textBG.y, native.systemFont, 25 );
--		local image = display.newImageRect(icon, v.IMG, ICON_SIZE, ICON_SIZE);				
			
end


function Hide(animation)
	AddIndex();
	isShow = false;
	local animTime = 1;
	if (animation == true) then
		animTime = 100;
	end
	transition.fadeOut( popupAdsGroup, { tag="popupads",time=animTime,onComplete=function(object)
	end});
end;

function Show()
	isShow = true;
	transition.fadeIn( popupAdsGroup, { tag="popupads",time=400 } );
end;


function Destroy()
	transition.cancel( "popupads" );
	display.remove(popupAdsGroup);
	popupAdsGroup = nil;
	print("destroy");		
end

