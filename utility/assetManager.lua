module(..., package.seeall);

local FILE_DIRECTORY = "assetData/";

local _W = display.contentWidth
local _H = display.contentHeight
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

local fileIO = require("utility.fileParsing");
local factory = require("utility.factory");


--TODO frontload and store data in hash tables

--section name == "menu", "gameplay", "shop"
function getAssets(strSectionName, group)	
	local listAssetData = fileIO.loadData(FILE_DIRECTORY .. strSectionName .. ".json", system.ResourceDirectory);
	local listAssets = {};
  
  for i = 1, #listAssetData.display_objects do
    local data = listAssetData.display_objects[i];
    
    listAssets[data.name] = factory.createObject(data);
	if (listAssets[data.name].group) then
      group:insert(listAssets[data.name].group);
    else
      group:insert(listAssets[data.name]);
    end
  end
  
	return listAssets;
end

function getPatients(strSectionName, group)
  local listAssetData = fileIO.loadData(FILE_DIRECTORY .. strSectionName .. ".json", system.ResourceDirectory);
	local listAssets = {};
  
  for i = 1, listAssetData.num_patients do
    local data = {};
    data.name = (listAssetData.name .. i);
    data.dispatch = data.name;
    data.type = listAssetData.type;
    data.filename = (listAssetData.filename .. i .. ".png");
    data.width = listAssetData.width;
    data.height = listAssetData.height;
    data.xPos = listAssetData.xPos;
    data.yPos = listAssetData.yPos;
    
    listAssets[data.name] = factory.createObject(data);
    group:insert(listAssets[data.name]);
  end
  
	return listAssets;
end

function createPatient(group,patientID)
  local listAssetData = fileIO.loadData(FILE_DIRECTORY .. "gameplay/"..patientID .. ".json", system.ResourceDirectory);
  
  local listAssets = {};
  
  for i = 1, #listAssetData.display_objects do
    local data = listAssetData.display_objects[i];
    
    listAssets[data.name] = factory.createObject(data);
    group:insert(listAssets[data.name]);
  end
  
	return listAssets;
end
function getScrollingAssets(file, group)
  local listAssetData = fileIO.loadData(FILE_DIRECTORY .. file .. ".json", system.ResourceDirectory);
	local listAssets = {};
  
  for i = 1, #listAssetData.display_objects do
    local data = listAssetData.display_objects[i];
    
    listAssets[data.name] = {};
    
    for i = 1, 4 do
      local object = factory.createObject(data);
      
      local prevIndex = (i - 1);
      local mod = 0.5;
        
      if (data.name == "bush") then
        mod = 2;
      end
      
      object.mod = mod;
      
      if (prevIndex > 0) then
        local currentTable = listAssets[data.name];
        
        
        object.x = (currentTable[prevIndex].contentBounds.xMax + (object.contentWidth * mod));
        
      end
      
      table.insert(listAssets[data.name], object);
      group:insert(object);
    end
  end
  
  return listAssets;
end


function getAssetsByLevel(numStage, numLevel)
  local stageData = fileIO.loadStageData(numStage);
  
  local levelData = fileIO.loadLevelData(numStage, stageData.levels[numLevel].filename);
  
  local dataForLevel = levelData.level_data;
  
  local listLayers = {};
  
  for i = 1, dataForLevel.total_layers do
    listLayers[i] = factory.newGroup();
  end
  
  --TODO create objects, insert into proper groups, return to scene
  --TODO dispatch some kind of level manager data so that it know what next level will be / check to unlock & unlock if that happens that way / if its the last level of a world / etc
end

--data for level select screen
function getListLevels()
  local levels = {};
  
  local numStages = fileIO.loadTotalStagesData();
  numStages = numStages.num_stages;
  
  local userData = fileIO.getUserData();
    
  for i = 1, numStages do
    local stageData = fileIO.loadStageData(i);
    
    local imageSet = factory.newGroup();
    
    for j = 1, #stageData.stage_data do
      local objectData = stageData.stage_data[j];
      
      imageSet:insert(factory.createObject(objectData));
    end
    
    local thumbnail = imageSet[imageSet.numChildren];
    
    if ((userData.levels_unlocked.unlocked == false) and (i ~= 1)) then
      thumbnail.locked = true;
      thumbnail:setFillColor(128,128,128);
      
      local lock = display.newImage("assets/images/levelselect/lock.png");
      lock.width = 114;
      lock.height = 114;
      lock.xScale = xScale;
      lock.yScale = yScale;
      lock.x = thumbnail.x;
      lock.y = thumbnail.y;
      imageSet:insert(lock);
    end  
    
    --get if level completed
--    local levelIsCompleted = fileIO.getLevelCompleted(i);
    
--    local strLevelIcon = "";
    
--    if (levelIsCompleted == true) then
--      strLevelIcon = "assets/images/levelselect/level_complete.PNG";
--    else
--      strLevelIcon = "assets/images/levelselect/level_incomplete.PNG";
--    end
    
--    local icon = display.newImage(strLevelIcon);
--    icon.xScale = xScale;
--    icon.yScale = yScale;
--    icon.x = thumbnail.x;
--    icon.y = thumbnail.y;
--    imageSet:insert(icon);
    
    imageSet.x = _w;
    imageSet.y = _h;
    levels[i] = imageSet;
  end
  
  return levels;
end

function getLevelBackground(numLevel)
  local assetData = fileIO.loadStageData(numLevel);
  
  assetData = assetData.background;
  return factory.createObject(assetData);
end

function getLevelItems(filename, group, strType)  
  local listAssetData = fileIO.loadObjectPlacement(FILE_DIRECTORY..filename);
  
  listAssetData = listAssetData.items;
  
  local listAssets = {};
  
  local imageSet = factory.newGroup();
  local counter;
  
  if (strType == "collector") then
    counter = 1;
  end
  
  for i = 1, #listAssetData do
    local data = listAssetData[i];
    data.title = data.name;
    
    local key = data.name;
    
    --differentiate same items when in challenge mode (otherwise will just reuse same key in list)
    if (counter) then
      key = (key .. counter);
      
      data.name = key;
      data.dispatch = key;
      
      counter = (counter + 1);
    else
      key = i;
    end
    
    listAssets[key] = factory.createObject(data);
    listAssets[key].title = data.title;
    
    if (listAssets[key].title == "socks") then
      listAssets[key].title = "sock";
    elseif (listAssets[key].title == "rubber ducky") then
      listAssets[key].title = "black rubber duck";
    elseif (listAssets[key].title == "santa's boot") then
      listAssets[key].title = "santa's red boot";  
--    elseif (data.filename == "assets/images/objects/popsicle 005.png") then
--      listAssets[key].title = "ice cream";
--    elseif (data.filename == "assets/images/objects/popsicle 008.png") then
--      listAssets[key].title = "ice cream";
--    elseif (data.filename == "assets/images/objects/popsicle 009.png") then
--      listAssets[key].title = "ice cream";
    elseif (data.filename == "assets/images/objects/Red Suitcase.png") then
      listAssets[key].title = "red suitcase";
    end
    
    
    if (strType == "collector") then
      if (data.title == "umbrella") then
          listAssets[key].xScale = (listAssets[key].xScale * 1.4);
          listAssets[key].yScale = (listAssets[key].yScale * 1.4);
      elseif (data.title == "hockey stick") then
        listAssets[key].xScale = (listAssets[key].xScale * 1.5);
        listAssets[key].yScale = (listAssets[key].yScale * 1.5);
      elseif (data.title == "parking meter") then
        listAssets[key].xScale = (listAssets[key].xScale * 1.15);
        listAssets[key].yScale = (listAssets[key].yScale * 1.15);
      elseif (data.title == "candlestick") then
        listAssets[key].xScale = (listAssets[key].xScale * 1.3);
        listAssets[key].yScale = (listAssets[key].yScale * 1.3);
      elseif (data.title == "tennis racket") then
        listAssets[key].xScale = (listAssets[key].xScale * 1.5);
        listAssets[key].yScale = (listAssets[key].yScale * 1.5);
      elseif (data.filename == "assets/images/objects/butterfly b.png") then
        listAssets[key].xScale = (0.5);
        listAssets[key].yScale = 0.5;
      end
      
--    
--      listAssets[key].y = (listAssets[key].y + 10);
    end
    
    if (listAssets[key].title == "parrot") then
      listAssets[key].title = "macaw";
    elseif (listAssets[key].title == "parrot1") then
      listAssets[key].title = "parrot";
    elseif (listAssets[key].title == "flowers") then
      listAssets[key].title = "flower pot";
    elseif (listAssets[key].title == "ice packs") then
      listAssets[key].title = "ice pack";
    elseif (listAssets[key].title == "cheese patter") then
      listAssets[key].title = "cheese platter";
    elseif (listAssets[key].title == "bowl of strawberries") then
      listAssets[key].title = "strawberries";  
    elseif (listAssets[key].title == "starbucks coffee mug") then
      listAssets[key].title = "starbucks mug";  
    elseif (listAssets[key].title == "italian dog") then
      listAssets[key].title = "dog";  
    end
    
    group:insert(listAssets[key]);
  end
  
  return listAssets;
end


function getAnimation(filename, group, xPos, yPos)  
  local listAssetData = fileIO.loadData(FILE_DIRECTORY .. filename .. ".json", system.ResourceDirectory);
  local data = listAssetData.properties;
  data.xPos = xPos;
  data.yPos = yPos;
  
	local animation = factory.createObject(data);
  print(group);
  group:insert(animation.group);

	return animation;
end



