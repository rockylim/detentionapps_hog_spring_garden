module(..., package.seeall);

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

local DELAY_WRONG_ANIMATION = 500;

local assetManager = require("utility.assetManager");

local wrongAnimation;

local handleAnimation;

local group;

function initialize(_group)
  group = display.newGroup();
  _group:insert(group);
end

local function endAnimation(e)  
  display.remove(wrongAnimation);
  Runtime:removeEventListener("end_wrong_animation", endAnimation);
end

function doAnimation(x, y)
  wrongAnimation = assetManager.getAnimation("animation_wrong", group, x, y);
  
  wrongAnimation.xScale = (100 / wrongAnimation.width);
  wrongAnimation.yScale = wrongAnimation.xScale;
  
  wrongAnimation:play(DELAY_WRONG_ANIMATION);
  
  Runtime:addEventListener("end_wrong_animation", endAnimation);  
end

function cancelAnimation()
  
end

function destroy()  
  display.remove(wrongAnimation);
  display.remove(group);
end
