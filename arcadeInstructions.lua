-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

local clickFX = audio.loadSound("SOUNDS/Instructions_Back_Forward Arrows.mp3");



-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view;
	
	local img1 = display.newImageRect(group, "DuckShooter_Instructions_003.png", 480, 320);
	img1.x, img1.y = 240, 160;
	
	local nextButton = display.newRect(group, 0, 0, 120, 40);
	nextButton.x, nextButton.y = 430, 125;
	nextButton.isHitTestable = true;
	nextButton.isVisible = false;
	function nextButton:touch(event)
		if event.phase == "began" then
			audio.play(clickFX, {channel = audio.findFreeChannel()});
			storyboard.gotoScene("gameMode", "fade");
		end
	end
	nextButton:addEventListener("touch", nextButton);
	
	local backButton = display.newRect(group, 0, 0, 120, 40);
	backButton.x, backButton.y = 50, 125;
	backButton.isHitTestable = true;
	backButton.isVisible = false;
	function backButton:touch(event)
		if event.phase == "began" then
			audio.play(clickFX, {channel = audio.findFreeChannel()});
			storyboard.gotoScene("menu", "fade");
		end
	end
	backButton:addEventListener("touch", backButton);
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	local removeAll;
	
	removeAll = function(group)
		if group.enterFrame then
			Runtime:removeEventListener("enterFrame", group);
		end
		if group.touch then
			group:removeEventListener("enterFrame", group);
			Runtime:removeEventListener("enterFrame", group);
		end		
		for i = group.numChildren, 1, -1 do
			if group[i].numChildren then
				removeAll(group[i]);
			else
				if group[i].enterFrame then
					Runtime:removeEventListener("enterFrame", group[i]);
				end
				if group[i].touch then
					group[i]:removeEventListener("enterFrame", group[i]);
					Runtime:removeEventListener("enterFrame", group[i]);
				end
			end
		end
	end
	
	removeAll(group);
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene;