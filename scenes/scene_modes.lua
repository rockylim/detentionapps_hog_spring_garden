local storyboard = require("storyboard");
local scene = storyboard.newScene();
storyboard.purgeOnSceneChange = true;

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

local DELAY_BETWEEN_FLASH = 6000;
local FLASH_PLAY_DELAY = 1000;

local EVENT_TAG = "lanterning";

--delegates for efficiency
local rand = math.random;
local randSeed = math.randomseed;
local randTime = os.time;

randSeed(randTime());
rand(); rand(); rand();

--contains all visuals
local children;

local listImages;

local listRuntimeListeners;

local handleFlashTimer;

local handleFlashAnimation = nil;
local handleChosenMode = nil;
local countdownSpawnCandy;

local weed;
local weedGroup;

local handleBirdTimer;

local handleBirdAnimation = nil;

local function spawnCandy()
  local startX = rand(_W * 0.1, _W * 0.9);
  local startY = (_H * -0.3);
  
  local randIndex = rand(1,2);
  
  local images = 
  {
    listImages["candy1"],
    listImages["candy2"],
  };
  
  local candy = images[randIndex];
  
  candy.x,candy.y = startX,startY;
  
  transition.to(candy,{time = 2000, y = (_H * 1.2), tag = EVENT_TAG, onComplete = function()
    
  end});
end

countdownSpawnCandy = function()
  transition.to(children,{time = rand(3000,8000), tag = EVENT_TAG, onComplete = function()
    spawnCandy();
    countdownSpawnCandy();
  end});
end

local function animateAxe()
  transition.to(listImages.axe, {time = 1000, x = (-200), tag = EVENT_TAG, rotation = -720, onComplete = function()
    listImages.axe.x = (_W * 1.2);
    listImages.axe.rotation = 0;
  end});
end

local function countdownThrowAxe()
  listImages.axe.x = (_W * 1.2);
  
  transition.to(children,{time = rand(3000,8000), tag = EVENT_TAG, onComplete = function()
--    spawnCandy();
    animateAxe();
    countdownThrowAxe();
  end});
end

local function animateBat(bat)

  bat:play(700);

end

local function leafAnim(leaf1,leaf2)

  leaf1.y = -leaf1.contentHeight;

  leaf1.x = (rand(300, _W));

  

  leaf2.y = -leaf2.contentHeight;

  leaf2.x = (rand(300, _W));

  

  transition.to(leaf1, {time = 3500, x = rand(0,_W), y = (leaf1.contentHeight * 0.5) + _H, tag = EVENT_TAG, onComplete = function()

    transition.to(leaf2, {time = 3500, x = rand(0,_W), y = (leaf2.contentHeight * 0.5) + _H, tag = EVENT_TAG, onComplete = function()

       transition.to(leaf1, {time = 2500, tag = EVENT_TAG, onComplete = function()

         leafAnim(leaf1,leaf2);

      end}); 

    end});

  end});



end

local function snowAnim(flake)
  local randX = rand(200,_W * 0.85);
  flake.x = randX;  flake.y = (-flake.contentHeight);
  transition.to(flake, {time = 3000, y = (_H + flake.contentHeight), tag = EVENT_TAG,rotation = rand(180,360), onComplete = function()
    flake.rotation = 0;
    snowAnim(flake);
  end});
end

local function flowerAnim(green,pink)
  green.y = -200;
  green.rotation = 0;
  
  pink.y = -200;
  pink.rotation = 0;
  
  local randX = rand(200,_W * 0.85);
  local randFlower = rand(1,2);
  
  local flower;
  
  if (randFlower == 1) then
    flower = green;
  else
    flower = pink;
  end
  
  flower.x = randX;
  --flake.x = randX;  flake.y = (-flake.contentHeight);
  transition.to(flower, {time = 3000, y = (_H + flower.contentHeight), rotation = 1080, tag = EVENT_TAG,rotation = rand(180,360), onComplete = function()
        
--    flake.rotation = 0;
--    snowAnim(flake);
    flowerAnim(green,pink)
  end});
end

local function maskRainbow(rainbow,index)
--  local group = display.newGroup();
--  group:insert(children[index]);
  local container = display.newContainer(_W,_H);
  container.anchorChildren = false;
  container.anchorX = 0;
  container.anchorY = 0;
  container.x = _W;  container.y = 0;
  container:insert(rainbow,false);

--  local mask = graphics.newMask( "assets/images/menu/mode/rainbowmask.png" );

--  rainbow:setMask(mask);

  rainbow.x = -_w;
  transition.to(container,{time = 2000, x = 0});
  transition.to(rainbow, {time = 2000, x = _w});

  print(rainbow.x);
  print(rainbow.contentBounds.xMin);
  children:insert(index,container);
end

local function animateBoat(boat)
  transition.to(boat.group, {time = 8000, x = (_W * 1.5), tag = EVENT_TAG, onComplete = function()
    listImages["boat"].group.x = (-_W * 0.5);
    
    transition.to(boat.group, {time = 4000, tag = EVENT_TAG, onComplete = function()
      animateBoat(boat);
    end});
  end});
end

local bInitialAnim;

local function countdownTaxi(taxi)
  
  local delay = 8000;
  
  if (bInitialAnim == true) then
    bInitialAnim = nil;
    delay = 10;
  end
  
  transition.to(taxi, {time = delay, tag = EVENT_TAG, onComplete = function()
      transition.to(taxi, {time = 1000, x = -_W * 0.3, tag = EVENT_TAG, onComplete = function()
      taxi.x = (_W * 1.2);
      countdownTaxi(taxi);
    end});
  end});
end

local modBfly = 0;
local function doButterfly(bfly1,bfly2,bfly3)
  
  bfly1.x,bfly2.x,bfly3.x = (-_W * 0.3),(-_W * 0.3),(-_W * 0.3);
  
  
--  modBfly = (modBfly + 1);
  
--  if (modBfly > 2) then
--    modBfly = 1;
--  end
  
  local flies = {bfly1,bfly2,bfly3};
  
  local bfly = flies[rand(1,3)];
  
--  bfly = bfly3; --test
  
  if (bfly == bfly2) then
    bfly.rotation = 100;
  elseif (bfly == bfly3) then
    bfly.rotation = 220;
  end
  
  bfly.y = rand(100,600);
  
  local targetY = rand(100,600);
  
--  if (modBfly == 1) then
--    bfly.x,bfly.y = (_W),(0);
--  end
  
  transition.to(bfly, {time = 3000, x = _W * 1.2, y = targetY, tag = EVENT_TAG, onComplete = function()
--    transition.to(bfly, {time = 1000, x = 100, y = _H+200, tag = EVENT_TAG,rotation = -20, onComplete = function()
      transition.to(bfly, {time = 6000, tag = EVENT_TAG, onComplete = function()
        doButterfly(bfly1,bfly2,bfly3);
      end});
--    end});
  end});
end

local function doCrane(crane)
  crane.y = (_H * 1.5);
  crane.x = (_W * 0.65);
  
  transition.to(crane, {time = 2000, tag = EVENT_TAG, onComplete = function()
    transition.to(crane, {time = 400, y = (_H * 0.8), tag = EVENT_TAG, onComplete = function()
      transition.to(crane, {time = 400, tag = EVENT_TAG, onComplete = function()
        crane.xScale = -crane.xScale;
        
        transition.to(crane, {time = 400, tag = EVENT_TAG, onComplete = function()
          crane.xScale = -crane.xScale;
          
          transition.to(crane, {time = 400, tag = EVENT_TAG, onComplete = function()
            transition.to(crane, {time = 400, y = (_H * 1.5), tag = EVENT_TAG, onComplete = function()
              crane.x = (_W * 0.35);
              
              transition.to(crane, {time = 400, y = (_H * 0.8), tag = EVENT_TAG, onComplete = function()
                 transition.to(crane, {time = 400, tag = EVENT_TAG, onComplete = function()
                   transition.to(crane, {time = 400, y = (_H * 1.5), tag = EVENT_TAG, onComplete = function()
                     doCrane(crane);
                   end});
                 end});
              end});
            end});
          end});
        end});
      end});
    end});
  end});
end

local function doBalloons(balloon, balloon2)
  balloon.y,balloon2.y = (_H * 1.5),(_H * 1.5);
  
  local randBalloon = rand(1,2);
  
  local targetBalloon = balloon;
  
  if (randBalloon == 2) then
    targetBalloon = balloon2;
  end
  
  targetBalloon.x = rand(200,_W - 200);
  transition.to(targetBalloon, {time = rand(2000, 4000), tag = EVENT_TAG, onComplete = function()
    transition.to(targetBalloon, {time = 3000, y = (-_H * 0.85), tag = EVENT_TAG, onComplete = function()
      doBalloons(balloon,balloon2); 
    end});
  end});
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
--  Runtime:dispatchEvent({name = "showBannerAd", isBottom = true});
--  Runtime:dispatchEvent({name = "hideAd"});
  --Runtime:dispatchEvent({name = "showInterstitial"});
  
	children = self.view;
  
  local assetManager = require("utility.assetManager");
	listImages = assetManager.getAssets("menu/modes", children);
  
--  listImages.balloon.xScale,listImages.balloon.yScale = 0.15,0.15;
--  listImages.balloon2.xScale,listImages.balloon2.yScale = 0.15,0.15;
  
--  doBalloons(listImages.balloon,listImages.balloon2);
  
--  listImages.crane.xScale,listImages.crane.yScale = 0.5,0.5;
--  doCrane(listImages.crane);
--  listImages.taxi.xScale, listImages.taxi.yScale = 0.1,0.1;
--  listImages.taxi.x,listImages.taxi.y = (_W * 1.2),(_H * 0.8);
  
--  bInitialAnim = true;
  
--  countdownTaxi(listImages.taxi);
  doButterfly(listImages.butterfly1,listImages.butterfly2,listImages.butterfly3);
--  listImages["boat"].group.x = (-_W * 0.5);
--  listImages["boat"]:play(400);
  
--  animateBoat(listImages["boat"]);
  --HO2014_GameModes_Car_1
  
--  for i = 1, children.numChildren do
--    if (children[i].name == "rainbow") then
--      maskRainbow(children[i],i);
--      return;
--    end
--  end
--  listImages["car"]:play(800);
--  listImages["bird"]:play(BIRD_PLAY_DELAY);
--  listImages.ship.x = (_W + 100);
--  countdownAnimateShip(listImages.ship,true);
--  leafAnim(listImages["leaf1"],listImages["leaf2"]);
--  snowAnim(listImages["snowflake"]);
  
--  flowerAnim(listImages["green flower"], listImages["pink flower"]);
  
--  balloonAnim(listImages["balloon"]);
--  countdownSpawnCandy();
--  listImages["balloons"]:play(BALLOONS_PLAY_DELAY);
--  countdownSpawnCandy();
--  countdownThrowAxe();
--  animateBat(listImages["bat"]);

--  transition.to(listImages["bat"].group, {time = 500, xScale = 4, yScale = 4, tag = EVENT_TAG, onComplete = function()

--  end});

--  transition.to(listImages["bat"].group, {time = 1000, x = listImages["bat"].group.x - 100, y = listImages["bat"].group.y + 100, tag = EVENT_TAG, onComplete = function()
--     transition.to(listImages["bat"].group, {time = 300, alpha = 0, tag = EVENT_TAG,onComplete = function()

--     end});
--  end});
end

local handleBalloonsTimer;

handleBirdAnimation = function(e)
  handleBirdTimer = timer.performWithDelay(3000, function(e)
    listImages["car"]:play(800);
  end);
end


-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
	
end

local function handleHomeHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  changeScene("scene_menu");
  return true;
end

local function handleMinigameHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
--  changeScene("scene_minigame");
  storyboard.gotoScene("arcadeShooter");
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
  listRuntimeListeners = 
  {
    --{name = "end_balloon", func = handleFlashAnimation},
--    {name = "end_car", func = handleBirdAnimation},
    {name = "traditional_hit", func = handleChosenMode},
    {name = "chill_hit", func = handleChosenMode},
    {name = "adventure_hit", func = handleChosenMode},
    {name = "home_hit", func = handleHomeHit},
    {name = "minigame_hit", func = handleMinigameHit},
--    {name = "end_balloons", func = handleBalloonsAnimation},
  };
  
  for i = 1, #listRuntimeListeners do
    Runtime:addEventListener(listRuntimeListeners[i].name, listRuntimeListeners[i].func);
  end
end

handleChosenMode = function(e)
  Runtime:dispatchEvent({name = "button_sound"});
  
  local chosenMode;
  
  if (e.name == "traditional_hit") then
    chosenMode = "traditional";
  elseif (e.name == "chill_hit") then
    chosenMode = "chill";
  elseif (e.name == "adventure_hit") then
    chosenMode = "adventure";    
  end
  
  if (chosenMode == "adventure") then
    changeScene("scene_adventure_levelselect");
  else
    changeScene("scene_gametypes", {mode = chosenMode});
  end
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
--  listImages["bat"]:stop();
  
	for i = 1, #listRuntimeListeners do
    Runtime:removeEventListener(listRuntimeListeners[i].name, listRuntimeListeners[i].func);
  end
  
  transition.cancel(EVENT_TAG);
  
--  listImages["car"]:stop();
  
--  listImages["bird"]:stop();
----  listImages["balloons"]:stop();
  
--  if (handleBirdTimer) then
--    timer.cancel(handleBirdTimer);
--    handleBirdTimer = nil;
--  end
  
end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
	
end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
	
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene;