local storyboard = require("storyboard");
local scene = storyboard.newScene();
storyboard.purgeOnSceneChange = true;

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

--contains all visuals
local children;

local listImages;
local listRuntimeListeners;

local levelPages;

local listLocks;
local locksGroup;
local lockPages;

local levelsGroup;
local currentPage;

local listCheckmarks;
local checkmarkPages;

local function createLock(xPos, yPos)
  local lock = display.newImage("assets/images/buttons/lock.png");
  lock.width = 128;
  lock.height = 128;
  lock.xScale = xScale;
  lock.yScale = yScale;
  lock.x = xPos;
  lock.y = yPos;
  return lock;
end

local function createCheckmark(xPos, yPos)
  local image = display.newImage("assets/images/buttons/checkmark.png");
  image.width = 150;
  image.height = 150;
  image.xScale = xScale;
  image.yScale = yScale;
  image.x = (xPos + 80);
  image.y = (yPos - 70);
  
  children:insert(image);
  return image;
end

local function forgePage()
  if (levelPages ~= nil) then
    display.remove(levelPages);
    levelPages = nil;
  end
  
    levelPages = display.newGroup();
    levelsGroup:insert(levelPages);
    local assetManager = require("utility.assetManager");
    local page = assetManager.getAssets("menu/adventure_levelselect"..currentPage, levelPages);
    
--    levelsGroup:insert(levelPages[i]);
--    levelPages[i].isVisible = false;
--  end
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	children = self.view;
  levelsGroup = display.newGroup();
  children:insert(levelsGroup);
  Runtime:dispatchEvent({name = "showBannerAd", isBottom = nil});
  
  
  local prevPage;
  
  if (event.params ~= nil) then
    prevPage = event.params.page;
  end
  
  local assetManager = require("utility.assetManager");
	listImages = assetManager.getAssets("menu/adventure_levelselect", children);
  
  --levelPages = {};
  
  
--  for i = 1, 5 do
--    levelPages[i] = display.newGroup();
--    local page = assetManager.getAssets("menu/adventure_levelselect"..i, levelPages[i]);
    
--    levelsGroup:insert(levelPages[i]);
--    levelPages[i].isVisible = false;
--  end
  
  currentPage = 1;
  
  if (prevPage ~= nil) then
    currentPage = prevPage;
  end
  
  forgePage();
  
  --levelPages[currentPage].isVisible = true;
  
  if (_G.GAME_UNLOCKED == false) then
    locksGroup = display.newGroup();
    children:insert(locksGroup);
    local lockIndex = 4;
    listLocks = {};
    lockPages = {};
    
    local count = 0;
    
    for i = 1, 5 do
      local tempGroup = display.newGroup();
      lockPages[i] = display.newGroup();
      
      if (i == 2) then
        
        local lock = createLock(572, 357);
        local lock2 = createLock(934, 357);
        
        tempGroup:insert(lock);
        tempGroup:insert(lock2);
        
        lockPages[i]:insert(tempGroup);
      elseif (i ~= 1) then
        local lock1 = createLock(201, 357);
        local lock2 = createLock(572, 357);
        local lock3 = createLock(934, 357);
        tempGroup:insert(lock1);
        tempGroup:insert(lock2);
        tempGroup:insert(lock3);
        
--        lockPages[i] = display.newGroup();
        lockPages[i]:insert(tempGroup);
        
      end
      
      locksGroup:insert(lockPages[i]);
      lockPages[i].isVisible = false;
    end
    
    lockPages[currentPage].isVisible = true;
  end
  
  listCheckmarks = {};
  
  local fileIO = require("utility.fileParsing");
  local userData = fileIO.getUserData();
  checkmarkPages = {};
  
  local counter = 0;
  
  for i = 1, 5 do
    local checkmark1 = createCheckmark(130, 250);
    local checkmark2 = createCheckmark(500, 250);
    local checkmark3 = createCheckmark(860, 250);
    local checkGroup = display.newGroup();
    checkGroup:insert(checkmark1);
    checkGroup:insert(checkmark2);
    checkGroup:insert(checkmark3);
    checkmarkPages[i] = checkGroup;
    
    counter = (counter + 1);
    if (userData["adventure"..counter].isCompleted == true) then
      checkmark1.isVisible = true;
    else
      checkmark1.isVisible = false;
    end
    
    counter = (counter + 1);
    if (userData["adventure"..counter].isCompleted == true) then
      checkmark2.isVisible = true;
    else
      checkmark2.isVisible = false;
    end
    
    counter = (counter + 1);
    if (userData["adventure"..counter].isCompleted == true) then
      checkmark3.isVisible = true;
    else
      checkmark3.isVisible = false;
    end
    
    children:insert(checkmarkPages[i]);
    checkmarkPages[i].isVisible = false;
  end
  
  checkmarkPages[currentPage].isVisible = true;
  
end


-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
	
end

local function handleHomeHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  changeScene("scene_menu");
end

local function handleBackHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  changeScene("scene_modes");
end

local function levelSelected(e)
  Runtime:dispatchEvent({name = "button_sound"});
  
  local levelNum;
  
  if (e.name == "level1_hit") then
    levelNum = 1;
  elseif (e.name == "level2_hit") then
    levelNum = 2;
  elseif (e.name == "level3_hit") then
    levelNum = 3;
  elseif (e.name == "level4_hit") then
    levelNum = 4;
  elseif (e.name == "level5_hit") then
    levelNum = 5;
  elseif (e.name == "level6_hit") then
    levelNum = 6;
  elseif (e.name == "level7_hit") then
    levelNum = 7;
  elseif (e.name == "level8_hit") then
    levelNum = 8;
  elseif (e.name == "level9_hit") then
    levelNum = 9;
  elseif (e.name == "level10_hit") then
    levelNum = 10;
  elseif (e.name == "level11_hit") then
    levelNum = 11;
  elseif (e.name == "level12_hit") then
    levelNum = 12;
  elseif (e.name == "level13_hit") then
    levelNum = 13;
  elseif (e.name == "level14_hit") then
    levelNum = 14;
  elseif (e.name == "level15_hit") then
    levelNum = 15;
    
  end
  
  if ((_G.GAME_UNLOCKED == false) and (levelNum > 4)) then
    Runtime:dispatchEvent({name = "buy_game_unlock"});
    return;
  end
  
  if ((levelNum <= 4) and (levelNum ~= 2)) then
    changeScene("scene_gameplay", {mode = "adventure", gameType = "word", level = levelNum, nextScene = "scene_gameplay", showAd = true});
    return;
  end
  
  changeScene("scene_gameplay", {mode = "adventure", gameType = "word", level = levelNum});
end

local function handleGameUnlocked(e)
  --display.remove(locksGroup);
  for i = 1, #lockPages do
    display.remove(lockPages[i]);
  end
  
  Runtime:removeEventListener("game_unlocked", handleGameUnlocked);
end

local function handlePageHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  local name = e.target.name;
  
  local pageMod = 0;
  
  if (name == "left") then
    pageMod = -1;
  elseif (name == "right") then
    pageMod = 1;
  end
  
  --levelPages[currentPage].isVisible = false;
  
  if(_G.GAME_UNLOCKED == false) then
    lockPages[currentPage].isVisible = false;
  end
  
  checkmarkPages[currentPage].isVisible = false;
  
  currentPage = (currentPage + pageMod);
  
  if (currentPage > 5) then
    currentPage = 1;
  elseif (currentPage < 1) then
    currentPage = 5;
  end
  
  --levelPages[currentPage].isVisible = true;
  forgePage();
  
  if(_G.GAME_UNLOCKED == false) then
    lockPages[currentPage].isVisible = true;
  end
  
  checkmarkPages[currentPage].isVisible = true;
  print("currentPage", currentPage);
end

local function onDemoTap(e)
--  PlayGameSound(audioData.CLICK_SOUND)
--  audio.play(clickFX, {channel = audio.findFreeChannel()});
  Runtime:dispatchEvent({name = "button_sound"});
  system.openURL(_G.DEMO_GAME_URL2);
  return true;
end

local EVENT_TAG = "adv";
local function pulse(image)
  if (image == nil) then return; end
  
  image.handle = transition.to(image, {time = 600, xScale = 0.85,yScale=0.85, tag = EVENT_TAG, onComplete = function()
    if (image == nil) then return; end
    image.handle = transition.to(image, {time = 600, xScale = 1,yScale=1, tag = EVENT_TAG,onComplete = function()
      pulse(image);
    end});
  end});
end

local function createDemoGameButton()
  local image;
  if (_G.isAmazon == true) then
	image = display.newImageRect(scene.view,"assets/images/buttons/demo_button3.png",150,150);
  else
	image = display.newImageRect(scene.view,"assets/images/buttons/demo_button2.png",150,150);
  end
  
  image.x,image.y = (_W * 0.5),(_H * 0.85);
  image.name = "demoGame";
  image:addEventListener("tap",onDemoTap);
  
  pulse(image);
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
  if (_G.ENABLE_DEMO_GAME_BUTTON2 == true) then
    createDemoGameButton();
  end
  if (_G.GAME_UNLOCKED == false) then
    Runtime:addEventListener("game_unlocked", handleGameUnlocked);
  end
  
  Runtime:addEventListener("touch", function(e) print(e.x,e.y); end);
  
	listRuntimeListeners = 
  {
    {name = "home_hit", func = handleHomeHit},
    {name = "back_hit", func = handleBackHit},
    {name = "level1_hit", func = levelSelected},
    {name = "level2_hit", func = levelSelected},
    {name = "level3_hit", func = levelSelected},
    {name = "level4_hit", func = levelSelected},
    {name = "level5_hit", func = levelSelected},
    {name = "level6_hit", func = levelSelected},
    {name = "level7_hit", func = levelSelected},
    {name = "level8_hit", func = levelSelected},
    {name = "level9_hit", func = levelSelected},
    {name = "level10_hit", func = levelSelected},
    {name = "level11_hit", func = levelSelected},
    {name = "level12_hit", func = levelSelected},
    {name = "level13_hit", func = levelSelected},
    {name = "level14_hit", func = levelSelected},
    {name = "level15_hit", func = levelSelected},
    {name = "page_hit", func = handlePageHit},
  };
  
  for i = 1, #listRuntimeListeners do
    Runtime:addEventListener(listRuntimeListeners[i].name, listRuntimeListeners[i].func);
  end
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
  transition.cancel(EVENT_TAG);
  if (_G.GAME_UNLOCKED == false) then
    Runtime:removeEventListener("levels_unlocked", handleGameUnlocked);
  end
  
	for i = 1, #listRuntimeListeners do
    Runtime:removeEventListener(listRuntimeListeners[i].name, listRuntimeListeners[i].func);
  end
end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
	
end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
	
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene;