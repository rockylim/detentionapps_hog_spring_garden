local storyboard = require("storyboard");
local scene = storyboard.newScene();
storyboard.purgeOnSceneChange = true;

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

--delegates for efficiency
local rand = math.random;
local randSeed = math.randomseed;
local randTime = os.time;

randSeed(randTime());
rand(); rand(); rand();

--constraints when dragging background objects when zoomed in/out
--local ZOOM_OUT_Y_MIN = (_H * -0.198);
local ZOOM_OUT_Y_MIN = (_H * -0.20);
local ZOOM_OUT_Y_MAX = (_H * 0.0);
local ZOOM_OUT_X_MIN = 0;
local ZOOM_OUT_X_MAX = 0;
local ZOOM_IN_Y_MIN = (_H * -1.20);
local ZOOM_IN_Y_MAX = (_H * 0.0);
local ZOOM_IN_X_MIN = (-_W);
local ZOOM_IN_X_MAX = (0);

local TRADITIONAL_SET = _G.TRADITIONAL_SET;

local CHILL_SET = _G.CHILL_SET;

local ADVENTURE_SET = _G.ADVENTURE_SET;

 
--TRADITIONAL- BG1 - BG8.
--CHILL: BG9 - BG16.
--ADVENTURE: BG 17 (level 1), BG 18 (level 2), use BG 1 (level 3), 
  --use BG 9 (level 4), use BG 11 (level 5), use BG 19 (level 6), use BG 20 (level 7), use BG 2 (level 8), 
  --BG 21 (level 9), use BG 10 (level 10), use BG 6 (level 11), use BG 15 (level 12), 
  --BG 23 (level 13), BG 22 (level 14), use BG 3 (level 15)
  
--level 1 is bg5 in adventure, so offset the number 1 by this number
local ADVENTURE_LEVEL_MOD = 15;

local children;

local listImages;

local HUD;
local listZoom;

local listRuntimeListeners;

local isZoomed;

local startDragX, startDragY, lastDragX, lastDragY;

local disableInput;

local listItems;

local foundAnimation;

local levelChosen;
local modeChosen;
local typeChosen;
local sceneChosen;

local originalZoomX;
local originalZoomY;

local endGame = nil;
local handleItemTouch = nil;
local collectItem = nil;

local backgroundGroup;
local HUDGroup;
local listZoomGroup;

local isBGDragged = false;

local function layerGroups()
  backgroundGroup = display.newGroup();
  HUDGroup = display.newGroup();  
  listZoomGroup = display.newGroup();  
  
  children:insert(backgroundGroup);
  children:insert(HUDGroup);  
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	children = self.view;
  
  layerGroups();
  
  local options = event.params;
  levelChosen = options.level;
  modeChosen = options.mode;
  typeChosen = options.gameType;
  sceneChosen = options.scene;
  
  if (options.showAd == true) then
    print("SHOWING AD");
    Runtime:dispatchEvent({name = "showInterstitial"});  
  end
  
  local level = (sceneChosen or levelChosen);
  
  if (modeChosen == "adventure") then
    level = (ADVENTURE_SET[level]);
    --sceneChosen = level;
    typeChosen = "adventure";
  else
    if (modeChosen == "chill") then
      level = CHILL_SET[level];
    else
      level = TRADITIONAL_SET[level];
    end
    
  --elseif (modeChosen == "chill") then
    --level = (level + 4);
  end
  
  local assetManager = require("utility.assetManager");
	
  local background = display.newImage(backgroundGroup,"assets/images/levels/BG"..level..".jpg");
  background.width = _W;
  background.height = _H;
  background.xScale = xScale;
  background.yScale = yScale;
  background.x = _w;
  background.y = _h;
  
  isZoomed = false;
   
  listItems = assetManager.getLevelItems("stages/"..modeChosen.."/stage"..(sceneChosen or levelChosen)..typeChosen, backgroundGroup, typeChosen);    

  listZoom = require("gameplay.ListZoom");
  listZoom.initialize(listZoomGroup);

  HUD = require("gameplay.HUD");
  HUD.initialize(HUDGroup, typeChosen, modeChosen, listItems, listZoom);
  
  foundAnimation = require("animation.animationFoundItem");
  foundAnimation.initialize(backgroundGroup);
  
  wrongAnimation = require("animation.animationWrong");
  wrongAnimation.initialize(backgroundGroup);
  
  disableInput = false;
end

-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
	
end

local function constrainBackground()
  local xMin, xMax;
  local yMin, yMax;
  
  if (isZoomed == false) then
    xMin = ZOOM_OUT_X_MIN;
    xMax = ZOOM_OUT_X_MAX;
    yMin = ZOOM_OUT_Y_MIN;
    yMax = ZOOM_OUT_Y_MAX;
  else
    xMin = ZOOM_IN_X_MIN;
    xMax = ZOOM_IN_X_MAX;
    yMin = ZOOM_IN_Y_MIN;
    yMax = ZOOM_IN_Y_MAX;
  end
  
  if (backgroundGroup.y > yMax) then
    backgroundGroup.y = yMax;
  elseif (backgroundGroup.y < yMin) then
    backgroundGroup.y = yMin;
  end
  
  if (backgroundGroup.x > xMax) then
    backgroundGroup.x = xMax;
  elseif (backgroundGroup.x < xMin) then
    backgroundGroup.x = xMin;
  end
  
  
end

local function dragBackground(e)
	if (e.y > 611) then
		return;
	end;

  if (e.phase == "began") then
    startDragX = e.x;
    startDragY = e.y;
	lastDragX = e.x;
    lastDragy = e.y;
    isBGDragged = false;
  elseif (e.phase == "moved") then 
    if (startDragX == nil) then return; end
    
    local deltaX = (e.x - lastDragX);
    local deltaY = (e.y - lastDragy);
    
    backgroundGroup.x = (backgroundGroup.x + deltaX);
    backgroundGroup.y = (backgroundGroup.y + deltaY);
    
    lastDragX = e.x;
    lastDragy = e.y;
    isBGDragged = true;   
    constrainBackground();
  elseif (e.phase == "ended") then    
  	if ((startDragX == nill) or (startDragY == nil)) then
  		return;
  	end
  	if ((math.floor(startDragX) == math.floor(e.x)) and (math.floor(startDragY) == math.floor(e.y))) then
      local localX, localY = e.target:contentToLocal( e.x, e.y );
      --wrongAnimation.doAnimation(e.x - backgroundGroup.x, e.y - backgroundGroup.y);    
      wrongAnimation.doAnimation(localX, localY);    
      Runtime:dispatchEvent({name = "wrong_sound"});
    end
  end
  
  return true;
end


local function hintHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  if (disableInput == true) then return; end
  
  disableInput = true;
  
  local xPos, yPos;
  
  --button sound, play animation on random item
  local counter = 0;
  local unfoundItems = {};
  
  for i,v in pairs(listItems) do
    if (v.found ~= true) then
      table.insert(unfoundItems, i);
    end
  end
  
  if (#unfoundItems == 0) then return; end
  
  local randItem = rand(1, #unfoundItems);
  randItem = unfoundItems[randItem];
  randItem = listItems[randItem];
  
  local assetManager = require("utility.assetManager");
  local hintAnimation = assetManager.getAnimation("animation_hint", backgroundGroup, randItem.x, randItem.y);
  hintAnimation:play(1000);
  Runtime:dispatchEvent({name = "hint_sound"});
  Runtime:dispatchEvent({name = "hint_used"});
  
  local function handleEndHintAnimation(e)
    Runtime:removeEventListener("end_hint_animation", handleEndHintAnimation);
    display.remove(hintAnimation);
    disableInput = false;
  end
  
  Runtime:addEventListener("end_hint_animation", handleEndHintAnimation);
end

local function zoomHit(e)
  Runtime:dispatchEvent({name = "zoom_sound"});
  if (disableInput == true) then 
    if (isZoomed == true) then
      e.target.toggleOn.isVisible = false;
      e.target.toggleOff.isVisible = true;
    else
      e.target.toggleOn.isVisible = true;
      e.target.toggleOff.isVisible = false;
    end
    
    
    return; 
  end
  
  if (originalZoomX == nil) then
    originalZoomX = backgroundGroup.xScale;
    originalZoomY = backgroundGroup.yScale;
  end
  
  --isZoomed  
  if (e.toggle == "off") then --handle zoom in
    isZoomed = true;
    backgroundGroup.xScale = (backgroundGroup.xScale * 2);
    backgroundGroup.yScale = (backgroundGroup.yScale * 2);
  elseif (e.toggle == "on") then --handle zoom out
    isZoomed = false;
    backgroundGroup.xScale = originalZoomX;
    backgroundGroup.yScale = originalZoomY;
  end
  
  constrainBackground();
end

local function imageRefresh(e)
  print("OONTER");
  for i,v in pairs(listItems) do
    if ((listItems[i] ~= nil) and (listItems[i].found == true)) then
      print("HIDE");
      listItems[i].alpha = 0;
    elseif (listItems[i] ~= nil) then
      print("FROOSH");
      local width,height,xPos,yPos,file,parent,dispatch,name;
      width = listItems[i].width;
      height = listItems[i].height;
      xPos = listItems[i].x;
      yPos = listItems[i].y;
      file = listItems[i].filename;
      parent = listItems[i].parent;
      dispatch = listItems[i].dispatch;
      name = listItems[i].name;
      
      local prevXScale, prevYScale = listItems[i].xScale,listItems[i].yScale;
      
      display.remove(listItems[i]);
      
      local factory = require("utility.factory");
      listItems[i] = factory.createObject({name = name, width = width, height = height, xPos = xPos, yPos = yPos, filename = file, dispatch = dispatch,type = "one_state_button"});
      --listItems[i] = display.newImageRect(file, width, height);
--      listItems[i].xScale = prevXScale;
--      listItems[i].yScale = prevYScale;
--      listItems[i].x = xPos;
--      listItems[i].y = yPos;
      parent:insert(listItems[i]);
    end
    
  end
  
end

-- Called immediately after scene has moved onscreen:
  function scene:enterScene( event )
  backgroundGroup:addEventListener("touch", dragBackground);  
  
  listRuntimeListeners = 
  {
    {name = "play_hint", func = hintHit},
    {name = "zoom_hit", func = zoomHit},
    {name = "end_game", func = endGame},
    {name = "collectItem", func = collectItem},
    {name = "refresh_images", func = imageRefresh},
    
  };
  
  local testNumItems = 0;
  
  for i,v in pairs(listItems) do
    testNumItems = (testNumItems + 1);
    table.insert(listRuntimeListeners, {name = v.dispatch, func = handleItemTouch});
  end
  
  print("NUMBER OF ITEMS ", testNumItems);
  
  for i = 1, #listRuntimeListeners do
    Runtime:addEventListener(listRuntimeListeners[i].name, listRuntimeListeners[i].func);
  end
end

collectItem = function(e)
  print("COLLECTING");
  e.target.found = true;
  e.target.inSlot = true;
  foundAnimation.doAnimation(e.target);
end

handleItemTouch = function(e)
  --dispatch to hud that item touched
  if ((e.target == nil) or (e.target.found == true)) then return; end
  
  Runtime:dispatchEvent({name = "checkTouchedItem", target = e.target});
end

endGame = function(e)
  --e.isWin
  
  timer.performWithDelay(2000, function()
    
    changeScene("scene_endgame", {isWin = e.isWin, score = e.score, scene = sceneChosen, level = levelChosen, mode = modeChosen, gameType = typeChosen});
  end);
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
  for i = 1, #listRuntimeListeners do
    Runtime:removeEventListener(listRuntimeListeners[i].name, listRuntimeListeners[i].func);
  end
  
  Runtime:dispatchEvent({name = "exit_gameplay"});
  
  disableInput = false;
  
  listZoom.destroy();  
  HUD.destroy();    
end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
	
end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
	
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene;