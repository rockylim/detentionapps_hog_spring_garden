local storyboard = require("storyboard");
local iconAds = require("IconAds");
local popupAds = require("PopupAds");

local att;
if (system.getInfo("platform") == "ios") then
    att = require( "plugin.att" )
end;

storyboard.purgeOnSceneChange = true;
local scene = storyboard.newScene();

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

local function getIfUnlocked()
  local fileIO = require("utility.fileParsing");    
  local userData = fileIO.getUserData();      
  return (userData.game_unlocked.unlocked  == true) or (_G.IS_IAP_ON == false);
end


--contains all visuals
local children;

local listImages;

local nextScene;

local firstShow = true;

-- Called when the scene's view does not exist:
function scene:createScene( event )
	print("creating menu scene");
  Runtime:dispatchEvent({name = "showBannerAd"});
  
	listImages = {};
	
	children = self.view;
	
	local assetManager = require("utility.assetManager");
	listImages = assetManager.getAssets("menu/menu", children);
	
	      if (_G.IS_IAP_ON == false) then
        listImages.button_restore_purchase.isVisible = false;        
    end
  
--  listImages.button_purchase_unlock.isVisible = false;
--  listImages.button_more_games.isVisible = false;
 
  listImages.play.isVisible = false;

  --nextScene = storyboard.loadScene("scenes.scene_modes", false);

-- Do something here

-- Later, transition to the scene (no loading necessary)
--storyboard.gotoScene( "scene2", "slideLeft", 800 )
end

-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
	
end

local function playMenuMusic(e)
  Runtime:dispatchEvent({name = "menu_music"});
end

local function moreGamesHit(e)
  system.openURL(MORE_GAMES_LINK);
  Runtime:dispatchEvent({name = "button_sound"});
end

local function restoreHit(e)
  Runtime:dispatchEvent({name = "restore_purchases"});
  Runtime:dispatchEvent({name = "button_sound"});
end

local function buyGameUnlockHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  Runtime:dispatchEvent({name = "buy_game_unlock"});
end

local function hideBuyButton(e)
  listAssets.button_buy_unlock.isVisible = false;
end

local function backgroundHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  changeScene("scene_modes");
end

local function purchaseUnlockHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  Runtime:dispatchEvent({name = "buy_game_unlock"});
end

local function handleUnlock(e)
  Runtime:removeEventListener("levels_unlocked", handleUnlock);
  listImages["button_purchase_unlock"].isVisible = false;
end

local function onDemoTap(e)
--  PlayGameSound(audioData.CLICK_SOUND)
--  audio.play(clickFX, {channel = audio.findFreeChannel()});
  Runtime:dispatchEvent({name = "button_sound"});
  system.openURL(_G.DEMO_GAME_URL);
  return true;
end

local function pulse(image)
  if (image == nil) then return; end
  
  image.handle = transition.to(image, {time = 600, xScale = 0.85,yScale=0.85, tag = EVENT_TAG, onComplete = function()
    if (image == nil) then return; end
    image.handle = transition.to(image, {time = 600, xScale = 1,yScale=1, tag = EVENT_TAG,onComplete = function()
      pulse(image);
    end});
  end});
end

local function createDemoGameButton()
  --[[local image = display.newImageRect(scene.view,"assets/images/buttons/demo_button.png",180,180);
  image.x,image.y = (_W * 0.09),100;
  image.name = "demoGame";
  image:addEventListener("tap",onDemoTap);
  
  pulse(image);]]--
end

local function attListener(e)
    print("******* ", tostring(e.status));
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
  iconAds.Init(children);
  popupAds.Init(children);
  
  if (_G.ENABLE_DEMO_GAME_BUTTON == true) then
    createDemoGameButton();
  end
  
  if (_G.GAME_UNLOCKED == true) then
    listImages["button_purchase_unlock"].isVisible = false;
  else
    Runtime:addEventListener("game_unlocked", handleUnlock);
  end
  
  Runtime:addEventListener("background_hit", backgroundHit);
  Runtime:addEventListener("more_games_hit", moreGamesHit);
  Runtime:addEventListener("restore_purchases_hit", restoreHit);
  Runtime:addEventListener("purchase_unlock_hit", purchaseUnlockHit);
  
  Runtime:dispatchEvent({name = "menu_music"});
  
    if (firstShow == false) then
      if (getIfUnlocked() == false) then
            popupAds.Show();
      end;
  else
    firstShow = false;
  end;
  
    if (system.getInfo("platform") == "ios") then
        --if (FREE_VERSION == false) then
            att.request(attListener);
        --end;
    end;
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )  
  if (_G.GAME_UNLOCKED == false) then
    Runtime:removeEventListener("levels_unlocked", handleUnlock);
  end
  
  Runtime:removeEventListener("background_hit", backgroundHit);
  Runtime:removeEventListener("more_games_hit", moreGamesHit);
  Runtime:removeEventListener("restore_purchases_hit", restoreHit);
  Runtime:removeEventListener("purchase_unlock_hit", purchaseUnlockHit);
  
--  Runtime:dispatchEvent({name = "showInterstitial"});
  --native.setActivityIndicator(true);
end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
  iconAds.Destroy();        	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
	
end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
	
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene;