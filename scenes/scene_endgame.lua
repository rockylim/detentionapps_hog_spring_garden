local storyboard = require("storyboard");
local scene = storyboard.newScene();
storyboard.purgeOnSceneChange = true;

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

--contains all visuals
local children;

local listImages;
local listRuntimeListeners;

local levelChosen;
local modeChosen;
local typeChosen;
local sceneChosen;
local isWin;

local currentScore;
local highScore;

local currentScoreImage;
local highScoreImage;

local function cacheWin()
  local fileIO = require("utility.fileParsing");
  local levelData = fileIO.getUserData();
  
  local key;
  if (modeChosen ~= "adventure") then
    key = (modeChosen .. sceneChosen .. typeChosen);
  else
    key = (modeChosen .. levelChosen);
  end
  
  print("BURG", key);
  
  highScore = levelData[key].highScore;
  
  levelData[key].isCompleted = true;
  
  fileIO.saveShopData(levelData);
end

local function getHighScore()
  local fileIO = require("utility.fileParsing");
  local levelData = fileIO.getUserData();
  
  local key;
  if (modeChosen ~= "adventure") then
    key = (modeChosen .. levelChosen .. typeChosen);
  else
    key = (modeChosen .. levelChosen);
  end
  
  highScore = levelData[key].highScore;
end

local function cacheHighScore()
  local fileIO = require("utility.fileParsing");
  local levelData = fileIO.getUserData();
  
  local key;
  
  if (modeChosen ~= "adventure") then
    key = (modeChosen .. levelChosen .. typeChosen);
  else
    key = (modeChosen .. levelChosen);
  end
  
  levelData[key].highScore = currentScore;
  
  fileIO.saveShopData(levelData);
end

local function deleteHighScore()
  currentScore = 0;
  highScore = 0;
  
  cacheHighScore();
  
  display.remove(currentScoreImage);
  display.remove(highScoreImage);
  
  local factory = require("utility.factory");
  local numFilename = "assets/images/numbers/score_";
  
  currentScoreImage = factory.forgeDigitsFromString(numFilename, currentScore, 35, 1, 1);
  highScoreImage = factory.forgeDigitsFromString(numFilename, highScore, 35, 1, 1);
  
  currentScoreImage.x = (_W * 0.55);
  currentScoreImage.y = (_H * 0.52);
  highScoreImage.x = (_W * 0.57);
  highScoreImage.y = (_H * 0.67);
  
  children:insert(currentScoreImage);
  children:insert(highScoreImage);
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	children = self.view;
--  Runtime:dispatchEvent({name = "showBannerAd", isBottom = nil});
  Runtime:dispatchEvent({name = "showInterstitial"});
  
  local options = event.params;
  levelChosen = options.level;
  modeChosen = options.mode;
  typeChosen = options.gameType;
  sceneChosen = options.scene;
  
  isWin = options.isWin;
  currentScore = options.score;
  
  if (isWin == true) then
    cacheWin();
  end
  
--  if (highScore == nil) then --on a loss
--    getHighScore();
--  end
  
  local factory = require("utility.factory");
  local numFilename = "assets/images/numbers/score_";
  
  currentScoreImage = factory.forgeDigitsFromString(numFilename, currentScore, 50, 1, 1);
  currentScoreImage.isVisible = false;
--  if (currentScore > highScore) then
--    highScore = currentScore;
--    cacheHighScore();
--  end
  
--  higScoreImage = factory.forgeDigitsFromString(numFilename, highScore, 35, 1, 1);
  
  local assetManager = require("utility.assetManager");
  listImages = assetManager.getAssets("gameplay/end_screen", children);
  
  currentScoreImage.anchorX = 1;
  currentScoreImage.anchorY = 1;
  currentScoreImage.x = (_W * 0.5);
  currentScoreImage.y = (_H * 0.65);
--  highScoreImage.x = (_W * 0.57);
--  highScoreImage.y = (_H * 0.67);
  
  children:insert(currentScoreImage);
--  children:insert(highScoreImage);
  
  if (isWin == true) then
    --listImages["button_next"].alpha = 0.01;
    listImages["win"].isVisible = true;
    listImages["loss"].isVisible = false;
    listImages["button_retry"].isVisible = false;
  else
    listImages["button_next"].isVisible = false;
    listImages["win"].isVisible = false;
    listImages["loss"].isVisible = true;
    listImages["button_retry"].alpha = 1;
    listImages["button_retry"].isVisible = true;
    
--    local retry = display.newImageRect(children, "assets/images/buttons/retry.png",100,100);
--    retry.x,retry.y = _w,(listImages["button_retry"].y);
  end
  
  --listImages["button_score_reset"].isVisible = false;
  
  listImages["button_next"].isVisible = false;
  listImages["button_retry"].isVisible = false;
end


-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
	
end

local function handleHomeHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  changeScene("scene_menu");
end

local function handleRetryHit(e)
  if (isWin ~= false) then return; end
  Runtime:dispatchEvent({name = "button_sound"});
  changeScene("scene_gameplay", {level = levelChosen, mode = modeChosen, gameType = typeChosen, scene = sceneChosen});
end

local function promptPurchase()
  Runtime:dispatchEvent({name = "buy_game_unlock"});
end

local function handleNextHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  if (isWin ~= true) then return; end
  
  local nextLevel = levelChosen;
  local currentMode = modeChosen;
  local nextGameType = typeChosen;
  
  if (currentMode ~= "adventure") then
    changeScene("scene_gametypes",{mode = modeChosen, level = levelChosen, scene = sceneChosen});
    return;
  else
    local adventurePage = math.ceil(levelChosen / 3);
    changeScene("scene_adventure_levelselect", {page = adventurePage});  
  end
  
  --changeScene("scene_gameplay", {level = nextLevel,  mode = currentMode, gameType = nextGameType});
end

local function handleResetScore(e)
  Runtime:dispatchEvent({name = "button_sound"});
  native.showAlert("Score Reset", "Are you sure you want to reset the score for this level?  This will also delete the current score!", {"No Thanks","Okay"},     function(e)
      if (e.index == 1) then --no thanks pressed
        
      elseif (e.index == 2) then --okay pressed
        deleteHighScore();
      end
      
  end);
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	listRuntimeListeners = 
  {
    {name = "home_hit", func = handleHomeHit},
    {name = "retry_hit", func = handleRetryHit},
    {name = "next_hit", func = handleNextHit},
    --{name = "delete_score_hit", func = handleResetScore},
  };
  
  for i = 1, #listRuntimeListeners do
    Runtime:addEventListener(listRuntimeListeners[i].name, listRuntimeListeners[i].func);
  end
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	for i = 1, #listRuntimeListeners do
    Runtime:removeEventListener(listRuntimeListeners[i].name, listRuntimeListeners[i].func);
  end
end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
	
end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
	
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene;