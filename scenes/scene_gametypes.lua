local storyboard = require("storyboard");
local scene = storyboard.newScene();
storyboard.purgeOnSceneChange = true;

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

local AD_SENT = false;

--contains all visuals
local children;

local listImages;
local listRuntimeListeners;

local modeChosen;
local currentPage;

local listLocks;
local locksGroup;
local bgGroup;

local listCheckmarks;

local handleArrowHit = nil;
local handleTypeHit= nil;

--local TRADITIONAL_SET =
--{
--  "BG16.jpg",
--  "BG17.jpg",
--  "BG18.jpg",
--  "BG19.jpg",
  
--  "BG20.jpg",
--  "BG21.jpg",
--  "BG22.jpg",
--  "BG23.jpg",
--};

--local CHILL_SET = 
--{
--  "BG24.jpg",
--  "BG25.jpg",
--  "BG26.jpg",
--  "BG27.jpg",
  
--  "BG28.jpg",
--  "BG29.jpg",
--  "BG30.jpg",
--  "BG31.jpg",
--};

local function setCurrentPage()
  listImages["banner_scene1"].isVisible = false;
  listImages["banner_scene2"].isVisible = false;
  listImages["banner_scene3"].isVisible = false;
  listImages["banner_scene4"].isVisible = false;
  listImages["banner_scene5"].isVisible = false;
  listImages["banner_scene6"].isVisible = false;
  listImages["banner_scene7"].isVisible = false;
  listImages["banner_scene8"].isVisible = false;
  
  listImages["banner_scene"..currentPage].isVisible = true;
  
  
--  for i = 1, 8 do
--    listImages["scene"..i].isVisible = false;
--  end
  
-- -- if (modeChosen == "traditional") then
--  listImages["scene"..currentPage].isVisible = true;
  --else
    --listImages["scene"..(currentPage+4)].isVisible = true;
  --end
  
  
  
  if (_G.GAME_UNLOCKED == false) then
    for i = 1, #listLocks do
      if (listLocks[i].locked == true) then
        listLocks[i].picture.isVisible = false;
        listLocks[i].word.isVisible = false; 
        listLocks[i].collector.isVisible = false;
      end
    end
    
    if (listLocks[currentPage].locked == true) then
      listLocks[currentPage].picture.isVisible = true;
      listLocks[currentPage].word.isVisible = true; 
      listLocks[currentPage].collector.isVisible = true;
    end
  end
  
  local fileIO = require("utility.fileParsing");
  local levelData = fileIO.getUserData();
  local partialKey = (modeChosen .. currentPage);
  print(partialKey);
  local pictureCompleted = levelData[partialKey.."picture"].isCompleted;
  local wordCompleted = levelData[partialKey.."word"].isCompleted;
  local collectorCompleted = levelData[partialKey.."collector"].isCompleted;
  
  if (pictureCompleted == true) then
    listCheckmarks[1].isVisible = true;
  else
    listCheckmarks[1].isVisible = false;
  end
  
  if (wordCompleted == true) then
    listCheckmarks[2].isVisible = true;
  else
    listCheckmarks[2].isVisible = false;
  end
  
  if (collectorCompleted == true) then
    listCheckmarks[3].isVisible = true;
  else
    listCheckmarks[3].isVisible = false;
  end
  
  local list;
  
  if (modeChosen == "traditional") then
    list = _G.TRADITIONAL_SET;
  else
    list = _G.CHILL_SET;
  end
  
  if (bgGroup ~= nil) then
    display.remove(bgGroup);
    bgGroup = nil;
  end
  
  bgGroup = display.newGroup();
  children:insert(bgGroup);
  bgGroup:toBack();
  
  local image = display.newImageRect("assets/images/levels/BG"..list[currentPage]..".jpg", _W, _H);
  image.xScale = xScale;  image.yScale = yScale;
  image.x = _w;  image.y = _h;
 -- image.isVisible = false;
  image.filename = ("assets/images/levels/"..list[currentPage]);
  bgGroup:insert(image);
  --image:toBack();
  --listImages["scene"..i] = image;
  
end

local function createLock(xPos, yPos)
  local lock = display.newImage("assets/images/buttons/lock.png");
  lock.width = 64;
  lock.height = 64;
  lock.xScale = xScale;
  lock.yScale = yScale;
  lock.x = xPos;
  lock.y = yPos;
  lock.isVisible = false;
  return lock;
end

local function createCheck(xPos,yPos)
  local image = display.newImage("assets/images/buttons/checkmark.png");
  image.width = 150;
  image.height = 150;
  image.xScale = xScale;
  image.yScale = yScale;
  image.x = (xPos + 130);
  image.y = (yPos - 50);
  
  children:insert(image);
  return image;
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
  Runtime:dispatchEvent({name = "showBannerAd", isBottom = nil});
  
	children = self.view;
  local assetManager = require("utility.assetManager");
  
	listImages = assetManager.getAssets("menu/gametypes", children);
  --listImages["background"].isVisible = false;
  
  for i = 1, 8 do
    listImages["banner_scene"..i].xScale,listImages["banner_scene"..i].yScale = 0.8,0.8;
  end
  
  local options = event.params;
  modeChosen = options.mode;
  
  local list;
  
  if (modeChosen == "traditional") then
    list = TRADITIONAL_SET;
  else
    list = CHILL_SET;
  end
  
--  for i = 1, #list do
--    local image = display.newImageRect("assets/images/levels/"..list[i], _W, _H);
--    image.xScale = xScale;  image.yScale = yScale;
--    image.x = _w;  image.y = _h;
--    image.isVisible = false;
--    image.filename = ("assets/images/levels/"..list[i]);
--    children:insert(image);
--    image:toBack();
--    listImages["scene"..i] = image;
--  end
  
  currentPage = 1;
  
  listLocks = {};
  locksGroup = display.newGroup();
  children:insert(locksGroup);
  
  local lockIndex;
  if (modeChosen == "traditional") then
    lockIndex = 4;
  elseif(modeChosen == "chill") then
    lockIndex = 4;
  end
  
  for i = 1, 8 do
    listLocks[i] = {};
    if (i > lockIndex) then
      listLocks[i].locked = true;
      listLocks[i].picture = createLock(listImages.button_picture.x,listImages.button_picture.y);
      listLocks[i].word = createLock(listImages.button_word.x,listImages.button_word.y);
      listLocks[i].collector = createLock(listImages.button_collector.x,listImages.button_collector.y);
      
      locksGroup:insert(listLocks[i].picture);
      locksGroup:insert(listLocks[i].word);
      locksGroup:insert(listLocks[i].collector);
    end
    
  end
  
  listCheckmarks = {};
  
  table.insert(listCheckmarks, createCheck(listImages.button_picture.x,listImages.button_picture.y));
  table.insert(listCheckmarks, createCheck(listImages.button_word.x,listImages.button_word.y));
  table.insert(listCheckmarks, createCheck(listImages.button_collector.x,listImages.button_collector.y));
  
  if (options.scene) then
    currentPage = options.scene;
  end
  
  setCurrentPage();
end


-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
	
end

local function handleHomeHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  changeScene("scene_menu");
end

local function handleGameUnlocked(e)
  display.remove(locksGroup);
  Runtime:removeEventListener("game_unlocked", handleGameUnlocked);
end

local function handleBackHit(e)
  Runtime:dispatchEvent({name = "button_sound"});
  changeScene("scene_modes");
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
  if (_G.GAME_UNLOCKED == false) then
    Runtime:addEventListener("game_unlocked", handleGameUnlocked);
  end
  
	listRuntimeListeners = 
  {
    {name = "home_hit", func = handleHomeHit},
    {name = "back_hit", func = handleBackHit},
    {name = "page_left_hit", func = handleArrowHit},
    {name = "page_right_hit", func = handleArrowHit},
    {name = "picture_hit", func = handleTypeHit},
    {name = "word_hit", func = handleTypeHit},
    {name = "collector_hit", func = handleTypeHit},
  };
  
  for i = 1, #listRuntimeListeners do
    Runtime:addEventListener(listRuntimeListeners[i].name, listRuntimeListeners[i].func);
  end
  
  --native.setActivityIndicator(false);
end

handleArrowHit = function(e)
  Runtime:dispatchEvent({name = "button_sound"});
  local deltaPage;
  if (e.name == "page_left_hit") then
    deltaPage = -1;
  elseif (e.name == "page_right_hit") then
    deltaPage = 1;
  end
  
  currentPage = (currentPage + deltaPage);
  
  if (currentPage < 1) then
    currentPage = 8;
  elseif (currentPage > 8) then
    currentPage = 1;
  end
  
  setCurrentPage();
end

handleTypeHit = function(e)
  Runtime:dispatchEvent({name = "button_sound"});
  local typeChosen;
  local levelChosen = currentPage;
  local sceneNum = levelChosen;
  
  if (e.name == "picture_hit") then
    typeChosen = "picture";
  elseif (e.name == "word_hit") then
    typeChosen = "word";
  elseif (e.name == "collector_hit") then
    typeChosen = "collector";
  end
  
  if (modeChosen == "traditional") then
    if (levelChosen > 4) then
      levelChosen = (levelChosen + 4);
    end
  elseif (modeChosen == "chill") then
    if (levelChosen > 4) then
      if (levelChosen == 5) then
        levelChosen = 14;
      elseif (levelChosen == 6) then
        levelChosen = 15;
      elseif (levelChosen == 7) then
        levelChosen = 19;
      elseif (levelChosen == 8) then
        levelChosen = 21;
      end
      
    end
  end
  
  if (_G.GAME_UNLOCKED == false) then
    if ((modeChosen == "traditional") and (levelChosen > 4)) then
      --locked under current design
      Runtime:dispatchEvent({name = "buy_game_unlock"});
      return;
    elseif ((modeChosen == "chill") and (levelChosen > 4)) then
      --locked under current design
      Runtime:dispatchEvent({name = "buy_game_unlock"});
      return;
    end
  end
  
  if ((typeChosen ~= "word")) then
    if ((modeChosen == "traditional") and (typeChosen == "picture")) then
      changeScene("scene_gameplay", {gameType = typeChosen, level = levelChosen, mode = modeChosen, nextScene = "scene_gameplay", showAd = true, scene = sceneNum});
    elseif (modeChosen == "chill") then
      changeScene("scene_gameplay", {gameType = typeChosen, level = levelChosen, mode = modeChosen, nextScene = "scene_gameplay", showAd = true, scene = sceneNum});
    elseif (modeChosen == "traditional") then--collector since no ad
      changeScene("scene_gameplay", {gameType = typeChosen, level = levelChosen, mode = modeChosen, scene = sceneNum});
    end
    
    return;
  end
  
  changeScene("scene_gameplay", {gameType = typeChosen, level = levelChosen, mode = modeChosen, scene = sceneNum});
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
  if (_G.GAME_UNLOCKED == false) then
    Runtime:removeEventListener("game_unlocked", handleGameUnlocked);
  end
  
	for i = 1, #listRuntimeListeners do
    Runtime:removeEventListener(listRuntimeListeners[i].name, listRuntimeListeners[i].func);
  end
end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
	
end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
	
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene;