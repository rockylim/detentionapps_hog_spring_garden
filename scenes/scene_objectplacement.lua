local storyboard = require("storyboard");
local scene = storyboard.newScene();
storyboard.purgeOnSceneChange = true;

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

local rotateMode;

local OBJECT_DIRECTORY = "assets/images/objects/";
local OBJECTS_LIST = 
{
  {name = "airplane", filename = "Airplane 2.png"},
  {name = "airplane", filename = "Airplane 3.png"},
  {name = "airplane", filename = "Airplane 4.png"},
  {name = "jet", filename = "Airplane.png"},
  {name = "antique camera", filename = "Antique Camera.png"},
  {name = "baseball hat", filename = "Baseball Hat.png"},
  {name = "baseball", filename = "Baseball.png"},
  {name = "bee", filename = "Bee.png"},
  {name = "blimp", filename = "Blimp.png"},
  {name = "boat", filename = "Boat 2.png"},
  {name = "boat", filename = "Boat.png"},
  {name = "bowling ball", filename = "Bowling Ball 2.png"},
  {name = "bowling ball", filename = "Bowling Ball.png"},
  {name = "bowling pin", filename = "Bowling Pin.png"},
  {name = "british penny", filename = "British Penny 2.png"},
  {name = "british penny", filename = "British Penny.png"},
  {name = "british pounds", filename = "British Pounds.png"},
  {name = "camel", filename = "Camel.png"},
  {name = "car", filename = "Car 2.png"},
  {name = "car", filename = "Car.png"},
  {name = "chicken", filename = "Chicken.png"},
  {name = "cleat", filename = "Cleat.png"},
  {name = "compass", filename = "Compass 2.png"},
  {name = "compass", filename = "Compass.png"},
  {name = "cradle", filename = "Cradle.png"},
  {name = "crane", filename = "Crane.png"},
  {name = "crock pot", filename = "Crock Pot 2.png"},
  {name = "crock pot", filename = "Crock Pot.png"},
  {name = "crown", filename = "Crown.png"},
  {name = "cup", filename = "Cup 2.png"},
  {name = "cup", filename = "Cup 3.png"},
  {name = "cup", filename = "Cup.png"},
  --
  {name = "diaper", filename = "Diaper.png"},
  {name = "diapers", filename = "Diapers.png"},
  {name = "dinosaur", filename = "Dinosaur 2.png"},
  {name = "dinosaur", filename = "Dinosaur 3.png"},
  {name = "dinosaur", filename = "Dinosaur.png"},
  {name = "dog", filename = "Dog 2.png"},
  {name = "dog", filename = "Dog 3.png"},
  {name = "dog", filename = "Dog 4.png"},
  {name = "dog", filename = "dog.png"},
  {name = "doll", filename = "Doll.png"},
  {name = "donkey", filename = "Donkey.png"},
  {name = "dumpster", filename = "Dumpster 2.png"},
  {name = "dumpster", filename = "Dumpster 3.png"},
  {name = "dumpster", filename = "Dumpster.png"},
  {name = "ear muffs", filename = "Ear Muffs.png"},
  {name = "egg", filename = "Egg.png"},
  {name = "elf", filename = "Elf 2.png"},
  {name = "elf", filename = "Elf 3.png"},
  {name = "elf", filename = "Elf 4.png"},
  {name = "elf", filename = "Elf 5.png"},
  {name = "elf", filename = "Elf.png"},
  {name = "elf hat", filename = "Elf Hat 2.png"},
  {name = "elf hat", filename = "Elf Hat.png"},
  {name = "fish", filename = "Fish 2.png"},
  {name = "fish", filename = "Fish a.png"},
  {name = "fish", filename = "Fish b.png"},
  {name = "fish", filename = "Fish.png"},
  {name = "fish net", filename = "Fish Net.png"},
  {name = "george washington", filename = "George Washington.png"},
  {name = "gumball machine", filename = "Gumball Machine.png"},
  {name = "hanger", filename = "Hanger 2.png"},
  {name = "hanger", filename = "Hanger.png"},
  {name = "hay bag", filename = "Hay Bag.png"},
  {name = "hay", filename = "Hay.png"},
  {name = "heart", filename = "Heart 2.png"},
  {name = "heart", filename = "Heart 3.png"},
  {name = "heart", filename = "Heart 4.png"},
  {name = "heart", filename = "Heart.png"},
  {name = "hockey mask", filename = "Hockey Mask.png"},
  {name = "hockey stick", filename = "Hockey Stick.png"},
  {name = "ice skates", filename = "Ice Skates.png"},
  {name = "iron", filename = "Iron.png"},
  {name = "jeep", filename = "Jeep 2.png"},
  {name = "jeep", filename = "Jeep 3.png"},
  {name = "jeep", filename = "Jeep.png"},
  {name = "jet", filename = "Jet.png"},
  {name = "ketchup", filename = "Ketchup 2.png"},
  {name = "ketchup", filename = "Ketchup.png"},
  {name = "lighter", filename = "Lighter 2.png"},
  {name = "lighter", filename = "Lighter.png"},
  {name = "meter token", filename = "Meter Token.png"},
  {name = "microwave", filename = "Microwave.png"},
  {name = "mop", filename = "Mop.png"},
  {name = "mug", filename = "Mug.png"},
  {name = "mushroom", filename = "Mushroom 2.png"},
  {name = "mushroom", filename = "Mushroom.png"},
  {name = "mustard", filename = "Mustard 2.png"},
  {name = "mustard", filename = "Mustard.png"},
--  {name = "native american", filename = "Native American.png"},
  {name = "nickel", filename = "Nickel 2.png"},
  {name = "nickel", filename = "Nickel a.png"},
  {name = "nickel", filename = "Nickel b.png"},
  {name = "onions", filename = "Onions.png"},
  {name = "outhouse", filename = "Outhouse.png"},
  {name = "parking meter", filename = "Parking Meter 2.png"},
  {name = "parking meter", filename = "Parking Meter 3.png"},
  {name = "parking meter", filename = "Parking Meter.png"},
  {name = "passport", filename = "Passport 2.png"},
  {name = "passport", filename = "passport.png"},
  {name = "penguin", filename = "Penguin.png"},
  {name = "pig", filename = "Pig.png"},
  {name = "pitcher", filename = "Pitcher.png"},
  {name = "pizza box", filename = "Pizza Box 2.png"},
  {name = "pizza box", filename = "Pizza Box 3.png"},
  {name = "pizza box", filename = "Pizza Box.png"},
  {name = "polar bear", filename = "Polar Bear.png"},
  {name = "quarter", filename = "Quarter 2.png"},
  {name = "quarter", filename = "Quarter.png"},
  {name = "rain boots", filename = "Rain Boots.png"},
  {name = "reindeer", filename = "Reindeer 2.png"},
  {name = "reindeer", filename = "Reindeer 3.png"},
  {name = "reindeer", filename = "Reindeer 4.png"},
  {name = "reindeer", filename = "Reindeer 5.png"},
  {name = "reindeer", filename = "Reindeer.png"},
--  {name = "robot", filename = "Robot 2.png"},
--  {name = "robot", filename = "Robot.png"},
  {name = "rubber ducky", filename = "Rubber Ducky.png"},
  {name = "ruler", filename = "Ruler 2.png"},
  {name = "ruler", filename = "Ruler 3.png"},
  {name = "ruler", filename = "Ruler 4.png"},
  {name = "ruler", filename = "Ruler.png"},
  {name = "saddle", filename = "Saddle 2.png"},
  {name = "saddle", filename = "Saddle.png"},
  {name = "safe", filename = "Safe 2.png"},
  {name = "safe", filename = "Safe.png"},
  {name = "santa", filename = "Santa 2.png"},
  {name = "santa", filename = "Santa 3.png"},
  {name = "santa", filename = "Santa.png"},
  {name = "santa bag", filename = "Santa Bag.png"},
  {name = "satellite dish", filename = "Satellite 2.png"},
  {name = "satellite dish", filename = "Satellite Dish.png"},
  {name = "shrimp", filename = "Shrimp.png"},
  {name = "sleigh", filename = "Sleigh 2.png"},
  {name = "sleigh", filename = "Sleigh.png"},
  {name = "snowman", filename = "Snowman 2.png"},
  {name = "snowman", filename = "Snowman 3.png"},
  {name = "snowman", filename = "Snowman 4.png"},
  {name = "snowman", filename = "Snowman 5.png"},
  {name = "squirrel", filename = "Squirrel.png"},
  {name = "straw hat", filename = "Straw Hat.png"},
  {name = "surfboard", filename = "Surfboard 2 a.png"},
  {name = "surfboard", filename = "Surfboard 2 b.png"},
  {name = "surfboard", filename = "Surfboard.png"},
  {name = "racket", filename = "Table Tennis Racket.png"},
  {name = "tag", filename = "Tag 2.png"},
  {name = "tag", filename = "Tag.png"},
  {name = "teddy bear", filename = "Teddy Bear.png"},
  {name = "tennis racket", filename = "Tennis Racket.png"},
  {name = "thermos", filename = "Thermos 2.png"},
  {name = "toaster oven", filename = "Toaster Oven.png"},
  {name = "toilet", filename = "Toilet.png"},
  {name = "toucan", filename = "Toucan.png"},
  {name = "train", filename = "Train.png"},
  {name = "truck", filename = "Truck 2.png"},
  {name = "truck", filename = "Truck 3.png"},
  {name = "truck", filename = "Truck.png"},
  {name = "typewriter", filename = "Typewriter.png"},
  {name = "globe", filename = "World Globe.png"},
  {name = "wreath", filename = "Wreath 2.png"},
  {name = "wreath", filename = "Wreath 3.png"},
  {name = "wreath", filename = "Wreath 4.png"},
  {name = "wreath", filename = "Wreath.png"},
  {name = "wrench", filename = "Wrench 2.png"},
  {name = "wrench", filename = "Wrench 3.png"},
  {name = "wrench", filename = "Wrench.png"},
  
  --
  {name = "african shield", filename = "African Shield.png"},
  {name = "ant", filename = "Ant.png"},
  {name = "bell", filename = "Antique Bell 2.png"},
  {name = "bell", filename = "Antique Bell.png"},
  {name = "buddha statue", filename = "Antique Buddah.png"},
  {name = "antique camera", filename = "Antique Camera.png"},
  {name = "candlestick", filename = "Antique Candlestick.png"},
  {name = "pitcher", filename = "Antique Pitcher.png"},
  {name = "radio", filename = "Antique Radio 2.png"},
  {name = "radio", filename = "Antique Radio.png"},
  {name = "apple", filename = "Apple.png"},
  {name = "baby shoe", filename = "Baby Shoe.png"},
  {name = "balloon", filename = "Balloon 2.png"},
  {name = "balloon", filename = "Balloon.png"},
  {name = "baseball", filename = "Baseball.png"},
  {name = "basketball", filename = "Basketball.png"},
  {name = "beach ball", filename = "Beachball.png"},
  --{name = "bear", filename = "Bear.png"},
  {name = "glass of beer", filename = "Beer.png"},
  {name = "orange bell pepper", filename = "Bell Pepper 2.png"},
  {name = "red bell pepper", filename = "Bell Pepper 3.png"},
  {name = "green bell pepper", filename = "Bell Pepper.png"},
  {name = "bench", filename = "Bench 2.png"},
  {name = "bench", filename = "Bench.png"},
  {name = "bike tire", filename = "Bike Tire.png"},
  {name = "birthday cake", filename = "Birthday Cake.png"},
  {name = "suitcase", filename = "Blue Suitcase.png"},
  {name = "blueberry", filename = "Blueberry.png"},
  {name = "bowling ball", filename = "Bowling Ball.png"},
  {name = "carrot cake", filename = "Cake.png"},
  {name = "camel statue", filename = "Camel Statue.png"},
  {name = "carousel horse", filename = "Carousel Horse 2.png"},
  {name = "carousel horse", filename = "Carousel Horse.png"},
  {name = "carrots", filename = "Carrots.png"},
  {name = "orange cat", filename = "Cat 2.png"},
  {name = "black kitten", filename = "Cat.png"},
  {name = "cheese", filename = "Cheese.png"},
  {name = "chef hat", filename = "Chef Hat.png"},
  {name = "clown hat", filename = "Clown Hat.png"},
  {name = "a die", filename = "Dice 1.png"},
  {name = "a die", filename = "Dice2.png"},
  {name = "dog", filename = "Dog 2.png"},
  {name = "dog", filename = "dog.png"},
  {name = "donut", filename = "Donut 2.png"},
  {name = "donut", filename = "Donut.png"},
  {name = "fish", filename = "Fish.png"},
  {name = "flower pot", filename = "Flower Pots 1.png"},
  {name = "flower pot", filename = "Flower Pots 2.png"},
  {name = "flower pot", filename = "Flower Pots 3.png"},
  {name = "flower pot", filename = "Flower Pots 4.png"},
  {name = "flower pot", filename = "Flower Pots 5.png"},
  {name = "flower pot", filename = "Flower Pots 6.png"},
  {name = "flowers", filename = "Flowers.png"},
  {name = "frog", filename = "Frog.png"},
  {name = "goat", filename = "Goat.png"},
  {name = "gold pump", filename = "Gold Pump.png"},
  {name = "golf clubs", filename = "Golf Clubs 2.png"},
  {name = "golf ball", filename = "Golfball.png"},
  {name = "guinea pig", filename = "Guinea Pig.png"},
  {name = "hard hat", filename = "Hard Hat.png"},
  {name = "horse", filename = "Horse.png"},
  {name = "horseshoe", filename = "Horseshoe.png"},
  {name = "hot air balloon", filename = "Hot Air Balloon.png"},
  {name = "jewelry box", filename = "Jewelry Box.png"},
  {name = "ladybug", filename = "Ladybug.png"},
  {name = "laptop", filename = "Laptop.png"},
  {name = "loafer", filename = "Loafer Shoe.png"},
  {name = "money", filename = "Money.png"},
  {name = "monkey", filename = "Monkey.png"},
  {name = "mouse", filename = "Mouse.png"},
  {name = "muffin", filename = "Muffin.png"},
  {name = "orange", filename = "Orange.png"},
  {name = "parrot", filename = "Parrot 2.png"},
  {name = "parrot", filename = "Parrot.png"},
  {name = "party hat", filename = "Party Hat.png"},
  {name = "peach", filename = "Peach.png"},
  {name = "pear", filename = "Pear.png"},
  {name = "pen", filename = "Pen.png"},
  {name = "pig", filename = "Pig.png"},
  {name = "piggy bank", filename = "Piggy Bank.png"},
  {name = "pizza", filename = "pizza.png"},
  {name = "plate", filename = "Plate 2.png"},
  {name = "plate", filename = "Plate 3.png"},
  {name = "plate", filename = "Plate.png"},
  {name = "poker chip", filename = "Poker Chip 2.png"},
  {name = "poker chip", filename = "Poker Chip.png"},
  {name = "purse", filename = "Purse 2.png"},
  {name = "purse", filename = "Purse 3.png"},
  {name = "purse", filename = "Purse.png"},
  {name = "snake", filename = "Rattle Snake.png"},
  {name = "record", filename = "Record 2.png"},
  {name = "record", filename = "Record.png"},
  {name = "red pump", filename = "Red Pump.png"},
  {name = "red suitcase", filename = "Red Suitcase.png"},
  {name = "red wine bottle", filename = "Red Wine.png"},
  {name = "ring", filename = "Rings 1.png"},
  {name = "ring", filename = "Rings 2.png"},
  {name = "ring", filename = "Rings 3.png"},
  {name = "ring", filename = "Rings 4.png"},
  {name = "ring", filename = "Rings 5.png"},
  {name = "ring", filename = "Rings 6.png"},
  {name = "ring", filename = "Rings 7.png"},
  {name = "ring", filename = "Rings 8.png"},
  {name = "ring", filename = "Rings 9.png"},
  {name = "rose", filename = "Rose.png"},
  {name = "red boot", filename = "Santa Shoe.png"},
  {name = "clam shell", filename = "Seashell.png"},
  {name = "shield", filename = "Shield 2.png"},
  {name = "shield", filename = "Shield.png"},
  {name = "ship wheel", filename = "Ship Wheel.png"},
  {name = "skunk", filename = "Skunk.png"},
  {name = "soccer ball", filename = "Soccer Ball.png"},
  {name = "sombrero", filename = "Sombrero.png"},
  {name = "stamp", filename = "Stamp.png"},
  {name = "straw hat", filename = "Straw Hat.png"},
  {name = "strawberry", filename = "Strawberry.png"},
  {name = "string", filename = "String.png"},
  {name = "sunflower", filename = "Sunflower.png"},
  {name = "cheese wedge", filename = "Swiss Cheese.png"},
  {name = "sword", filename = "Sword 2.png"},
  {name = "sword", filename = "Sword.png"},
  {name = "tea cup", filename = "Teacup.png"},
  {name = "tennis ball", filename = "Tennis Ball.png"},
  {name = "sneaker", filename = "Tennis Shoe 2.png"},
  {name = "sneaker", filename = "Tennis Shoe.png"},
  {name = "toy airplane", filename = "Toy Airplane.png"},
  {name = "toy phone", filename = "Toy Phone.png"},
  {name = "toy rocket", filename = "Toy Rocket.png"},
  {name = "toy train", filename = "Toy Train 2.png"},
  {name = "wallet", filename = "Wallet.png"},
  {name = "wagon wheel", filename = "Wheel 2.png"},
  {name = "car wheel", filename = "Wheel.png"},
  {name = "white wine bottle", filename = "White Wine.png"},
  {name = "old signpost", filename = "Wooden Signs.png"},
  {name = "globe", filename = "World Globe.png"},
  {name = "zodiac carvings", filename = "Zodiac Signs.png"},
  {name = "acorn", filename = "Acorn 2.png"},
  {name = "acorn", filename = "Acorn.png"},
  {name = "almond", filename = "Almond.png"},
  {name = "baseball glove", filename = "Baseball Glove.png"},
  {name = "baseball bat", filename = "Bat.png"},
  {name = "battery", filename = "Battery.png"},
  {name = "bow tie", filename = "Bow Tie 2.png"},
  {name = "bow tie", filename = "Bow Tie.png"},
  {name = "butter", filename = "Butter.png"},
  {name = "cane", filename = "Cane.png"},
  {name = "car battery", filename = "Car Battery.png"},
  {name = "cassette", filename = "Cassette Tape 2.png"},
  {name = "cassette", filename = "Cassette Tape.png"},
  {name = "compact disc", filename = "CD.png"},
  {name = "celery", filename = "Celery.png"},
  {name = "croissant", filename = "Croissant.png"},
  {name = "crown", filename = "Crown.png"},
  {name = "crutch", filename = "Crutch.png"},
  {name = "deer statue", filename = "Deer Statue.png"},
  {name = "dentures", filename = "Dentures.png"},
  {name = "door knob", filename = "Door Knob 2.png"},
  {name = "door knob", filename = "Door Knob 3.png"},
  {name = "door knob", filename = "Door Knob 4.png"},
  {name = "door knob", filename = "Door Knob.png"},
  {name = "duct tape", filename = "Duct Tape.png"},
  {name = "dust pan", filename = "Dustpan 2.png"},
  {name = "dust pan", filename = "Dustpan.png"},
  {name = "eiffel tower", filename = "Eiffel Tower.png"},
  {name = "elk statue", filename = "Elk Statue.png"},
  {name = "brown envelope", filename = "Envelope 2.png"},
  {name = "envelope", filename = "Envelope.png"},
  {name = "eraser", filename = "Eraser.png"},
  {name = "fig", filename = "Fig.png"},
  {name = "film reel", filename = "Film Reel.png"},
  {name = "fishing hook", filename = "Fishing Hook.png"},
  {name = "flute", filename = "flute 2.png"},
  {name = "flute", filename = "Flute.png"},
  {name = "picture frame", filename = "Frame 2.png"},
  {name = "picture frame", filename = "Frame 3.png"},
  {name = "picture frame", filename = "Frame 4.png"},
  {name = "picture frame", filename = "Frame 5.png"},
  {name = "picture frame", filename = "Frame 6.png"},
  {name = "picture frame", filename = "Frame 7.png"},
  {name = "picture frame", filename = "Frame 8.png"},
  {name = "picture frame", filename = "Frame 9.png"},
  {name = "picture frame", filename = "Frame 10.png"},
  {name = "picture frame", filename = "Frame 11.png"},
  {name = "picture frame", filename = "Frame 12.png"},
  {name = "picture frame", filename = "Frame 13.png"},
  {name = "picture frame", filename = "Frame 14.png"},
  {name = "picture frame", filename = "Frame 15.png"},
  {name = "picture frame", filename = "Frame.png"},
  {name = "frying pan", filename = "Frying Pan.png"},
  {name = "garbage bag", filename = "Garbage Bag.png"},
  {name = "glasses", filename = "Glasses 2.png"},
  {name = "glasses", filename = "Glasses.png"},
  {name = "glove", filename = "Glove.png"},
  {name = "guitar", filename = "guitar.png"},
  {name = "harmonica", filename = "Harmonica.png"},
  {name = "headphones", filename = "Headphones 2.png"},
  {name = "headphones", filename = "Headphones.png"},
  {name = "iphone", filename = "iPhone.png"},
  {name = "kiwi", filename = "Kiwi.png"},
  {name = "knife", filename = "Knife.png"},
--  {name = "lamp", filename = "Lamp 2.png"},
--  {name = "lamp", filename = "Lamp 3.png"},
--  {name = "lamp", filename = "Lamp 4.png"},
--  {name = "lamp", filename = "Lamp.png"},
  {name = "lantern", filename = "Lantern.png"},
  {name = "leaf", filename = "Leaf 3.png"},
  {name = "leaf", filename = "Leaf 4.png"},
  {name = "leaf", filename = "Leaf 5.png"},
  {name = "leaf", filename = "Leaf.png"},
  {name = "light bulb", filename = "Lightbulb.png"},
  {name = "lipstick", filename = "Lipstick 1.png"},
  {name = "lipstick", filename = "Lipstick 2.png"},
  {name = "lipstick", filename = "Lipstick 3.png"},
  {name = "lipstick", filename = "Lipstick 4.png"},
  {name = "lipstick", filename = "Lipstick 5.png"},
  {name = "lipstick", filename = "Lipstick 6.png"},
  {name = "lipstick", filename = "Lipstick 7.png"},
  {name = "lipstick", filename = "Lipstick 8.png"},
  {name = "lipstick", filename = "Lipstick 9.png"},
  {name = "lipstick", filename = "Lipstick 10.png"},
  {name = "lizard", filename = "Lizard 2.png"},
  {name = "lizard", filename = "Lizard.png"},
  {name = "microphone", filename = "Microphone 2.png"},
  {name = "microphone", filename = "Microphone 3.png"},
  {name = "microphone", filename = "Microphone.png"},
  {name = "mitten", filename = "Mitten.png"},
  {name = "mona lisa", filename = "Mona Lisa.png"},
  {name = "mouse", filename = "Mouse.png"},
  {name = "moustache", filename = "Moustache.png"},
  {name = "nail polish", filename = "Nail Polish 1.png"},
  {name = "nail polish", filename = "Nail Polish 2.png"},
  {name = "nail polish", filename = "Nail Polish 3.png"},
  {name = "nail polish", filename = "Nail Polish 4.png"},
  {name = "nail polish", filename = "Nail Polish 5.png"},
  {name = "oil can", filename = "Oil Can 2.png"},
  {name = "oil can", filename = "Oil Can.png"},
  {name = "paint brush", filename = "Paint Brush 2.png"},
  {name = "paint can", filename = "Paint Can 2.png"},
  {name = "paint can", filename = "Paint Can.png"},
  {name = "paint brush", filename = "Paintbrush 2.png"},
  {name = "paint brush", filename = "Paintbrush.png"},
  {name = "paper bag", filename = "Paper Bag.png"},
  {name = "paper clip", filename = "Paperclip 2.png"},
  {name = "paper clip", filename = "Paperclip 3.png"},
  {name = "paper clip", filename = "Paperclip 4.png"},
  {name = "paper clip", filename = "Paperclip 5.png"},
  {name = "paper clip", filename = "Paperclip.png"},
  {name = "penny", filename = "Penny.png"},
  {name = "plastic bag", filename = "Plastic Bag.png"},
  {name = "puzzle piece", filename = "Puzzle Piece 3.png"},
  {name = "puzzle piece", filename = "Puzzle Piece 4.png"},
  {name = "puzzle piece", filename = "Puzzle Piece 5.png"},
  {name = "puzzle piece", filename = "Puzzle Piece 6.png"},
  {name = "puzzle piece", filename = "Puzzle Piece.png"},
  {name = "saturn", filename = "Saturn.png"},
  {name = "saw", filename = "Saw.png"},
  {name = "snowball", filename = "Snowball.png"},
  {name = "soap", filename = "Soap 2.png"},
  {name = "soap", filename = "Soap.png"},
  {name = "sock", filename = "Sock.png"},
  {name = "speaker", filename = "Speaker.png"},
  {name = "steering wheel", filename = "Steering Wheel 2.png"},
  {name = "steering wheel", filename = "Steering Wheel.png"},
  {name = "stocking", filename = "Stocking.png"},
  {name = "measuring tape", filename = "Tape Measure.png"},
  {name = "thermometer", filename = "Thermometer 2.png"},
  {name = "thumbtack", filename = "Thumbtack 1.png"},
  {name = "thumbtack", filename = "Thumbtack 2.png"},
  {name = "thumbtack", filename = "Thumbtack 3.png"},
  {name = "thumbtack", filename = "Thumbtack 4.png"},
  {name = "tiara", filename = "Tiara.png"},
  {name = "tie", filename = "Tie 2.png"},
  {name = "tie", filename = "Tie.png"},
  {name = "toilet paper", filename = "Toilet Paper.png"},
  {name = "toothbrush", filename = "Toothbrush 1.png"},
  {name = "toothbrush", filename = "Toothbrush 2.png"},
  {name = "toothbrush", filename = "Toothbrush 3.png"},
  {name = "toothbrush", filename = "Toothbrush 4.png"},
  {name = "umbrella", filename = "Umbrella 2.png"},
  {name = "umbrella", filename = "Umbrella.png"},
  {name = "waffle", filename = "Waffel.png"},
  {name = "walnut", filename = "Walnut 2.png"},
  {name = "walnut", filename = "Walnut.png"},
  
  --
  {name = "baby shoe", filename = "baby shoe 2.png"},
  {name = "baby shoe", filename = "baby shoe 3.png"},
  {name = "baby shoe", filename = "baby shoe 4.png"},
  {name = "baby shoe", filename = "baby shoe5.png"},
  {name = "beaver", filename = "beaver.png"},
  {name = "bench", filename = "bench 5.png"},
  {name = "bench", filename = "bench 3.png"},
  {name = "bench", filename = "bench 4.png"},
  {name = "blanket", filename = "blanket.png"},
  {name = "blanket", filename = "blanket 2.png"},
  {name = "blanket", filename = "blanket 3.png"},
  {name = "compass", filename = "compass 3.png"},
  {name = "deer", filename = "deer.png"},
  {name = "eye chart", filename = "eye chart.png"},
  {name = "fire alarm", filename = "fire alarm.png"},
  {name = "fire alarm", filename = "fire alarm 2.png"},
  {name = "fire extinguisher", filename = "fire extinguisher.png"},
  {name = "fire extinguisher", filename = "fire extinguisher 2.png"},
  {name = "fire hydrant", filename = "fire hydrant.png"},
  {name = "fire hydrant", filename = "fire hydrant 2.png"},
  {name = "fire hydrant valves", filename = "fire hydrant valves.png"},
  {name = "goggles", filename = "goggles.png"},
  {name = "melon", filename = "melon.png"},
  {name = "melon", filename = "melon 2.png"},
  {name = "needle", filename = "needle.png"},
  {name = "needle and thread", filename = "needle and thread.png"},
  {name = "otter", filename = "otter.png"},
  {name = "otter", filename = "otter 2.png"},
  {name = "rain boots", filename = "Rain Boots.png"},
  {name = "rainbow", filename = "rainbow.png"},
  {name = "satellite", filename = "satellite.png"},
  {name = "school bus", filename = "school bus.png"},
  {name = "school bus", filename = "school bus 2.png"},
  {name = "sock", filename = "sock 2.png"},
  {name = "sock", filename = "sock 3.png"},
  {name = "steering wheel", filename = "steering wheel 3.png"},
  {name = "steering wheel", filename = "steering wheel 4.png"},
  {name = "stethoscope", filename = "stethoscope.png"},
  {name = "swing", filename = "swing 2.png"},
  {name = "swing", filename = "swing.png"},
  {name = "swing set", filename = "swing set.png"},
  {name = "tennis shoe", filename = "Tennis Shoe.png"},
  {name = "thimble", filename = "thimble.png"},
  {name = "tire", filename = "tire 2.png"},
  {name = "tire", filename = "tire.png"},
  {name = "tongue depressors", filename = "tongue depressors.png"},
  {name = "turtle", filename = "turtle 2.png"},
  {name = "turtle", filename = "turtle 3.png"},
  {name = "turtle", filename = "turtle.png"},
  {name = "weasel", filename = "weasel.png"},
  {name = "soda can", filename = "cola soda.png"},
  {name = "cooler", filename = "cooler 2.png"},
  {name = "cooler", filename = "cooler 3.png"},
  {name = "cooler", filename = "cooler 4.png"},
  {name = "cooler", filename = "cooler 5.png"},
  {name = "cooler", filename = "cooler.png"},
  {name = "golf club", filename = "golf club 2.png"},
  {name = "golf club", filename = "golf club.png"},
  {name = "ice bucket", filename = "ice bucket 2.png"},
  {name = "ice bucket", filename = "ice bucket 3.png"},
  {name = "ice bucket", filename = "ice bucket.png"},
  {name = "ice pack", filename = "ice pack.png"},
  {name = "ice packs", filename = "ice packs a.png"},
  {name = "ice packs", filename = "ice packs b.png"},
  {name = "ice packs", filename = "ice packs c.png"},
  {name = "ice pops", filename = "ice pops.png"},
  {name = "lemon soda", filename = "lemon soda.png"},
  {name = "orange soda", filename = "orange soda 2.png"},
  {name = "orange soda", filename = "orange soda 3.png"},
  {name = "orange soda", filename = "orange soda.png"},
  {name = "pineapple slice", filename = "pineapple slice.png"},
  {name = "pineapple", filename = "pineapple.png"},
  {name = "potato chips", filename = "potato chips.png"},
  {name = "towel", filename = "towel 2.png"},
  {name = "towel", filename = "towel 3.png"},
  {name = "towel", filename = "towel 4.png"},
  {name = "towel", filename = "towel.png"},
  {name = "visor", filename = "visor.png"},
  {name = "antique camera", filename = "antique camera3.png"},  
  {name = "bentley logo", filename = "bentley logo.png"},
  {name = "black bag", filename = "black bag.png"},
  {name = "blue bag", filename = "blue bag.png"},
  {name = "camera and tripod", filename = "camera and tripod.png"},
  {name = "camera", filename = "camera3.png"},
  {name = "captain hat", filename = "captain hat.png"},
  {name = "champagne glass", filename = "champagne glass.png"},
  {name = "champagne", filename = "champagne.png"},
  {name = "chef hat", filename = "chef hat 2.png"},
  {name = "chef hat", filename = "chef hat 3.png"},
  {name = "chocolate bar", filename = "chocolate bar 2.png"},
  {name = "chocolate bar", filename = "chocolate bar 3.png"},
  {name = "chocolate bar", filename = "chocolate bar 4.png"},
  {name = "chocolate bar", filename = "chocolate bar 5.png"},
  {name = "chocolate bar", filename = "chocolate bar 6.png"},
  {name = "chocolate bon bon", filename = "chocolate bon bon.png"},
  {name = "chocolate heart", filename = "chocolate heart.png"},
  {name = "chocolate strawberry", filename = "chocolate strawberry.png"},
  {name = "chocolate truffle", filename = "chocolate truffle.png"},
  {name = "chocolates", filename = "chocolate.png"},
  {name = "cupcake", filename = "cupcake.png"},
  {name = "espresso machine", filename = "espresso machine 2.png"},
  {name = "espresso machine", filename = "espresso machine.png"},
  {name = "film camera", filename = "film camera 2.png"},
  {name = "film camera", filename = "film camera.png"},
  {name = "film clapboard", filename = "film clapboard.png"},
  {name = "film clapboard", filename = "film clapboard 2.png"},
  {name = "film reel", filename = "film reel 2.png"},
  {name = "film reel", filename = "film reel 3.png"},
  {name = "film reel", filename = "film reel 4.png"},
  {name = "film", filename = "film.png"},
  {name = "gold giftbox", filename = "gold box.png"},
  {name = "grammy award", filename = "grammy award.png"},
  {name = "green giftbox", filename = "green box.png"},
  {name = "bag of money", filename = "money bag 2.png"},
  {name = "money bag", filename = "money bag.png"},
  {name = "moon jewel", filename = "moon.png"},
  {name = "orange rose bush", filename = "orange rose bush.png"},
  {name = "oscar statue", filename = "oscar statue.png"},
  {name = "oscar statue", filename = "oscar statue 2.png"},
  {name = "pastry", filename = "pastry.png"},
  {name = "pigeon", filename = "pigeon 2.png"},
  {name = "pigeon", filename = "pigeon 3.png"},
  {name = "pigeon", filename = "pigeon 4.png"},
  {name = "pigeon", filename = "pigeon.png"},
  {name = "pink bag", filename = "pink bag.png"},
  {name = "pink giftbox", filename = "pink box.png"},
  {name = "pink flower", filename = "pink flower.png"},
  {name = "pink hat", filename = "pink hat.png"},
  {name = "popcorn", filename = "popcorn 2.png"},
  {name = "popcorn", filename = "popcorn 3.png"},
  {name = "popcorn", filename = "popcorn.png"},
  {name = "red bag", filename = "red bag.png"},
  {name = "roller skate", filename = "roller skate 2.png"},
  {name = "roller skate", filename = "roller skate.png"},
  {name = "silver hat", filename = "silver hat.png"},
  {name = "spiderman mask", filename = "spider man.png"},
  {name = "strawberries", filename = "strawberries.png"},
  {name = "sun jewel", filename = "sun.png"},
  {name = "surfboard", filename = "surfboard 4.png"},
  {name = "surfboard", filename = "surfboard 5.png"},
  {name = "swim flippers", filename = "swim flippers 2.png"},
  {name = "swim flippers", filename = "swim flippers 3.png"},
  {name = "swim flippers", filename = "swim flippers.png"},
  {name = "toy car", filename = "toy car.png"},
  {name = "tripod", filename = "tripod.png"},
  {name = "wedding cake", filename = "wedding cake 2.png"},
  {name = "wedding cake", filename = "wedding cake.png"},
  {name = "whistle", filename = "whistle.png"},
  {name = "yellow rose", filename = "yellow rose.png"},
  {name = "yin yang symbol", filename = "yin yang symbol.png"},
  {name = "airplane", filename = "airplane 5.png"},
  {name = "airplane", filename = "airplane 6.png"},
  {name = "aviator glasses", filename = "aviator glasses 2.png"},
  {name = "aviator glasses", filename = "aviator glasses.png"},
  {name = "aviator goggles", filename = "aviator goggles 2.png"},
  {name = "aviator goggles", filename = "aviator goggles.png"},
  {name = "aviator hat", filename = "aviator hat.png"},
  {name = "baby aviator", filename = "baby aviator.png"},
  {name = "baby food jar", filename = "baby food jar 2.png"},
  {name = "baby food jar", filename = "baby food jar.png"},
  {name = "belt", filename = "belt 2.png"},
  {name = "belt", filename = "belt.png"},
  {name = "black ipad", filename = "black ipad.png"},
  {name = "black sock", filename = "black sock.png"},
  {name = "black striped sock", filename = "black striped sock.png"},
  {name = "black tie", filename = "black tie.png"},
  {name = "blue coffee mug", filename = "blue coffee mug.png"},
  {name = "blue tie", filename = "blue tie.png"},
  {name = "brown jacket", filename = "brown jacket.png"},
  {name = "brown slippers", filename = "brown slippers.png"},
  {name = "cell phone", filename = "cell phone 2.png"},
  {name = "cell phone", filename = "cell phone 3.png"},
  {name = "cell phone", filename = "cell phone.png"},
  {name = "chocolate box", filename = "chocolate box.png"},
  {name = "coffee mug", filename = "coffee mug-2.png"},
  {name = "coffee mug", filename = "coffee mug.png"},
  {name = "ear plugs", filename = "ear plugs 2.png"},
  {name = "ear plugs", filename = "ear plugs 3.png"},
  {name = "ear plugs", filename = "ear plugs.png"},
  {name = "eye mask", filename = "eye mask 2.png"},
  {name = "eye mask", filename = "eye mask.png"},
  {name = "gold tie", filename = "gold tie.png"},
  {name = "green jacket", filename = "green jacket.png"},
  {name = "ipad", filename = "ipad.png"},
  {name = "laptop", filename = "laptop 2.png"},
  {name = "laptop", filename = "laptop 3.png"},
  {name = "lavendar tie", filename = "lavendar tie.png"},
  {name = "luggage tag", filename = "luggage tag.png"},
  {name = "orange airplane", filename = "orange airplane.png"},
  {name = "orange slippers", filename = "orange slippers.png"},
  {name = "orange vest", filename = "orange vest.png"},
  {name = "pill bottle", filename = "pill bottle 2.png"},
  {name = "pill bottle", filename = "pill bottle.png"},
  {name = "pink striped tie", filename = "pink striped tie.png"},
  {name = "red glove", filename = "red glove.png"},
  {name = "red jacket", filename = "red jacket.png"},
  {name = "red scarf", filename = "red scarf 2.png"},
  {name = "red scarf", filename = "red scarf.png"},
  {name = "scarf", filename = "scarf.png"},
  {name = "shoe horn", filename = "shoe horn 2.png"},
  {name = "shoe horn", filename = "shoe horn.png"},
  {name = "social security card", filename = "social security card.png"},
  {name = "starbucks mug", filename = "starbucks coffee mug.png"},
  {name = "striped tie", filename = "striped tie.png"},
  {name = "tie", filename = "tie 4.png"},
  {name = "tie", filename = "tie 5.png"},
  {name = "tissue", filename = "tissue 2.png"},
  {name = "tissue", filename = "tissue.png"},
  {name = "USB stick", filename = "USB stick 2.png"},
  {name = "USB stick", filename = "USB stick 3.png"},
  {name = "USB stick", filename = "USB stick.png"},
  {name = "watch", filename = "watch 2.png"},
  {name = "watch", filename = "watch 3.png"},
  {name = "watch", filename = "watch.png"},
  {name = "water bottle", filename = "water bottle 2.png"},
  {name = "water bottle", filename = "water bottle 3.png"},
  {name = "water bottle", filename = "water bottle 4.png"},
  {name = "water bottle", filename = "water bottle.png"},
  {name = "white ipad", filename = "white ipad.png"},
  {name = "yellow scarf", filename = "yellow scarf.png"},
  {name = "ziplock bag", filename = "ziplock bag 2.png"},
  {name = "ziplock bag", filename = "ziplock bag 3.png"},
  {name = "ziplock bag", filename = "ziplock bag.png"},

  {name = "baby doll", filename = "baby doll 2.png"},
  {name = "baby doll", filename = "baby doll.png"},
  {name = "baby shoe", filename = "baby shoe 5.png"},
  {name = "baseball bat", filename = "baseball bat 3.png"},
  {name = "baseball bat", filename = "baseball bat 2.png"},
  {name = "baseball bat", filename = "baseball bat.png"},
  {name = "baseball hat", filename = "baseball hat 2.png"},  
  {name = "baseball hat", filename = "baseball hat 3.png"},  
  {name = "baseball hat", filename = "baseball hat 4.png"},  
  {name = "baseball hat", filename = "baseball hat 5.png"},  
  {name = "biscotti", filename = "biscotti.png"},  
  {name = "black boot", filename = "black boot.png"},  
  {name = "blue flower", filename = "blue flower.png"},  
  {name = "blue plate", filename = "blue plate.png"},  
  {name = "brown purse", filename = "brown purse.png"},  
  {name = "butterfly", filename = "butterfly a.png"},  
  {name = "butterfly", filename = "butterfly b.png"},  
  {name = "butterfly", filename = "butterfly c.png"},  
  {name = "butterfly", filename = "butterfly d.png"},  
  {name = "butterfly", filename = "butterfly e.png"},  
  {name = "butterfly", filename = "butterfly f.png"},  
  {name = "butterfly", filename = "butterfly g.png"},  
  {name = "butterfly", filename = "butterfly h.png"},  
  {name = "butterfly", filename = "butterfly i.png"},  
  {name = "butterfly", filename = "butterfly j.png"},  
  {name = "butterfly", filename = "butterfly k.png"},  
  {name = "butterfly", filename = "butterfly l.png"},  
  {name = "butterfly", filename = "butterfly m.png"},  
  {name = "cheese block", filename = "cheese block.png"},  
  {name = "cheese platter", filename = "cheese platter.png"},  
  {name = "cheese", filename = "cheese 2.png"},  
  {name = "chef hat", filename = "chefs hat.png"},  
  {name = "chocolate heart", filename = "chocolate heart 2.png"},  
  {name = "coffee grinder", filename = "coffee grinder.png"},  
  {name = "coffee pot", filename = "coffee pot 2.png"},  
  {name = "coffee pot", filename = "coffee pot 3.png"},  
  {name = "coffee pot", filename = "coffee pot 4.png"},  
  {name = "cork", filename = "cork 2.png"},  
  {name = "cork", filename = "cork 3.png"},  
  {name = "cork", filename = "cork.png"},  
  {name = "corkscrew", filename = "corkscrew 2.png"},  
  {name = "corkscrew", filename = "corkscrew 3.png"},  
  {name = "corkscrew", filename = "corkscrew 4.png"},  
  {name = "corkscrew", filename = "corkscrew 5.png"}, 
  {name = "gelato", filename = "gelato 2.png"},  
  {name = "gelato", filename = "gelato 3.png"},  
  {name = "gelato", filename = "gelato.png"},  
  {name = "gondolier hat", filename = "gondolier hat 2.png"},  
  {name = "gondolier hat", filename = "gondolier hat 3.png"},  
  {name = "gondolier hat", filename = "gondolier hat 4.png"},  
  {name = "gondolier hat", filename = "gondolier hat.png"},  
  {name = "gorgonzola cheese", filename = "gorgonzola cheese.png"},  
  {name = "green flower", filename = "green flower.png"},  
  {name = "guitar", filename = "guitar 2.png"},  
  {name = "guitar", filename = "guitar 3.png"},  
  {name = "guitar", filename = "guitar 4.png"}, 
  {name = "guitar", filename = "guitar 5.png"},  
  {name = "guitar pick", filename = "guitar pick 2.png"},  
  {name = "guitar pick", filename = "guitar pick 3.png"},  
  {name = "guitar pick", filename = "guitar pick 4.png"},  
  {name = "guitar pick", filename = "guitar pick.png"},  
  {name = "heart", filename = "heart 10.png"},  
  {name = "heart", filename = "heart 11.png"},  
  {name = "heart", filename = "heart 12.png"},  
  {name = "heart", filename = "heart 5.png"},  
  {name = "heart", filename = "heart 6.png"},  
  {name = "heart", filename = "heart 7.png"},  
  {name = "heart", filename = "heart 8.png"},  
  {name = "heart", filename = "heart 9.png"},  
  {name = "dog", filename = "italian dog.png"},  
  {name = "italian hat", filename = "italian hat.png"},  
  {name = "loafer shoes", filename = "loafer shoes.png"},  
  {name = "men's shoes", filename = "mens shoes.png"},  
  {name = "mona lisa", filename = "mona lisa 2.png"},  
  {name = "moon", filename = "moon 3.png"},  
  {name = "oil and vinegar", filename = "oil and vinegar.png"},  
  {name = "oil painting", filename = "oil painting 2.png"},  
  {name = "oil painting", filename = "oil painting 3.png"},  
  {name = "oil painting", filename = "oil painting.png"},  
  {name = "olive oil", filename = "olive oil.png"},  
  {name = "orange flower", filename = "orange flower 2.png"},  
  {name = "orange flower", filename = "orange flower.png"},  
  {name = "parmesan cheese", filename = "parmesan cheese.png"},  
  {name = "passport", filename = "passport 4.png"},  
  {name = "pie", filename = "pie 2.png"},  
  {name = "pie", filename = "pie 3.png"},  
  {name = "pie", filename = "pie 4.png"},  
  {name = "pie", filename = "pie 5.png"},  
  {name = "pie", filename = "pie 6.png"},  
  {name = "pie", filename = "pie 7.png"},
  {name = "pink flower", filename = "pink flower 3.png"}, 
  {name = "pinocchio", filename = "pinocchio 2.png"}, 
  {name = "pinocchio", filename = "pinocchio 3.png"}, 
  {name = "pinocchio", filename = "pinocchio 4.png"}, 
  {name = "pinocchio", filename = "pinocchio.png"}, 
  {name = "pistachio nut", filename = "pistachio nut 2.png"},  
  {name = "pistachio nut", filename = "pistachio nut 3.png"},  
  {name = "pistachio nut", filename = "pistachio nut.png"},  
  {name = "pitcher", filename = "pitcher 2.png"},  
  {name = "pizza", filename = "pizza 7.png"},  
  {name = "pizza", filename = "pizza 8.png"},  
  {name = "pizza", filename = "pizza 4.png"},  
  {name = "pizza", filename = "pizza 5.png"},  
  {name = "pizza", filename = "pizza 6.png"},  
  {name = "plate", filename = "plate 8.png"},  
  {name = "plate", filename = "plate 9.png"},  
  {name = "plate", filename = "plate 10.png"},  
  {name = "pot", filename = "pot 5.png"},  
  {name = "purple grapes", filename = "purple grapes.png"},  
  {name = "purse", filename = "purse 5.png"},  
  {name = "rag doll", filename = "rag doll 2.png"},  
  {name = "rag doll", filename = "rag doll.png"}, 
  {name = "red flower", filename = "red flower 2.png"},  
  {name = "red flower", filename = "red flower 3.png"},  
  {name = "red grapes", filename = "red grapes 2.png"},  
  {name = "red grapes", filename = "red grapes 3.png"},  
  {name = "red grapes", filename = "red grapes.png"},  
  {name = "red loafer", filename = "red loafer.png"},  
  {name = "soccer ball", filename = "soccer ball 2.png"},  
  {name = "soccer ball", filename = "soccer ball 3.png"},  
  --{name = "solar system scroll", filename = "solar system scroll.png"},  
  {name = "typewriter", filename = "typewriter 2.png"},  
  {name = "yellow flower", filename = "yellow flower.png"},  
  {name = "zodiac signs", filename = "zodiac signs 2.png"}, 
  
  {name = "antique pitcher", filename = "antique pitcher 2.png"},  
  {name = "canvas frame", filename = "canvas frame.png"},  
  {name = "cheese", filename = "cheese 4.png"},  
  {name = "cheese", filename = "cheese 3.png"},  
  {name = "chef's hat", filename = "chefs hat.png"},  
  {name = "cracker", filename = "cracker.png"},  
  {name = "crepe", filename = "crepe.png"},  
  {name = "crepes", filename = "crepes.png"},  
  {name = "croissant", filename = "croissant 2.png"},  
  {name = "egg", filename = "egg 2.png"},  
  {name = "cheese basket", filename = "french cheese basket.png"},  
  {name = "french horn", filename = "french horn.png"},  
  {name = "french horn", filename = "french horn 2.png"},  
  {name = "french horn", filename = "french horn 3.png"},  
  {name = "french toast", filename = "french toast.png"},  
  {name = "gray beret", filename = "gray beret.png"},  
  {name = "gray hat", filename = "gray hat.png"},  
  {name = "macaron", filename = "macaron 2.png"},  
  {name = "macaron", filename = "macaron 3.png"},  
  {name = "macaron", filename = "macaron.png"},  
  {name = "macarons", filename = "macarons 2.png"},  
  {name = "macarons", filename = "macarons.png"},  
  {name = "mona lisa", filename = "mona lisa 5.png"},  
  {name = "napoleon cake", filename = "napoleon cake.png"},  
  {name = "oysters", filename = "oysters.png"},  
  {name = "padlock", filename = "padlock 2.png"},  
  {name = "padlock", filename = "padlock 3.png"},  
  {name = "padlock", filename = "padlock 4.png"},  
  {name = "padlock", filename = "padlock 5.png"},  
  {name = "padlock", filename = "padlock 6.png"},  
  {name = "padlock", filename = "padlock 7.png"},  
  {name = "padlock", filename = "padlock 8.png"},  
  {name = "padlock", filename = "padlock 9.png"},  
  {name = "padlock", filename = "padlock 10.png"},  
  {name = "padlock", filename = "padlock 11.png"},  
  {name = "padlock", filename = "padlock.png"},  
  {name = "pliers", filename = "pliers.png"},  
  {name = "purple beret", filename = "purple beret.png"},  
  {name = "soccer ball", filename = "soccer ball 6.png"},  
  {name = "straw hat", filename = "straw hat 2.png"},  
  {name = "straw hat", filename = "straw hat 3.png"},  
  {name = "suitcase", filename = "suitcase 5.png"}, 
  
 
  {name = "airplane", filename = "airplane 8.png"},  
--  {name = "beer", filename = "beer a.png"},  
--  {name = "beer", filename = "beer b.png"},  
--  {name = "beer", filename = "beer c.png"},  
  {name = "blue paint", filename = "blue paint.png"},  
  {name = "rainbow butterfly", filename = "butterfly rainbow.png"},  
  {name = "cabbage", filename = "cabbage 2.png"},  
  {name = "cabbage", filename = "cabbage.png"},  
  {name = "cradle", filename = "cradle 4.png"}, 
  {name = "green cupcake", filename = "green cupcake.png"},  
  {name = "dark beer", filename = "dark beer 2.png"},  
  {name = "dark beer", filename = "dark beer.png"},  
  {name = "rainbow feather", filename = "rainbow feather.png"},  
  {name = "french fries", filename = "french fries 2.png"},  
  {name = "french fries", filename = "french fries 3.png"},  
  {name = "french fries", filename = "french fries 4.png"},  
  {name = "french fries", filename = "french fries 5.png"},  
  {name = "french fries", filename = "french fries 6.png"},  
  {name = "glasses", filename = "glasses 5.png"},  
  {name = "glasses", filename = "glasses 6.png"},  
  {name = "gravy bowl", filename = "gravy bowl.png"},  
  {name = "green beer", filename = "green beer.png"},  
  {name = "green paint", filename = "green paint.png"},  
  {name = "green ribbon", filename = "green ribbon award.png"},  
  {name = "guinness", filename = "guinness 2.png"},  
  {name = "guinness", filename = "guinness 3.png"},  
  {name = "guinness", filename = "guinness 4.png"},  
  {name = "guinness", filename = "guinness.png"},  
  {name = "harmonica", filename = "harmonica 2.png"},  
  {name = "harp", filename = "harp.png"},  
  {name = "harp", filename = "harp 2.png"},  
  {name = "harp", filename = "harp 3.png"},  
  {name = "irish beer", filename = "irish beer.png"},  
  {name = "old phone", filename = "phone.png"},  
  {name = "piggy bank", filename = "rainbow piggy bank.png"},  
  {name = "pink glasses", filename = "pink glasses.png"},  
  {name = "pipe", filename = "pipe.png"},  
  {name = "pipe", filename = "pipe 2.png"},  
  {name = "pipe", filename = "pipe 3.png"},  
  {name = "pipe", filename = "pipe 4.png"},  
  {name = "pipe", filename = "pipe 5.png"},  
  {name = "pipe", filename = "pipe 6.png"},  
  {name = "potato", filename = "potato.png"},  
  {name = "potato", filename = "potato 2.png"},  
  {name = "potato", filename = "potato 3.png"},  
  {name = "potato chips", filename = "potato chips 6.png"},  
  {name = "potatoes", filename = "potatoes.png"},  
  {name = "potatoes", filename = "potatoes 2.png"},  
  {name = "potatoes", filename = "potatoes 3.png"},  
  {name = "potatoes", filename = "potatoes 4.png"},  
  {name = "red cabbage", filename = "red cabbage 2.png"},  
  {name = "red cabbage", filename = "red cabbage.png"},  
  {name = "red paint", filename = "red paint.png"},  
  {name = "scone", filename = "scone 2.png"},  
  {name = "scone", filename = "scone 3.png"},  
  {name = "scone", filename = "scone.png"},  
  {name = "soccer ball", filename = "soccer ball 9.png"},  
  {name = "soccer ball", filename = "soccer ball 8.png"},  
  {name = "tan hat", filename = "tan hat.png"},  
  {name = "rainbow umbrella", filename = "rainbow umbrella.png"},  
  {name = "rainbow umbrella", filename = "rainbow umbrella 2.png"},  
  
  {name = "bell", filename = "bell 3.png"}, 
  {name = "blue umbrella", filename = "blue umbrella 2.png"}, 
  {name = "boots", filename = "boots 2.png"}, 
  {name = "boots", filename = "boots.png"}, 
  {name = "cat", filename = "cat 3.png"}, 
  {name = "dog", filename = "chinese dog.png"}, 
  {name = "dragon lizard", filename = "dragon lizard.png"}, 
  {name = "fish", filename = "fish 5.png"}, 
  {name = "fish", filename = "fish 6.png"}, 
  {name = "fortune cookie", filename = "fortune cookie 3.png"}, 
  {name = "fortune cookie", filename = "fortune cookie 2.png"}, 
  {name = "fortune cookie", filename = "fortune cookie.png"}, 
  {name = "fortune cookies", filename = "fortune cookies 2.png"}, 
  {name = "fortune cookies", filename = "fortune cookies.png"}, 
  {name = "golden cat", filename = "gold cat.png"}, 
  {name = "knife", filename = "knife 2.png"}, 
  {name = "money tree", filename = "money tree.png"}, 
  {name = "orange umbrella", filename = "orange umbrella.png"}, 
  {name = "panda", filename = "panda bear 7.png"}, 
  {name = "panda", filename = "panda bear 6.png"}, 
  {name = "panda", filename = "panda bear 5.png"}, 
  {name = "panda", filename = "panda bear 4.png"}, 
  {name = "panda", filename = "panda bear 3.png"}, 
  {name = "panda", filename = "panda bear 2.png"}, 
  {name = "panda", filename = "panda bear.png"}, 
  {name = "passport", filename = "passport 6.png"}, 
  {name = "passport", filename = "passport 5.png"}, 
  {name = "pink umbrella", filename = "pink umbrella 2.png"}, 
  {name = "pink umbrella", filename = "pink umbrella.png"}, 
  {name = "rabbit", filename = "rabbit.png"}, 
  {name = "red bag", filename = "red bag 2.png"}, 
  {name = "red bag", filename = "red bag 3.png"}, 
  {name = "red umbrella", filename = "red umbrella 3.png"}, 
  {name = "red umbrella", filename = "red umbrella 2.png"}, 
  {name = "red umbrella", filename = "red umbrella.png"}, 
  {name = "shoes", filename = "shoes 5.png"}, 
  {name = "shoes", filename = "shoes 4.png"}, 
  {name = "shoes", filename = "shoes 3.png"}, 
  {name = "sword", filename = "sword 3.png"}, 
  {name = "sword", filename = "sword 4.png"}, 
  {name = "teapot", filename = "teapot 5.png"}, 
  {name = "teapot", filename = "teapot 4.png"}, 
  {name = "teapot", filename = "teapot 3.png"}, 
  {name = "teapot", filename = "teapot 2.png"}, 
  {name = "turtle", filename = "turtle 5.png"}, 
  {name = "umbrella", filename = "umbrella 3.png"}, 
  {name = "wooden sword", filename = "wooden sword.png"}, 
  {name = "yin yang symbol", filename = "yin yang symbol 4.png"}, 
  {name = "yin yang symbol", filename = "yin yang symbol 3.png"}, 
  {name = "yin yang symbol", filename = "yin yang symbol 2.png"}, 
  
  {name = "antlers", filename = "antlers 2.png"}, 
  {name = "antlers", filename = "antlers.png"}, 
  {name = "bear", filename = "bear 2.png"}, 
  {name = "blanket", filename = "blanket 4.png"}, 
  {name = "blue scarf", filename = "blue scarf 2.png"}, 
  {name = "blue scarf", filename = "blue scarf.png"}, 
  {name = "blue star", filename = "blue star 2.png"}, 
  {name = "blue star", filename = "blue star 3.png"}, 
  {name = "blue star", filename = "blue star 4.png"}, 
  {name = "blue star", filename = "blue star.png"}, 
  {name = "bow", filename = "bow.png"}, 
  {name = "cat", filename = "cat 4.png"}, 
  {name = "chocolate flower", filename = "chocolate flower.png"}, 
  {name = "chocolate snowman", filename = "chocolate snowman 2.png"}, 
  {name = "chocolate snowman", filename = "chocolate snowman.png"}, 
  {name = "cupcake", filename = "cupcake 2.png"}, 
  {name = "cupcake", filename = "cupcake 3.png"}, 
  {name = "cupcake", filename = "cupcake 4.png"}, 
  {name = "cupcake", filename = "cupcake 5.png"}, 
  {name = "cupcake", filename = "cupcake 6.png"}, 
  {name = "doll", filename = "doll 2.png"}, 
  {name = "doll", filename = "doll 3.png"}, 
  {name = "dove", filename = "dove.png"}, 
  {name = "dreidel", filename = "dreidel 2.png"}, 
  {name = "dreidel", filename = "dreidel 3.png"}, 
  {name = "dreidel", filename = "dreidel 4.png"}, 
  {name = "dreidel", filename = "dreidel 5.png"}, 
  {name = "dreidel", filename = "dreidel.png"}, 
  {name = "elf", filename = "elf 6.png"}, 
  {name = "elf", filename = "elf 7.png"}, 
  {name = "elf", filename = "elf 8.png"}, 
  {name = "elf", filename = "elf 9.png"}, 
  {name = "elf", filename = "elf 10.png"}, 
  {name = "elf hat", filename = "elf hat 5.png"}, 
  {name = "elf hat", filename = "elf hat 4.png"}, 
  {name = "elf hat", filename = "elf hat 3.png"}, 
  {name = "gingerbread house", filename = "gingerbread house.png"}, 
  {name = "green scarf", filename = "green scarf 2.png"}, 
  {name = "green scarf", filename = "green scarf.png"}, 
  {name = "holly wreath", filename = "holly wreath.png"}, 
  {name = "holly", filename = "holly.png"}, 
  {name = "hot chocolate", filename = "hot chocolate.png"}, 
  {name = "ice cream", filename = "ice cream 2.png"}, 
  {name = "ice cream", filename = "ice cream 3.png"}, 
  {name = "ice cream", filename = "ice cream 4.png"}, 
  {name = "ice cream", filename = "ice cream 5.png"}, 
  {name = "ice cream", filename = "ice cream 6.png"}, 
  {name = "ice cream", filename = "ice cream 7.png"}, 
  {name = "ice cream", filename = "ice cream.png"}, 
  {name = "menorah", filename = "menorah 2.png"}, 
  {name = "menorah", filename = "menorah 3.png"}, 
  {name = "menorah", filename = "menorah 4.png"}, 
  {name = "menorah", filename = "menorah 5.png"}, 
  {name = "menorah", filename = "menorah 6.png"}, 
  {name = "menorah", filename = "menorah.png"}, 
  {name = "milkshake", filename = "milkshake 2.png"}, 
  {name = "milkshake", filename = "milkshake 3.png"}, 
  {name = "milkshake", filename = "milkshake.png"}, 
  {name = "mitten", filename = "mitten 2.png"}, 
  {name = "mitten", filename = "mitten 3.png"}, 
  {name = "mitten", filename = "mitten 4.png"}, 
  {name = "mitten", filename = "mitten 5.png"}, 
  {name = "mittens", filename = "mittens.png"}, 
  {name = "oven mitt", filename = "oven mitt.png"}, 
  {name = "pear", filename = "pear 2.png"}, 
  {name = "penguin", filename = "penguin 2.png"}, 
  {name = "penguin", filename = "penguin 3.png"}, 
  {name = "penguin", filename = "penguin 4.png"}, 
  {name = "penguin", filename = "penguin 5.png"}, 
  {name = "penguin", filename = "penguin 6.png"}, 
  {name = "penguin", filename = "penguin 7.png"}, 
  {name = "penguin", filename = "penguin 8.png"}, 
  {name = "penguin", filename = "penguin 9.png"}, 
  {name = "penguin", filename = "penguin 10.png"}, 
  {name = "penguin", filename = "penguin 11.png"}, 
  {name = "penguin", filename = "penguin 12.png"}, 
  {name = "penguin", filename = "penguin 13.png"}, 
  {name = "pine cone", filename = "pine cone.png"}, 
  {name = "pine cones", filename = "pine cones 2.png"}, 
  {name = "pine cones", filename = "pine cones 3.png"}, 
  {name = "pine cones", filename = "pine cones 4.png"}, 
  {name = "pine cones", filename = "pine cones.png"}, 
  {name = "pink star", filename = "pink star.png"}, 
  {name = "polar bear", filename = "polar bear 2.png"}, 
  {name = "polar bear", filename = "polar bear 3.png"}, 
  {name = "polar bear", filename = "polar bear 4.png"}, 
  {name = "polar bear", filename = "polar bear 5.png"}, 
  {name = "polar bear", filename = "polar bear 6.png"}, 
  {name = "polar bear", filename = "polar bear 7.png"}, 
  {name = "popsicle", filename = "popsicle 2.png"}, 
  {name = "popsicle", filename = "popsicle 3.png"}, 
  {name = "popsicle", filename = "popsicle.png"}, 
  {name = "purple blanket", filename = "purple blanket.png"}, 
  {name = "purple bow", filename = "purple bow.png"}, 
  {name = "purple scarf", filename = "purple scarf.png"}, 
  {name = "purple snowflake", filename = "purple snowflake.png"}, 
  {name = "purple snowflake", filename = "purple snowflake 2.png"}, 
  {name = "purple star", filename = "purple star.png"}, 
  {name = "raspberry", filename = "raspberry.png"}, 
  {name = "red bell", filename = "red bell.png"}, 
  {name = "red flower", filename = "red flower 5.png"}, 
  {name = "red flower", filename = "red flower 6.png"}, 
  {name = "red flower", filename = "red flower 4.png"}, 
  {name = "reindeer", filename = "reindeer 8.png"}, 
  {name = "reindeer", filename = "reindeer 7.png"}, 
  {name = "reindeer", filename = "reindeer 6.png"}, 
  {name = "santa's bag", filename = "santas bag 2.png"}, 
  {name = "santa's bag", filename = "santas bag 3.png"}, 
  {name = "santa's bag", filename = "santas bag.png"}, 
  {name = "santa's boot", filename = "santas boot 2.png"}, 
  {name = "santa's boot", filename = "santas boot 3.png"}, 
  {name = "santa's boot", filename = "santas boot 4.png"}, 
  {name = "santa's boot", filename = "santas boot 5.png"}, 
  {name = "santa's boot", filename = "santas boot 6.png"}, 
  {name = "santa's boot", filename = "santas boot 7.png"}, 
  {name = "santa's boot", filename = "santas boot.png"}, 
  {name = "silver bell", filename = "silver bell 2.png"}, 
  {name = "silver bell", filename = "silver bell 3.png"}, 
  {name = "silver bell", filename = "silver bell.png"}, 
  {name = "silver bells", filename = "silver bells 2.png"}, 
  {name = "silver bells", filename = "silver bells.png"},
  {name = "silver star", filename = "silver star.png"},
  {name = "skier", filename = "skier.png"}, 
  {name = "snowball", filename = "snowball 3.png"}, 
  {name = "snowflake", filename = "snowflake 2.png"}, 
  {name = "snowflake", filename = "snowflake 3.png"}, 
  {name = "snowflake", filename = "snowflake 4.png"}, 
  {name = "snowflake", filename = "snowflake 5.png"}, 
  {name = "snowflake", filename = "snowflake 6.png"}, 
  {name = "snowflake", filename = "snowflake.png"}, 
--  {name = "snowman", filename = "snowman 16.png"}, 
  {name = "snowman", filename = "snowman 18.png"}, 
  {name = "snowman", filename = "snowman 19.png"}, 
  {name = "snowman", filename = "snowman 20.png"}, 
  {name = "snowman", filename = "snowman 6.png"}, 
  {name = "snowman", filename = "snowman 7.png"}, 
  {name = "snowman", filename = "snowman 8.png"}, 
  {name = "snowman", filename = "snowman 9.png"}, 
  {name = "snowman", filename = "snowman 10.png"}, 
  {name = "snowman", filename = "snowman 11.png"}, 
  {name = "snowman", filename = "snowman 12.png"}, 
  {name = "snowman", filename = "snowman 13.png"}, 
  {name = "snowman", filename = "snowman 14.png"}, 
  {name = "snowman", filename = "snowman 15.png"}, 
  {name = "snowman", filename = "snowman 17.png"}, 
  {name = "star cookie", filename = "star cookie 2.png"}, 
  {name = "stocking", filename = "stocking 2.png"}, 
  {name = "stocking", filename = "stocking 3.png"}, 
  {name = "stocking", filename = "stocking 4.png"}, 
  {name = "stocking", filename = "stocking 5.png"}, 
  {name = "stocking", filename = "stocking 6.png"}, 
  {name = "stocking", filename = "stocking 7.png"}, 
  {name = "stocking", filename = "stocking 8.png"}, 
  {name = "stocking", filename = "stocking 9.png"}, 
  {name = "stocking", filename = "stocking 10.png"}, 
  {name = "stocking", filename = "stocking 11.png"}, 
  {name = "stocking", filename = "stocking 12.png"}, 
  {name = "stocking", filename = "stocking 13.png"}, 
  {name = "stocking", filename = "stocking 14.png"}, 
  {name = "stocking", filename = "stocking 15.png"}, 
  {name = "stocking", filename = "stocking 16.png"}, 
  {name = "stocking", filename = "stocking 17.png"}, 
  {name = "stocking", filename = "stocking 18.png"}, 
  {name = "stocking", filename = "stocking 19.png"}, 
  {name = "stocking", filename = "stocking 20.png"}, 
  {name = "stocking", filename = "stocking 21.png"}, 
  {name = "stocking", filename = "stocking 22.png"}, 
  {name = "stocking", filename = "stocking 23.png"}, 
  {name = "strawberry", filename = "strawberry 2.png"}, 
  {name = "sundae", filename = "sundae 2.png"}, 
  {name = "teal scarf", filename = "teal scarf.png"}, 
  {name = "carrot", filename = "carrot.png"}, 
  {name = "corn", filename = "corn.png"}, 
  {name = "grapes", filename = "grapes.png"}, 
  {name = "gummy bear", filename = "gummy bear 2.png"}, 
  {name = "gummy bear", filename = "gummy bear.png"}, 
  {name = "monkey", filename = "monkey 2.png"}, 
  {name = "watermelon", filename = "watermelon.png"}, 
  {name = "heart", filename = "heart 13.png"}, 
  {name = "strawberry", filename = "strawberry 3.png"}, 
  {name = "strawberry", filename = "strawberry 4.png"}, 
  
  {name = "baseball hat", filename = "baseball hat 4.png"},
  {name = "bell", filename = "bell 2.png"}, 
  {name = "bell", filename = "bell 3.png"}, 
  {name = "bell", filename = "bell 4.png"}, 
  {name = "bell", filename = "bell 6.png"}, 
  {name = "bell", filename = "bell 7.png"}, 
  {name = "bowling ball", filename = "bowling ball 3.png"},
  {name = "candlestick", filename = "candlestick 2.png"}, 
  {name = "candlestick", filename = "candlestick 3.png"}, 
  {name = "candlestick", filename = "candlestick 5.png"}, 
  {name = "candlestick", filename = "candlestick 9.png"}, 
  {name = "candlestick", filename = "candlestick 10.png"}, 
  {name = "candlestick", filename = "candlestick.png"}, 
  {name = "straw hat", filename = "straw hat 2.png"}, 
  {name = "straw hat", filename = "straw hat 3.png"}, 
  {name = "strawberry jam", filename = "strawberry jam.png"}, 
  {name = "crown", filename = "crown 3.png"}, 
  {name = "goat", filename = "goat 2.png"},
  {name = "golf clubs", filename = "golf clubs 3.png"},
  {name = "golf clubs", filename = "golf clubs.png"},
  {name = "green donut", filename = "green donut.png"},
  {name = "orange donut", filename = "orange donut.png"},
  {name = "hot air balloon", filename = "hot air balloon 2.png"}, 
  {name = "hot air balloon", filename = "hot air balloon 3.png"}, 
  {name = "hot air balloon", filename = "hot air balloon 4.png"}, 
  {name = "hot air balloon", filename = "hot air balloon 5.png"}, 
  {name = "hot air balloon", filename = "hot air balloon 6.png"},
  {name = "pink donut", filename = "pink donut.png"}, 
  {name = "pitcher", filename = "pitcher 3.png"}, 
  {name = "radio", filename = "radio 2.png"}, 
  {name = "radio", filename = "radio 4.png"}, 
  {name = "radio", filename = "radio 5.png"}, 
  {name = "radio", filename = "radio 6.png"}, 
  {name = "radio", filename = "radio 7.png"}, 
  {name = "radio", filename = "radio 8.png"}, 
  {name = "radio", filename = "radio 9.png"}, 
  {name = "toy plane", filename = "toy airplane 3.png"}, 
  {name = "toy plane", filename = "toy airplane 4.png"}, 
  {name = "toy truck", filename = "truck 4.png"}, 
  {name = "toy truck", filename = "truck 5.png"}, 
  
  {name = "acorn", filename = "acorn 3.png"}, 
  {name = "acorn squash", filename = "acorn squash.png"}, 
  {name = "acorn", filename = "acorn 4.png"}, 
  {name = "apricot jam", filename = "apricot jam.png"}, 
  {name = "astronaut", filename = "astronaut 2.png"}, 
  {name = "astronaut", filename = "astronaut.png"}, 
  {name = "astronaut helmet", filename = "astronaut helmet 2.png"}, 
  {name = "astronaut helmet", filename = "astronaut helmet.png"}, 
  {name = "beehive", filename = "beehive.png"}, 
  {name = "bell", filename = "bell 5.png"}, 
  {name = "bell", filename = "bell8.png"}, 
  {name = "bike chain", filename = "bike chain.png"}, 
  {name = "bike seat", filename = "bike seat 4.png"}, 
  {name = "bike seat", filename = "bike seat 3.png"}, 
  {name = "bike seat", filename = "bike seat 2.png"}, 
  {name = "bike seat", filename = "bike seat.png"}, 
  {name = "black saddle", filename = "black saddle 2.png"}, 
  {name = "blue glove", filename = "blue glove.png"}, 
  {name = "blue rose", filename = "blue rose.png"}, 
  {name = "blue ruler", filename = "blue ruler 4.png"}, 
  {name = "blue ruler", filename = "blue ruler 3.png"}, 
  {name = "blue ruler", filename = "blue ruler 2.png"}, 
  {name = "blue ruler", filename = "blue ruler.png"}, 
  {name = "bowling ball", filename = "bowling ball 4.png"}, 
  {name = "bowling ball", filename = "bowling ball 5.png"}, 
  {name = "british pounds", filename = "british pounds 2.png"}, 
  {name = "earmuffs", filename = "brown ear muffs.png"}, 
  {name = "candlestick", filename = "candlestick 4.png"}, 
  {name = "candlestick", filename = "candlestick 6.png"}, 
  {name = "candlestick", filename = "candlestick 7.png"}, 
  {name = "candlestick", filename = "candlestick 8.png"}, 
  {name = "carousel horse", filename = "carousel horse 3.png"}, 
  {name = "cashew fruit", filename = "cashew fruit.png"}, 
  {name = "cherry jam", filename = "cherry jam.png"}, 
  {name = "cherry pie", filename = "cherry pie 2.png"}, 
  {name = "cherry pie", filename = "cherry pie.png"}, 
  {name = "cradle", filename = "cradle 2.png"}, 
  {name = "crane", filename = "crane 2.png"}, 
  {name = "crock pot", filename = "crock pot 3.png"}, 
  {name = "crown", filename = "crown 2.png"}, 
  {name = "crown", filename = "crown 4.png"},
  {name = "dinosaur", filename = "dinosaur 4.png"}, 
  {name = "doll", filename = "doll 4.png"}, 
  {name = "door knocker", filename = "door knocker.png"}, 
  {name = "frog", filename = "frog 2.png"}, 
  {name = "frog", filename = "frog 3.png"}, 
  {name = "frog", filename = "frog 4.png"}, 
  {name = "frog", filename = "frog 5.png"}, 
  {name = "george washington", filename = "george washington 2.png"}, 
  {name = "goat", filename = "goat 3.png"}, 
  {name = "green calculator", filename = "green calculator.png"}, 
  {name = "green boots", filename = "green rain boots.png"}, 
  {name = "green ruler", filename = "green ruler.png"}, 
  {name = "handprint", filename = "handprint 2.png"}, 
  {name = "handprint", filename = "handprint 3.png"}, 
  {name = "handprint", filename = "handprint 4.png"}, 
  {name = "handprint", filename = "handprint.png"}, 
  {name = "hot air balloon", filename = "hot air balloon 2.png"}, 
  {name = "ladybug", filename = "ladybug 2.png"}, 
  {name = "ladybug", filename = "ladybug 3.png"}, 
  {name = "lizard", filename = "lizard 3.png"}, 
  {name = "mouse", filename = "mouse 2.png"}, 
  {name = "orange calculator", filename = "orange calculator.png"}, 
  {name = "oven mitt", filename = "oven mitt 2.png"}, 
  {name = "pear", filename = "pear 3.png"}, 
  {name = "pear", filename = "pear 4.png"}, 
  {name = "pink earmuffs", filename = "pink ear muffs.png"}, 
  {name = "pink flower", filename = "pink flower 2.png"}, 
  {name = "pink purse", filename = "pink purse 2.png"}, 
  {name = "pink purse", filename = "pink purse.png"}, 
  {name = "pink rose", filename = "pink rose.png"}, 
  {name = "pink ruler", filename = "pink ruler.png"}, 
  {name = "plate", filename = "plate 4.png"}, 
  {name = "polar bear", filename = "polar bear 8.png"}, 
  {name = "polka dot purse", filename = "polka dot purse.png"}, 
  {name = "purple flower", filename = "purple flower.png"}, 
  {name = "radio", filename = "radio 3.png"}, 
  {name = "radio", filename = "radio.png"}, 
  {name = "rat", filename = "rat.png"}, 
  {name = "red calculator", filename = "red calculator.png"}, 
  {name = "red headphones", filename = "red headphones 2.png"}, 
  {name = "red headphones", filename = "red headphones.png"}, 
  {name = "red purse", filename = "red purse.png"}, 
  {name = "red boots", filename = "red rain boots.png"}, 
  {name = "red ruler", filename = "red ruler 2.png"}, 
  {name = "red ruler", filename = "red ruler.png"},
  {name = "rocking horse", filename = "rocking horse.png"}, 
  {name = "satellite dish", filename = "satellite dish 2.png"},
  {name = "red ruler", filename = "red ruler 2.png"}, 
  {name = "skunk", filename = "skunk 2.png"}, 
  {name = "skunk", filename = "skunk 3.png"}, 
  {name = "soap", filename = "soap 3.png"}, 
  {name = "soap", filename = "soap 4.png"}, 
  {name = "soap", filename = "soap 5.png"}, 
  {name = "soap", filename = "soap 6.png"}, 
  {name = "spider", filename = "spider 4.png"}, 
  {name = "spider", filename = "spider 3.png"}, 
  {name = "spider", filename = "spider 2.png"}, 
  {name = "spider", filename = "spider.png"}, 
  {name = "star cookie", filename = "star cookie 3.png"}, 
  {name = "straw hat", filename = "straw hat 6.png"}, 
  {name = "striped earmuffs", filename = "striped ear muffs.png"}, 
  {name = "thermometer", filename = "thermometer 3.png"}, 
  {name = "thermometer", filename = "thermometer.png"},
  {name = "tire", filename = "tire 3.png"}, 
  {name = "toy airplane", filename = "toy airplane 2.png"}, 
  {name = "toy train", filename = "toy train 3.png"}, 
  {name = "toy train", filename = "toy train 4.png"}, 
  {name = "toy train", filename = "toy train.png"}, 
  {name = "trash bag", filename = "trash bag 3.png"}, 
  {name = "trash bag", filename = "trash bag 2.png"}, 
  {name = "trash bag", filename = "trash bag.png"}, 
  {name = "truck", filename = "truck 6.png"}, 
  {name = "truck", filename = "truck 7.png"}, 
  {name = "truck", filename = "truck 8.png"},
  {name = "walnut", filename = "walnut 3.png"}, 
  {name = "white plate", filename = "white plate.png"}, 
  {name = "yellow boots", filename = "yellow rain boots.png"}, 
  {name = "yellow rose", filename = "yellow rose 2.png"}, 
  
  {name = "acorn", filename = "acorn 4.png"}, 
  {name = "acorn", filename = "acorn 5.png"}, 
  {name = "blimp", filename = "blimp 2.png"}, 
  {name = "blimp", filename = "blimp 3.png"}, 
  {name = "chicken", filename = "chicken 2.png"}, 
  {name = "cleat", filename = "cleat 2.png"}, 
  {name = "coffee grinder", filename = "coffee grinder 2.png"}, 
  {name = "crock pot", filename = "crock pot 4.png"}, 
  {name = "crock pot", filename = "crock pot 5.png"}, 
  {name = "crock pot", filename = "crock pot 6.png"}, 
  {name = "eraser", filename = "eraser 2.png"},
  {name = "eraser", filename = "eraser 3.png"}, 
  {name = "thermometer", filename = "thermometer 4.png"}, 
  {name = "almond nut", filename = "almond nut.png"}, 
  {name = "eraser", filename = "eraser 2.png"}, 
  {name = "british pounds", filename = "british pounds 3.png"}, 
  {name = "british pounds", filename = "british pounds 4.png"}, 
  {name = "grape jam", filename = "grape jam.png"}, 
  {name = "cradle", filename = "cradle 3.png"}, 
  {name = "doll", filename = "doll 5.png"}, 
  {name = "doll", filename = "doll 6.png"}, 
  {name = "doll", filename = "doll 7.png"}, 
  {name = "gray glove", filename = "gray glove.png"}, 
  {name = "koala", filename = "koala.png"}, 
  {name = "marmalade jam", filename = "marmalade jam.png"},
  {name = "mouse", filename = "mouse 3.png"}, 
  {name = "lizard", filename = "lizard 4.png"}, 
  {name = "mouse", filename = "mouse 4.png"}, 
  
  {name = "apple pie", filename = "apple pie 2.png"}, 
  {name = "apple pie", filename = "apple pie.png"}, 
  {name = "licorice", filename = "black licorice.png"}, 
  {name = "blue donut", filename = "blue donut.png"}, 
  {name = "brownie", filename = "brownie.png"}, 
  {name = "burrito", filename = "burrito.png"}, 
  {name = "cake", filename = "cake 2.png"}, 
  {name = "cake", filename = "cake2.png"},
  {name = "cake pop", filename = "cake pop 2.png"}, 
  {name = "cake pop", filename = "cake pop 3.png"}, 
  {name = "cake pop", filename = "cake pop 4.png"}, 
  {name = "cake pop", filename = "cake pop 5.png"}, 
  {name = "cake pop", filename = "cake pop 6.png"}, 
  {name = "cake pop", filename = "cake pop.png"}, 
  {name = "blue candy", filename = "candy 2.png"}, 
  {name = "gold candy", filename = "candy.png"}, 
  {name = "candy apple", filename = "candy apple 2.png"}, 
  {name = "candy apple", filename = "candy apple 3.png"}, 
  {name = "candy apple", filename = "candy apple.png"}, 
  {name = "candy cane", filename = "candy cane 2.png"}, 
  {name = "candy cane", filename = "candy cane.png"}, 
  {name = "cereal", filename = "cereal 2.png"}, 
  {name = "cereal", filename = "cereal.png"}, 
  {name = "cheeseburger", filename = "cheeseburger 2.png"}, 
  {name = "cheeseburger", filename = "cheeseburger 3.png"}, 
  {name = "cheeseburger", filename = "cheeseburger 4.png"}, 
  {name = "cheeseburger", filename = "cheeseburger 5.png"}, 
  {name = "cheeseburger", filename = "cheeseburger 6.png"}, 
  {name = "cheeseburger", filename = "cheeseburger.png"}, 
  {name = "chicken nuggets", filename = "chicken nuggets.png"}, 
  {name = "chocolate bar", filename = "chocolate bar 7.png"}, 
  {name = "chocolate bar", filename = "chocolate bar 8.png"}, 
  {name = "chocolate bar", filename = "chocolate bar 9.png"},
  {name = "chocolate cake", filename = "chocolate cake.png"}, 
  {name = "cookie", filename = "chocolate chip cookie.png"},
  {name = "chocolate donut", filename = "chocolate donut 2.png"}, 
  {name = "chocolate donut", filename = "chocolate donut.png"}, 
  {name = "chocolate muffin", filename = "chocolate muffin.png"}, 
  {name = "churros", filename = "churros 2.png"}, 
  {name = "churros", filename = "churros.png"}, 
  {name = "coleslaw", filename = "coleslaw.png"}, 
  {name = "curly fries", filename = "curly fries.png"}, 
  {name = "donut hole", filename = "donut hole 2.png"}, 
  {name = "donut hole", filename = "donut hole.png"}, 
  {name = "french fries", filename = "french fries 7.png"},
  {name = "french fries", filename = "french fries 8.png"},
  {name = "french fries", filename = "french fries 9.png"},
  {name = "french fries", filename = "french fries 10.png"},
  {name = "fried rice", filename = "fried rice.png"}, 
  {name = "frozen yogurt", filename = "frozen yogurt.png"}, 
  {name = "funnel cake", filename = "funnel cake.png"},
  {name = "gingerbread house", filename = "gingerbread house 2.png"}, 
  {name = "gingerbread man", filename = "gingerbread man.png"}, 
  {name = "glazed donut", filename = "glazed donut.png"}, 
  {name = "green candy", filename = "green candy.png"}, 
  {name = "green donut", filename = "green donut 2.png"}, 
  {name = "green macaroon", filename = "green macaroon.png"},
  {name = "gummy bear", filename = "gummy bear 3.png"}, 
  {name = "gummy cherries", filename = "gummy cherries.png"}, 
  {name = "gummy fish", filename = "gummy fish.png"}, 
  {name = "gummy frog", filename = "gummy frog.png"}, 
  {name = "gummy grapes", filename = "gummy grapes.png"}, 
  {name = "ice cream bar", filename = "ice cream bar.png"}, 
  {name = "ice cream cone", filename = "ice cream cone.png"}, 
  {name = "ice cream sandwich", filename = "ice cream sandwich.png"}, 
  {name = "jelly bean", filename = "jelly bean 2.png"}, 
  {name = "jelly bean", filename = "jelly bean 3.png"}, 
  {name = "jelly bean", filename = "jelly bean.png"}, 
  {name = "lemonade", filename = "lemonade.png"}, 
  {name = "lollipop", filename = "lollipop 2.png"}, 
  {name = "lollipop", filename = "lollipop 3.png"}, 
  {name = "lollipop", filename = "lollipop 4.png"}, 
  {name = "lollipop", filename = "lollipop 5.png"}, 
  {name = "lollipop", filename = "lollipop 6.png"}, 
  {name = "lollipop", filename = "lollipop 7.png"}, 
  {name = "lollipop", filename = "lollipop 8.png"}, 
  {name = "lollipop", filename = "lollipop.png"}, 
  {name = "macaroon", filename = "macaroon 001.png"}, 
  {name = "macaroon", filename = "macaroon 002.png"}, 
  {name = "macaroon", filename = "macaroon 003.png"}, 
  {name = "macaroon", filename = "macaroon 004.png"}, 
  {name = "muffin", filename = "muffin 2.png"}, 
  {name = "nachos", filename = "nachos 2.png"}, 
  {name = "nachos", filename = "nachos.png"}, 
  {name = "onion rings", filename = "onion rings 2.png"}, 
  {name = "onion rings", filename = "onion rings 3.png"}, 
  {name = "onion rings", filename = "onion rings.png"}, 
  {name = "orange candy", filename = "orange candy.png"}, 
  {name = "orange lollipop", filename = "orange lollipop.png"}, 
  {name = "peppermint candy", filename = "peppermint candy.png"}, 
  {name = "pink donut", filename = "pink donut 2.png"},
  {name = "pink lollipop", filename = "pink lollipop.png"}, 
  {name = "pink macaroon", filename = "pink macaroon 2.png"}, 
  {name = "pink macaroon", filename = "pink macaroon.png"}, 
  {name = "popsicle", filename = "popsicle 001.png"}, 
  {name = "popsicle", filename = "popsicle 002.png"}, 
  {name = "ice cream cone", filename = "popsicle 003.png"}, 
  {name = "popsicle", filename = "popsicle 004.png"}, 
  {name = "ice cream", filename = "popsicle 005.png"}, 
  {name = "popsicle", filename = "popsicle 006.png"}, 
  {name = "popsicle", filename = "popsicle 007.png"}, 
  {name = "ice cream bar", filename = "popsicle 008.png"}, 
  {name = "ice cream", filename = "popsicle 009.png"}, 
  {name = "popsicle", filename = "popsicle 0010.png"}, 
  {name = "popsicle", filename = "popsicle 0011.png"}, 
  {name = "potato chips", filename = "potato chips 001.png"}, 
  {name = "potato chips", filename = "potato chips 002.png"}, 
  {name = "potato chips", filename = "potato chips 003.png"}, 
  {name = "red lollipop", filename = "red lollipop 2.png"}, 
  {name = "soda can", filename = "soda can 2.png"}, 
  {name = "soda can", filename = "soda can 3.png"}, 
  {name = "soda can", filename = "soda can 4.png"}, 
  {name = "soda can", filename = "soda can 5.png"}, 
  {name = "soda can", filename = "soda can 6.png"}, 
  {name = "soda can", filename = "soda can.png"}, 
  {name = "sprinkled donut", filename = "sprinkled donut 2.png"},
  {name = "sprinkled donut", filename = "sprinkled donut 3.png"},
  {name = "sprinkled donut", filename = "sprinkled donut 4.png"},
  {name = "sprinkled donut", filename = "sprinkled donut.png"},
  {name = "sugar cubes", filename = "sugar cubes 2.png"}, 
  {name = "sugar cubes", filename = "sugar cubes.png"}, 
  {name = "taco", filename = "taco 2.png"}, 
  {name = "taco", filename = "taco.png"}, 
  {name = "macaroon", filename = "yellow macaroon.png"}, 
  
  {name = "black boots", filename = "black boots.png"}, 
  {name = "striped socks", filename = "blue striped socks.png"}, 
  {name = "gray gloves", filename = "gray gloves.png"}, 
  {name = "ice skates", filename = "ice skates 2.png"}, 
  {name = "ice skates", filename = "ice skates 3.png"}, 
  {name = "ice skates", filename = "ice skates 4.png"}, 
  {name = "mittens", filename = "mittens 2.png"}, 
  {name = "mittens", filename = "mittens 3.png"}, 
  {name = "orange boots", filename = "orange boots.png"}, 
  {name = "orange scarf", filename = "orange scarf.png"}, 
  {name = "pink boots", filename = "pink boots.png"}, 
  {name = "plaid blanket", filename = "plaid blanket.png"},
  {name = "heart", filename = "heart 14.png"}, 
  {name = "stocking", filename = "stocking 24.png"}, 
  
  {name = "baseball", filename = "baseball 2.png"}, 
  {name = "black bear", filename = "black bear.png"}, 
  {name = "brown bear", filename = "brown bear.png"}, 
  {name = "rubber ducky", filename = "blue rubber ducky.png"}, 
  {name = "crane", filename = "crane 3.png"}, 
  {name = "genie lamp", filename = "genie lamp.png"}, 
  {name = "picture frame", filename = "gold picture frame.png"}, 
  {name = "picture frame", filename = "green picture frame 2.png"}, 
  {name = "picture frame", filename = "green picture frame.png"}, 
  {name = "picture frame", filename = "picture frame.png"}, 
  {name = "picture frame", filename = "picture frame 2.png"}, 
  {name = "picture frame", filename = "picture frame 3.png"}, 
  {name = "picture frame", filename = "pink picture frame.png"}, 
  {name = "picture frame", filename = "pink picture frame 2.png"}, 
  {name = "picture frame", filename = "purple picture frame.png"}, 
  {name = "picture frame", filename = "red picture frame.png"}, 
  {name = "picture frame", filename = "teal picture frame.png"}, 
  {name = "frog", filename = "green frog.png"}, 
  {name = "iphone", filename = "iphone 2.png"},
  {name = "keyboard", filename = "keyboard.png"}, 
  {name = "paint palette 2", filename = "paint palette 2.png"}, 
  {name = "paint palette 2", filename = "paint palette 3.png"}, 
  {name = "paint palette 2", filename = "paint palette 4.png"}, 
  {name = "paint palette 2", filename = "paint palette 5.png"}, 
  {name = "paint palette 2", filename = "paint palette.png"}, 
  {name = "salt", filename = "salt.png"}, 
  {name = "shuttlecock", filename = "shuttlecock.png"}, 
  {name = "soccer ball", filename = "soccer ball 4.png"}, 
  {name = "telephone", filename = "telephone.png"}, 
  {name = "tennis ball", filename = "tennis ball 2.png"}, 
  {name = "umbrella", filename = "umbrella 4.png"}, 
  {name = "umbrella", filename = "umbrella 5.png"}, 
  {name = "umbrella", filename = "umbrella 6.png"}, 
  {name = "umbrella", filename = "umbrella 7.png"}, 
  {name = "van gogh painting", filename = "van gogh painting.png"}, 
  
  {name = "ant", filename = "ant 2.png"}, 
  {name = "cherry pie", filename = "cherry pie 4.png"}, 
  {name = "glasses", filename = "glasses 3.png"}, 
  {name = "harmonica", filename = "harmonica 3.png"}, 
  {name = "lizard", filename = "lizard 5.png"}, 
  {name = "passport", filename = "passport 3.png"}, 
  {name = "pie", filename = "pie 2.png"}, 
  {name = "pizza", filename = "pizza 5.png"}, 
  {name = "aardvark", filename = "aardvark.png"}, 
  {name = "anteater", filename = "anteater.png"}, 
  {name = "apple pie", filename = "apple pie 2.png"}, 
  {name = "beetle", filename = "beetle 2.png"}, 
  {name = "beetle", filename = "beetle.png"}, 
  {name = "Benjamin Franklin", filename = "Benjamin Franklin 2.png"}, 
  {name = "Benjamin Franklin", filename = "Benjamin Franklin.png"}, 
  {name = "blue tennis shoe", filename = "blue tennis shoe.png"}, 
  {name = "buddha statue", filename = "buddha statue 2.png"}, 
  {name = "buddha statue", filename = "buddha statue 3.png"}, 
  {name = "buddha statue", filename = "Buddha Statue.png"}, 
  {name = "cat statue", filename = "cat statue.png"}, 
  {name = "cherry", filename = "cherry 2.png"}, 
  {name = "cherry", filename = "cherry 3.png"}, 
  {name = "cherry pie", filename = "cherry pie 3.png"}, 
  {name = "cherry pie", filename = "cherry pie.png"}, 
  {name = "cherry", filename = "cherry.png"}, 
  {name = "cockroach", filename = "cockroach 2.png"}, 
  {name = "cockroach", filename = "cockroach.png"}, 
  {name = "dog", filename = "dog.png"}, 
  {name = "door mat", filename = "door mat 2.png"}, 
  {name = "door mat", filename = "door mat.png"}, 
  {name = "driver license", filename = "Driver License.png"}, 
  {name = "firefighter hat", filename = "firefighter hat 2.png"}, 
  {name = "firefighter hat", filename = "firefighter hat 3.png"}, 
  {name = "firefighter hat", filename = "firefighter hat.png"}, 
  {name = "flashlight", filename = "flashlight 2.png"}, 
  {name = "flashlight", filename = "flashlight 4.png"}, 
  {name = "flashlight", filename = "flashlight 5.png"}, 
  {name = "flashlight", filename = "flashlight.png"}, 
  {name = "flute", filename = "flute 2.png"}, 
  {name = "flute", filename = "flute 3.png"}, 
  {name = "flute", filename = "flute 4.png"}, 
  {name = "flute", filename = "Flute.png"}, 
  {name = "footprint", filename = "footprint 2.png"}, 
  {name = "footprint", filename = "footprint 3.png"}, 
  {name = "footprint", filename = "footprint.png"}, 
  {name = "gavel", filename = "gavel 2.png"}, 
  {name = "gavel", filename = "gavel 3.png"}, 
  {name = "gavel", filename = "gavel 4.png"}, 
  {name = "gavel", filename = "gavel.png"}, 
  {name = "glasses", filename = "Glasses 2.png"}, 
  {name = "golf bag", filename = "golf bag 2.png"}, 
  {name = "golf bag", filename = "golf bag.png"}, 
  {name = "golf tee", filename = "golf tee 2.png"}, 
  {name = "golf tee", filename = "golf tee 3.png"}, 
  {name = "golf tee", filename = "golf tee.png"}, 
  {name = "green apple", filename = "green apple 2.png"}, 
  {name = "green apple", filename = "green apple.png"}, 
  {name = "green tennis shoe", filename = "green tennis shoe.png"}, 
  {name = "guitar", filename = "guitar.png"}, 
  {name = "liberty bell", filename = "liberty bell.png"}, 
  {name = "lime", filename = "lime 2.png"}, 
  {name = "lime", filename = "lime.png"}, 
  {name = "lime slice", filename = "lime slice.png"}, 
  {name = "mango", filename = "mango 2.png"}, 
  {name = "mango", filename = "mango 3.png"}, 
  {name = "mango", filename = "mango.png"}, 
  {name = "monkey statue", filename = "monkey statue 001.png"}, 
  {name = "monkey statue", filename = "monkey statue 002.png"}, 
  {name = "monkey statue", filename = "monkey statue 003.png"}, 
  {name = "orange tennis shoe", filename = "orange tennis shoe.png"}, 
  {name = "passport", filename = "passport.png"}, 
  {name = "pizza slice", filename = "pizza slice.png"}, 
  {name = "pizza", filename = "pizza.png"}, 
  {name = "snake", filename = "snake 2.png"}, 
  {name = "snake", filename = "snake 3.png"}, 
  {name = "snake", filename = "snake 4.png"}, 
  {name = "snake", filename = "snake 5.png"}, 
  {name = "snake", filename = "snake 6.png"}, 
  {name = "snake", filename = "snake.png"}, 
  {name = "space shuttle", filename = "space shuttle 2.png"}, 
  {name = "space shuttle", filename = "space shuttle 3.png"}, 
  {name = "space shuttle", filename = "space shuttle.png"}, 
  {name = "striped scarf", filename = "striped scarf 2.png"}, 
  {name = "striped scarf", filename = "striped scarf.png"}, 
  {name = "sunglasses", filename = "sunglasses 2.png"}, 
  {name = "sunglasses", filename = "sunglasses.png"}, 
  {name = "yellow paper", filename = "yellow paper.png"}, 
};
 --{name = "watch", filename = "watch.png"}, 
 
 --TODO need to set thumbnails / level numbers before doing obj placement
--[8/17/15, 4:28:17 PM] Llc Apps: BG1 - BG 8 (Traditional)
--[8/17/15, 4:28:57 PM] Llc Apps: BG9 - BG16 (Chill scenes 1 - 8)
--[8/17/15, 4:29:44 PM] Llc Apps: BG17 - BG23 (Adventure levels 1 - 7)
--[8/17/15, 4:32:20 PM] Llc Apps: BG1, BG2, BG3, BG7, BG9, BG10, BG12, BG14 (Adventure levels 8 - 15)

--chill scene 1 word

local TEST_LEVEL = 24;
--TEST_LEVEL = (TEST_LEVEL + 8); --when doing chill
local fileIndex = TEST_LEVEL;
--fileIndex = (fileIndex - 8); -- chill
fileIndex = (8); --adventure
--2/4/6/8
--adventure/picture/word/collector
local TEST_GAME_TYPE = "adventure";

if (TEST_GAME_TYPE == "adventure") then
--  TEST_LEVEL = (TEST_LEVEL + 8);  
end

--contains all visuals
local children;

local listPages;
local currentPage;

local listObjects;

local scaleButton;
local scaleMode;

local currentDragX,currentDragY;

local function dragObject(e)
  if (e.phase == "began") then
    display.getCurrentStage():setFocus(e.target);
    e.target.isFocus = true;
    currentDragX = e.x;
    currentDragY = e.y;
    Runtime:dispatchEvent({name = "began_drag", target = e.target});
    
  elseif ((e.phase == "moved") and (e.target.isFocus == true)) then  
    if (scaleMode ~= true) then
        
      local deltaX = (e.x - currentDragX);
      local deltaY = (e.y - currentDragY);
      
      e.target.x = (e.target.x + deltaX);
      e.target.y = (e.target.y + deltaY);
      
      currentDragX = e.x;
      currentDragY = e.y;
      
      --Runtime:dispatchEvent({name = "check_collision", target = e.target});
    else
      local deltaY = (e.y - currentDragY);
      
      local mod = -0.02;
      
      if (deltaY > 0) then
        mod = 0.02;
      end
      
      e.target.xScale = (e.target.xScale + mod);
      e.target.yScale = (e.target.yScale + mod);
      
      currentDragY = e.y;
    end
    
    if (rotateMode == true) then
      e.target.rotation = (e.target.rotation + 2);
    end
    
  elseif ((e.phase == "ended") and (e.target.isFocus == true)) then
    Runtime:dispatchEvent({name = "stop_drag", target = e.target});
    display.getCurrentStage():setFocus(nil);
    e.target.isFocus = nil;
  end
  
  return true;

end

local function eraseObject(e)
  local object = e.target;
  
  for i = #listObjects, 1,-1 do
    if (object.name == listObjects[i].name) then
      display.remove(object);
      table.remove(listObjects,i);
      return;
    end
    
  end
  
  return true;
end

local function choseObject(e)
  if (e.phase == "ended") then
    local selected = e.target;
    
    local object = display.newImage(selected.filename);
    
    object.name = selected.name;
    object.filename = selected.filename;
    object.type = "one_state_button";
    object.dispatch = object.name;
    object.width = (selected.xScale * selected.width);
    object.height = (selected.yScale * selected.height);
    
--    object.xScale = (object.xScale * 0.65);
--    object.yScale = (object.yScale * 0.65);
    local ratio = object.width;
    
    if (object.height > object.width) then
      ratio = object.height;
    end
    
    object.xScale = (80 / ratio);
    object.yScale = (80 / ratio);
    
    object.x = _w;
    object.y = _h;
    
    object:addEventListener("touch", dragObject);
    object:addEventListener("tap", eraseObject);
    
    if (listObjects == nil) then
      listObjects = {};
    end
    
    backgroundGroup:insert(object);
    
    table.insert(listObjects, object);
    
    print("TOTAL OBJECTS : " .. #listObjects);
  end
  
  return true;
end

local function setCurrentPage()
  for i = 1, #listPages do
    if (i == currentPage) then
      listPages[i].isVisible = true;
    else
      listPages[i].isVisible = false;
    end
    
  end
end

local function greenButtonTouched(e)
  if (e.phase == "began") then
    display.getCurrentStage():setFocus(e.target);
    e.target.isFocus = true;
  elseif ((e.target.isFocus == true) and (e.phase == "ended")) then
    display.getCurrentStage():setFocus(nil);
    e.target.isFocus = nil;
    
    if (listObjectsGroup.isClosed ~= true)  then
      listObjectsGroup.isClosed = true;
      listObjectsGroup.y = -150;
      scaleButton.isVisible = false;
    else
      listObjectsGroup.isClosed = nil;
      listObjectsGroup.y = 0;
      scaleButton.isVisible = true;
    end
    
  end
  
  return true;
end

local function createPictureList()
  listPages = {};
  
  local imagesPerPage = 5;
  
  local xMod, yMod = 0,0;
  local xSpacing = 100;
  local yPos = (_H * 0.06);
  local initialX = (_W * 0.2);
  
  local function bgTouch(e) return true; end
  
  local bgRect = display.newRect(0,0,1100, 150);
  bgRect.x = _w;
  bgRect.y = 80;
  bgRect:addEventListener("touch", bgTouch);
  bgRect:addEventListener("tap", bgTouch);
  
  local rectButton = display.newRect(0,0,50,50);
  rectButton:setFillColor(0,255,0);
  rectButton.x = (_W * 0.95);
  rectButton.y = 150;
  rectButton:addEventListener("touch", greenButtonTouched);
  
  listObjectsGroup:insert(bgRect);
  listObjectsGroup:insert(rectButton);
  
  table.insert(listPages, display.newGroup());
  listObjectsGroup:insert(listPages[#listPages]);
--  OBJECT_DIRECTORY = "assets/images/game/objects/";
--local OBJECTS_LIST = 
  for i = 1, #OBJECTS_LIST do
    local filename = (OBJECT_DIRECTORY..OBJECTS_LIST[i].filename);
    local object = display.newImage(filename);
    if (object ~= nil) then
      
      object.x = (initialX + (xMod * xSpacing));
      object.y = yPos;
      object.filename = filename;
      object.name = OBJECTS_LIST[i].name;
      
      local ratio = object.width;
      if (object.height > ratio) then
        ratio = object.height;
      end
      
      object.xScale = (140 / ratio);
      object.yScale = (140 / ratio);
      
      xMod = (xMod + 1);
      
      object:addEventListener("touch",choseObject);
      
      listPages[#listPages]:insert(object);
      
      if (xMod > imagesPerPage) then
        xMod = 0;
        
        --if ((imagesPerPage + i) < 700) then
          table.insert(listPages, display.newGroup());
          listObjectsGroup:insert(listPages[#listPages]);
        --end
      end
      
      print(i);
    end
    
  end
  
  currentPage = 1;
  setCurrentPage();
end

local function saveData(e)
  if (e.phase == "ended") then
    print("saving");
    local fileIO = require("utility.fileParsing");
    
--    local levelIndex = TEST_LEVEL;
    
--    if (TEST_GAME_TYPE == "adventure") then
--      levelIndex = (levelIndex - 8);
--    end
    
    local saveFile = ("stage"..fileIndex..TEST_GAME_TYPE);
    fileIO.saveObjectPlacement(listObjects, saveFile);
    
    if (listObjects ~= nil) then
      print("TOTAL OBJECTS : " .. #listObjects);
    end
  end
end

local function page(e)
  if (e.target.name == "previous") then
    currentPage = (currentPage - 1);
    
    if (currentPage <= 0) then
      currentPage = #listPages;
    end
    
  else
    currentPage = (currentPage + 1);
    
    if (currentPage > #listPages) then
      currentPage = 1;
    end
    
  end
  
  setCurrentPage();
  return true;
end

local function createArrows()
  local arrow = display.newRect(0,0,75,75);
  arrow.x = 50;
  arrow.y = 50;
  arrow:setFillColor(255,0,0);
  arrow:addEventListener("tap", page);
  arrow.name = "previous";
  arrow.alpha = 0.5;
  
  local nextArrow = display.newRect(0,0,75,75);
  nextArrow.x = 1100;
  nextArrow.y = 50;
  nextArrow:setFillColor(255,0,0);
  nextArrow:addEventListener("tap", page);
  nextArrow.name = "next";
  nextArrow.alpha = 0.5;
  
  listObjectsGroup:insert(arrow);
  listObjectsGroup:insert(nextArrow);
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	children = self.view;
  listObjectsGroup = display.newGroup();
  backgroundGroup = display.newGroup();
  
  children:insert(backgroundGroup);
  children:insert(listObjectsGroup);
  
  --
  local background = display.newImage("assets/images/levels/BG"..TEST_LEVEL..".jpg");
  background.width = 1136;
  background.height = 768;
  background.xScale = xScale;
  background.yScale = yScale;
  background.x = _w;
  background.y = _h;
  backgroundGroup:insert(background);
  
  local function scaleTouch(e)
    --if (e.phase == "ended") then
      if (scaleMode ~= true) then
        scaleMode = true;
        e.target:setFillColor(0,0,0);
      else
        scaleMode = nil;
        e.target:setFillColor(255,255,255);
      end
      
    --end
    
    return true;
  end
  
  scaleButton = display.newRect(0,0, 100, 200);
  scaleButton.x = 1100;
  scaleButton.y = 700;
  scaleButton:addEventListener("tap", scaleTouch);
  listObjectsGroup:insert(scaleButton);
  
  local function rotateTouch(e)
      if (rotateMode ~= true) then
        rotateMode = true;
        e.target:setFillColor(0,0,0);
      else
        rotateMode = nil;
        e.target:setFillColor(255,255,255);
      end
    return true;
  end
  
  local rotateButton = display.newRect(0,0,100,200);
  rotateButton.x,rotateButton.y = 100, 700;
  rotateButton:addEventListener("tap", rotateTouch);
  listObjectsGroup:insert(rotateButton);
  
  createPictureList();
  createArrows();
  
  Runtime:addEventListener("touch", saveData);
  --]]
  
  --ITEM COUNT
  --[[
  local usedObjects = {};
  
  local assetManager = require("utility.assetManager");
  
  local bannedList = {};
  
  --traditional
  for i = 1, 8 do
    --listItems = assetManager.getLevelItems("stages/"..modeChosen.."/stage"..(sceneChosen or levelChosen)..typeChosen, backgroundGroup, typeChosen);
    local tempGroup1 = display.newGroup();
    local tempGroup2 = display.newGroup();
    local tempGroup3 = display.newGroup();
    
    print("TRAD", "LEVEL " .. i);
    
    local listItems = assetManager.getLevelItems("stages/".."traditional".."/stage"..(i).."picture", tempGroup1, "picture");
    local listWordItems = assetManager.getLevelItems("stages/".."traditional".."/stage"..(i).."word", tempGroup2, "word");
    local listCollectorItems = assetManager.getLevelItems("stages/".."traditional".."/stage"..(i).."collector", tempGroup3, "collector");
    
    for i,v in pairs(listItems) do
      if (usedObjects[v.filename] == nil) then
        usedObjects[v.filename] = 0;
      else
        usedObjects[v.filename] = (usedObjects[v.filename] + 1);
      end
    
      if ((v.name == "wine") or (v.name == "beer") or (v.name == "bottle of beer") or (v.name == "cigar")) then
        table.insert(bannedList,"stages/".."traditional".."/stage"..(i).."picture");
      end
      
    end
    
    for i,v in pairs(listWordItems) do
      if (usedObjects[v.filename] == nil) then
        usedObjects[v.filename] = 0;
      else
        usedObjects[v.filename] = (usedObjects[v.filename] + 1);
      end
      
      if ((v.name == "wine") or (v.name == "beer") or (v.name == "bottle of beer") or (v.name == "cigar")) then
        table.insert(bannedList,"stages/".."traditional".."/stage"..(i).."word");
      end
    end
    
    for i,v in pairs(listCollectorItems) do
      if (usedObjects[v.filename] == nil) then
        usedObjects[v.filename] = 0;
      else
        usedObjects[v.filename] = (usedObjects[v.filename] + 1);
      end
      
      if ((v.name == "wine") or (v.name == "beer") or (v.name == "bottle of beer") or (v.name == "cigar")) then
        table.insert(bannedList,"stages/".."traditional".."/stage"..(i).."collector");
      end
    end
    display.remove(tempGroup1);
    display.remove(tempGroup2);
    display.remove(tempGroup3);
  end
  
  --chill
  for i = 1, 8 do
    --listItems = assetManager.getLevelItems("stages/"..modeChosen.."/stage"..(sceneChosen or levelChosen)..typeChosen, backgroundGroup, typeChosen);
    local tempGroup1 = display.newGroup();
    local tempGroup2 = display.newGroup();
    local tempGroup3 = display.newGroup();
    
    print("CHILL", "LEVEL " .. i);
    
    local listItems = assetManager.getLevelItems("stages/".."chill".."/stage"..(i).."picture", tempGroup1, "picture");
    local listWordItems = assetManager.getLevelItems("stages/".."chill".."/stage"..(i).."word", tempGroup2, "word");
    local listCollectorItems = assetManager.getLevelItems("stages/".."chill".."/stage"..(i).."collector", tempGroup3, "collector");
    
    for i,v in pairs(listItems) do
      if (usedObjects[v.filename] == nil) then
        usedObjects[v.filename] = 0;
      else
        usedObjects[v.filename] = (usedObjects[v.filename] + 1);
      end
      
      if ((v.name == "wine") or (v.name == "beer") or (v.name == "bottle of beer") or (v.name == "cigar")) then
        table.insert(bannedList,"stages/".."chill".."/stage"..(i).."picture");
      end
    end
    
    for i,v in pairs(listWordItems) do
      if (usedObjects[v.filename] == nil) then
        usedObjects[v.filename] = 0;
      else
        usedObjects[v.filename] = (usedObjects[v.filename] + 1);
      end
      
      if ((v.name == "wine") or (v.name == "beer") or (v.name == "bottle of beer") or (v.name == "cigar")) then
        table.insert(bannedList,"stages/".."chill".."/stage"..(i).."word");
      end
    end
    
    for i,v in pairs(listCollectorItems) do
      if (usedObjects[v.filename] == nil) then
        usedObjects[v.filename] = 0;
      else
        usedObjects[v.filename] = (usedObjects[v.filename] + 1);
      end
      
      if ((v.name == "wine") or (v.name == "beer") or (v.name == "bottle of beer") or (v.name == "cigar")) then
        table.insert(bannedList,"stages/".."chill".."/stage"..(i).."collector");
      end
    end
    
    display.remove(tempGroup1);
    display.remove(tempGroup2);
    display.remove(tempGroup3);
  end
  
  --adventure
  for i = 1, 15 do
    --listItems = assetManager.getLevelItems("stages/"..modeChosen.."/stage"..(sceneChosen or levelChosen)..typeChosen, backgroundGroup, typeChosen);
    print("ADV", "LEVEL " .. i);
    local tempGroup1 = display.newGroup();
    local listItems = assetManager.getLevelItems("stages/".."adventure".."/stage"..(i).."adventure", tempGroup1, "adventure");
    
    for i,v in pairs(listItems) do
      print(v.filename);
      if (usedObjects[v.filename] == nil) then
        usedObjects[v.filename] = 0;
      else
        usedObjects[v.filename] = (usedObjects[v.filename] + 1);
      end
      
      if ((v.name == "wine") or (v.name == "beer") or (v.name == "bottle of beer") or (v.name == "cigar")) then
        table.insert(bannedList,"stages/".."adventure".."/stage"..(i).."");
      end
    end
    
    display.remove(tempGroup1);
  end
  
  local freeObjects = {};
  
  for i = 1, #OBJECTS_LIST do
    if (usedObjects["assets/images/objects/"..OBJECTS_LIST[i].filename] == nil) then
      freeObjects[#freeObjects+1] = ("assets/images/objects/"..OBJECTS_LIST[i].filename);
      
    end
    
  end
  
  print("FREE OBJECTS");
  print(#freeObjects);
  local fileIO = require("utility.fileParsing");
  
  for i = 1, #freeObjects do
    print(freeObjects[i]);
    if (fileIO.getIfFileExists(freeObjects[i], system.ResourceDirectory) == true) then
      os.remove(system.pathForFile(freeObjects[i], system.ResourceDirectory)); 
    end
    
    
  end
  
  print("BANNED");
  
  for i = 1, #bannedList do
    print(bannedList[i]);
  end
  
  --]]
end


-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
	
end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	
end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	
end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
	
end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
	
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene;