
-- config.lua

application =
{
    license =
    {
        google =
        {
            --put google key between the quotes below: key is obtained from the Google Play Developers Console under All applications>Your App>Services & APIs>LICENSING & IN-APP BILLING (note that this key can be fairly big)
            key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5uDcuLOqaWt1yZVqgA/FEfYgNaames84vSI2TBOyawG2cnLhAKqwZR/228bGZ7xwP6q3XFUfHrCT1bgJHovn+ExTb/ZWYU9dY2Nut76y9SKKVzuHQ0g+QmPTbTfLXHrk/kmCInvnX/7CfGM38t2F4yF7xWaEDcHq5w8EVSPrENgYMoqNEgbgYelbfwz2RThjaFF7c06MVJF9gTs+TTKa7JGi20WkaUbqt1SawCy9J9BrimEenyTJFowA2Qt02gsknT3UukUudZhvs1O+svSpah8jHr5hveOFGt8qO8yF3DStTgW9HrB6yuUMapr0GVQ1EXnVOuCP23YCM5HUtuEipwIDAQAB",
        },
    },
    content =
    {
           -- graphicsCompatibility = 1,  --this turns on V1 Compatibility mode
            width = 768,
            height = 1136,
            scale = "zoomStretch",
            fps=30,
			
			imageSuffix =
			{
				--["@2x"] = 1.5,
				--["@4x"] = 4,
			},
    },
}


