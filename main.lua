--Initial Settings
display.setStatusBar(display.HiddenStatusBar);

local fileIO = require("utility.fileParsing");
_G.isAmazon = false;
if (system.getInfo("platform") == "android") then
    if (system.getInfo("targetAppStore") == "amazon") then
        _G.isAmazon = true;
    else
        _G.isAmazon = false;
    end
end    
_G.isAmazon = true;


if (("simulator" == system.getInfo("environment"))) then
  fileIO.deleteUserData();
end

fileIO.getUserData();

ADS_INITIALIZED = false;

--true == free store
--false == in-app purchasing
FREE_VERSION = false;
_G.IS_IAP_ON = true;
if (FREE_VERSION == true) then
    _G.IS_IAP_ON = false;
end

GAME_UNLOCKED = false;

if (FREE_VERSION == true) then
  GAME_UNLOCKED = true;
end

--add url for more games here between quotes
if (system.getInfo("platform") == "ios") then
    --add url for more games here between quotes
    MORE_GAMES_LINK = "http://apps.apple.com/us/developer/detention-apps/id999167300";
else
    if (_G.isAmazon == true) then
        MORE_GAMES_LINK = "https://www.amazon.com/s?i=mobile-apps&rh=p_4%3ABrainfull";
    else
        MORE_GAMES_LINK = "https://play.google.com/store/apps/dev?id=7426371415851456012";
    end
end

if (system.getInfo("platform") == "ios") then
    _G.UNLOCK_GAME_PRICE = "$3.99";
else
    if (_G.isAmazon == true) then
        _G.UNLOCK_GAME_PRICE = "$4.99";
    else
        _G.UNLOCK_GAME_PRICE = "$1.99";
    end
    
end


if (system.getInfo("platform") == "ios") then
    _G.STORE_PRODUCTS =
    {
      --all levels ID
      --no ads ID (if applicable)
      
      --example (remove this when inputting real id's)
      "com_detentiongames_hiddenobjectsspringgarden_megahugeobjectspack",
      --end example (remove when inputting real id's)
    };
else
    if (_G.isAmazon == true) then    
        _G.STORE_PRODUCTS =
        {
          --all levels ID
          --no ads ID (if applicable)
          
          --example (remove this when inputting real id's)
            "com_detentiongames_hiddenobjectsspringgarden_megahugeobjectspack",
          --end example (remove when inputting real id's)
        };
    else
        _G.STORE_PRODUCTS =
        {
          --all levels ID
          --no ads ID (if applicable)
          
          --example (remove this when inputting real id's)
            "com_detentiongames_hiddenobjectsspringgarden_megahugeobjectspack",
          --end example (remove when inputting real id's)
        };
    end
    
end


if (FREE_VERSION ~= true) then
  local store = require("store.storeSystem");  
  if (system.getInfo("platform") == "ios") then
    store.initializeApple();
    else
        if (_G.isAmazon == true) then            
            store.initializeAmazon();
        else
            store.initializeGoogle();
        end
    end;
end

--##MENU DEMO BUTTON
_G.ENABLE_DEMO_GAME_BUTTON = true;
if (_G.isAmazon == true) then
	_G.ENABLE_DEMO_GAME_BUTTON = false;
end;

if (system.getInfo("platform") == "ios") then
    _G.DEMO_GAME_URL = "http://apps.apple.com/us/developer/detention-apps/id999167300";
else
    if (_G.isAmazon == true) then
        _G.DEMO_GAME_URL = "https://www.amazon.com/s?i=mobile-apps&rh=p_4%3ADetention+Apps&search-type=ss";
    else
        _G.DEMO_GAME_URL = "http://apps.apple.com/us/developer/detention-apps/id999167300";
    end
end

--##ADVENTURE MODE DEMO BUTTON
_G.ENABLE_DEMO_GAME_BUTTON2 = true;
if (system.getInfo("platform") == "ios") then
    _G.DEMO_GAME_URL2 = "https://apps.apple.com/us/app/hidden-object-travel-quest-usa/id1259328096";
else
    if (_G.isAmazon == true) then
        _G.DEMO_GAME_URL2 = "https://www.amazon.com/Hidden-Objects-Road-Trip-USA/dp/B073X4VC3Z/";
    else
        _G.DEMO_GAME_URL2 = "https://apps.apple.com/us/app/hidden-object-travel-quest-usa/id1259328096";
    end
end

--_G.FUSE_INT_ID = "";
--local fuse = require("fuse");
--fuse.initialize();

--uncomment the two lines of an ad type to use
--local iads = require("iAds.iads");
--iads.initialize();

--local chartboost = require("chartboost.chartboostSystem");
--chartboost.initialize(_userPurchasedAds);
if (system.getInfo("platform") == "ios") then
    _G.WATERFALL_ORDER =
    {
         {
    adType = "applovin", 
    INT_ID = "kyNwmH7leQ_s8k-gI4fiBNUxQNCFMf95bW_Lp75SX4IgoHL5L_pXNHSpff20JB5vi5FWCKzlPGPCXmHum4P8Mt",
  },
  
  {
    adType = "admob", 
    INT_ID = "ca-app-pub-2712579443424568/5265358737",
  --BANNER_ID = "",
  },
    }
else
    if (_G.isAmazon == true) then
        _G.WATERFALL_ORDER =
        {
          {
            adType = "applovin", 
            INT_ID = "kyNwmH7leQ_s8k-gI4fiBNUxQNCFMf95bW_Lp75SX4IgoHL5L_pXNHSpff20JB5vi5FWCKzlPGPCXmHum4P8Mt",
          }              
        }
    else
       _G.WATERFALL_ORDER =
        {
          {
            adType = "applovin", 
            INT_ID = "kyNwmH7leQ_s8k-gI4fiBNUxQNCFMf95bW_Lp75SX4IgoHL5L_pXNHSpff20JB5vi5FWCKzlPGPCXmHum4P8Mt",
          },
          
          {
            adType = "admob", 
            INT_ID = "ca-app-pub-2712579443424568/2974557537",
        --    BANNER_ID = "",
          },
        }
    end
end;

local waterfall = require("adWaterfall.waterfall");
waterfall.initialize();
--_G.ADMOB_INTERSTITIALS_ID = "ca-app-pub-2712579443424568/2311892335";
----_G.ADMOB_BANNER_ID = "";
--local admob = require("admob.admob");
--admob.initialize();

--uncomment below to disable printing
--print = function() end

_G.TRADITIONAL_SET =
{
  1,--level1
  2,--level2
  3,--level3
  4,--level4
  
  5,--level5
  6,--level6
  7,--level7
  8,--level8
};

_G.CHILL_SET = 
{
  9,--level1
  10,--level2
  11,--level3
  12,--level4
  
  13,--level5
  14,--level6
  15,--level7
  16,--level8
};

_G.ADVENTURE_SET = 
{
  17,--level1
  18,--level2
  19,--level3
  20,--level4
  21,--level5
  22,--level6
  23,--level7
  24,--level8
  25,--level9
  26,--level10
  27,--level11
  28,--level12
  29,--level13
  30,--level14
  31,--level15
--  16,--level15
--  17,--level16
--  18,--level17
--  19,--level19
--  20,--level20
--  21,--level21
};

_G.MUSIC_MUTED = false;
_G.SOUND_MUTED = false;

--locals
local storyboard = require("storyboard");
local sceneDir = "scenes.";

--enabled/disables certain things depending if testing or releasing.  false if intending to be release mode
_testMode = false;

--globals
_currentScene, _previousScene = nil,nil;

local isTransitioning;

--global methods
function changeScene(strScene, params)
  if (isTransitioning == true) then return; end
  isTransitioning = true;
  
  local options = {};
  options.params = params;
  
	_previousScene = (_currentScene or strScene);
  
	storyboard.gotoScene(sceneDir .. tostring(strScene), options);
	
	_currentScene = strScene;
  timer.performWithDelay(100, function()
    isTransitioning = false;
  end);
end

local function onSystemEvent( event ) 
    if (event.type == "applicationStart") then
--        local gameCenter = require("gamecenter.GameCenter");
--        gameCenter.initialize();
        return true
    elseif (event.type == "applicationResume") then
        native.setActivityIndicator(false);
        Runtime:dispatchEvent({name = "startChartboostSession"});
        Runtime:dispatchEvent({name = "loadIntAd"});
        
        if (_currentScene == "scene_gameplay") then
          print("GRAMEPLAY");
          --Runtime:dispatchEvent({name = "refresh_images"});
        else
          --changeScene("scene_menu");
        end
        --local fileIO = require("utility.fileIO");
--        changeScene("scene_menu");
    elseif (event.type == "applicationSuspend") then
      --native.requestExit();
      --changeScene("scene_menu");
    end
end

local function displayFonts()
  local fonts = native.getFontNames()
  for i,fontname in ipairs( fonts ) do
    print(fontname);
  end
end

--local function onKeyReceived( event )
--   if event.phase == "down" and event.keyName == "back" then
--    system.request( "suspendApplication" )
--    return true
--   else
----    system.request( "resumeApplication" )

--   end
--end
--Runtime:addEventListener( "key", onKeyReceived )

--local LEVEL_TEST_DATA = 
--{
--  traditional = {1,2,3,4,5,6,7,8,},
--  chill = {1,2,3,4,5,6,7,8,},
--  adventure = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,},
--};

--local currentMode = "traditional";
--local currentLevel = 1;
local TEST_QUEUE = 
{
--  {scene = "scene_menu", sceneData = {}},
  
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "picture", level = 1}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "word", level = 1}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "collector", level = 1}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "picture", level = 2}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "word", level = 2}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "collector", level = 2}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "picture", level = 3}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "word", level = 3}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "collector", level = 3}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "picture", level = 4}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "word", level = 4}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "collector", level = 4}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "picture", level = 5}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "word", level = 5}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "collector", level = 5}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "picture", level = 6}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "word", level = 6}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "collector", level = 6}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "picture", level = 7}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "word", level = 7}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "collector", level = 7}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "picture", level = 8}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "word", level = 8}},
  {scene = "scene_gameplay", sceneData = {mode = "traditional", gameType = "collector", level = 8}},
  
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "picture", level = 1}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "word", level = 1}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "collector", level = 1}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "picture", level = 2}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "word", level = 2}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "collector", level = 2}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "picture", level = 3}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "word", level = 3}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "collector", level = 3}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "picture", level = 4}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "word", level = 4}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "collector", level = 4}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "picture", level = 5}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "word", level = 5}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "collector", level = 5}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "picture", level = 6}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "word", level = 6}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "collector", level = 6}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "picture", level = 7}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "word", level = 7}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "collector", level = 7}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "picture", level = 8}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "word", level = 8}},
  {scene = "scene_gameplay", sceneData = {mode = "chill", gameType = "collector", level = 8}},
  
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 1}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 2}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 3}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 4}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 5}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 6}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 7}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 8}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 9}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 10}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 11}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 12}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 13}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 14}},
  {scene = "scene_gameplay", sceneData = {mode = "adventure", gameType = "word", level = 15}},
};

local currentQ = 1;

local function TESTSCENES()
--  local level = LEVEL_TEST_DATA[currentMode][currentLevel];
  changeScene("scene_menu");
  
  timer.performWithDelay(2000, function()--mode = "adventure", gameType = "word", level = levelNum
    changeScene(TEST_QUEUE[currentQ].scene, TEST_QUEUE[currentQ].sceneData);
    
    currentQ = (currentQ + 1);
    
    if (currentQ <= #TEST_QUEUE) then
      timer.performWithDelay(2000, function()
      TESTSCENES();
      end);
    end
    
--    changeScene("scene_gameplay",{mode = currentMode, gameType = "picture", level = currentLevel});
    
--    timer.performWithDelay(2000, function()
--      changeScene
--      if (currentMode ~= "adventure") then
        
--      end
      
--    end);
  end);
end

local function main()
	Runtime:addEventListener( "system", onSystemEvent );
  
  if (FREE_VERSION == true) then
    --ensure everything is unlocked
--    local userData = fileIO.getUserData();
--    userData.game_unlocked.unlocked = true;
--    fileIO.saveShopData(userData);
  end
	
	local soundManager = require("sound.soundManager");
	soundManager.initialize();
  
  if (_testMode == false) then
    changeScene("scene_menu");
    
    --TEST (COMMENT ABOVE SCENE CHANGE)
--    TESTSCENES();
  else
    changeScene("scene_objectplacement");
  end
  
  --native.showAlert("Cannot", "Endo main", {"Okay"});
  --displayFonts();
end

main();