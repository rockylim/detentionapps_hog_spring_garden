module(..., package.seeall);

--publisher id
local APP_ID = _G.ADMOB_INTERSTITIALS_ID;
local BANNER_ID = _G.ADMOB_BANNER_ID;

local TIME_TIL_AUTO_CLOSE = 6000;

local admob;

local handleCloseAd;

local hasBanner,hasInt;

--forward declaration
--local showInterstitial = nil;
--local showBannerAd = nil;
--local hideAd = nil;
--local stopAds = nil;
--local loadIntAd = nil;

local currentBannerY;
local showIntOnLoad;
local showBannerOnLoad;

local initFailed = true;

local function adListener(e)
	print("Result of AdMob Call : " .. tostring(e.response) .. " " .. tostring(e.phase));
  if ( e.phase == "init" ) then  -- Successful initialization
      print( e.provider )
      initFailed = nil;
      
      if (hasBanner == true) then
        print("LOADING BANNER");
        admob.load( "banner", { adUnitId=BANNER_ID} );
      end
      
      if (hasInt == true) then
        print("LOADING INT");
        admob.load( "interstitial", { adUnitId=APP_ID, childSafe=true } )
      end
      
      
      
      
  elseif (e.phase == "loaded") then
    if ((e.type == "banner") and (showBannerOnLoad == true)) then
      if (hasBanner ~= true) then showBannerOnLoad = nil; return; end
      
      showBannerOnLoad = nil;
      print("SHOWING ON LOAD ".. tostring(currentBannerY));
      admob.show( "banner",{y=currentBannerY} );
      currentBannerY = nil;
      
    elseif (showIntOnLoad == true) then
      showIntOnLoad = nil;
      if (hasInt ~= true) then return; end
      admob.show( "interstitial" );
    end
    
--    "banner" and "interstitial".
  elseif ((e.phase == "closed")) then
    print("CLOSED " .. e.type);
    Runtime:dispatchEvent({name = "adShown"});
    
    if (e.type == "banner") then
--      admob.load( "banner", { adUnitId=BANNER_ID} );
    else
      admob.load( "interstitial", { adUnitId=APP_ID, childSafe=true } )
    end
    
  elseif (e.phase == "failed") then
    showIntOnLoad = nil;
    
    if (e.phase == "init") then return; end
    if (initFailed == true) then return; end
    
    --don't fail, just clear waterfall since admob is last
    --to resolve issue with multiple ads on error during loading int on init
    Runtime:dispatchEvent({name = "adShown"}); 
--    Runtime:dispatchEvent({name = "adFailed"});
    
    if (hasInt ~= true) then return; end
--    admob.load( "interstitial", { adUnitId=APP_ID, childSafe=true } )

--    Runtime:dispatchEvent({name = "showInterstitial"});
--    Runtime:dispatchEvent({name = "adFailed"});
  end
  
--elseif (e.phase == "failed") then
  
  print("AD RESPONSES");
  print(tostring(e.type), tostring(e.phase));
end

--local function getIfUnlocked()
--  local fileIO = require("utility.fileParsing");
--  local data = fileIO.getUserData();
  
--  return (data.game_unlocked.unlocked == true);
--end

function initialize(intID, bannerID)
--  if (getIfUnlocked() == true) then return; end
  
  APP_ID = intID;
  BANNER_ID = bannerID;
  
  _G.ADS_INITIALIZED = true;
	admob = require( "plugin.admob" );
  
  if ((APP_ID ~= nil) and (APP_ID ~= "")) then
    hasInt = true;
  end
  
  if ((bannerID ~= nil) and (bannerID ~= "")) then
    hasBanner = true;
  end
  
  if (hasInt == true) then
    admob.init( adListener, { appId=APP_ID, testMode=false } )
  elseif (hasBanner == true) then--if ((intID == nil) or (intID == "")) then
    admob.init( adListener, { appId=BANNER_ID, testMode=false } )
  end
  
  
  
--	Runtime:addEventListener("showInterstitial", showInterstitial);
--	Runtime:addEventListener("showBannerAd", showBannerAd);
--	Runtime:addEventListener("hideAd", hideAd);
--	Runtime:addEventListener("no_ads", stopAds);
--  Runtime:addEventListener("loadIntAd", loadIntAd);
end

--loadIntAd = function(e)
function loadIntAd(e)
--  if (_G.playerData.purchased == 1) then return; end
--  if (getIfUnlocked() == true) then return; end
  
--  pcall(function()
--      if (admobInterstitial.isLoaded("interstitial") == false) then
--        admobInterstitial.load("interstitial", {appId=APP_ID, testMode=false});
--      end
--  end);
end

--showInterstitial = function(e)
function showInterstitial()
--  if (getIfUnlocked() == true) then return; end
  if (hasInt ~= true) then 
    Runtime:dispatchEvent({name = "adFailed"});
    return; 
  end
  
  showIntOnLoad = true;
  
	if ( admob.isLoaded("interstitial") ) then
    showIntOnLoad = nil;
    admob.show( "interstitial" );
    return;
  else
    admob.load( "interstitial", { adUnitId=APP_ID, childSafe=true } )
  end
end

--showBannerAd = function(e)
function showBannerAd(isBottom)
--  if (getIfUnlocked() == true) then return; end
  if (hasBanner ~= true) then return; end
  
  hideAd();
  local adY = 0;
  adY = "top";
  
  if (isBottom == true) then
    adY = (display.contentHeight - display.screenOriginY);
    adY = "bottom";
  end
  
  currentBannerY = adY;
  showBannerOnLoad = true;
  
  if ( admob.isLoaded("banner") ) then
    showBannerOnLoad = nil;
    print("SHOWING BANNER @ : " .. adY);
    admob.show( "banner",{ y=adY } );
    
    return;
  else
    admob.load( "banner", { adUnitId=BANNER_ID} );
  end
  
end

--hideAd = function(e)
function hideAd()
	admob.hide();
end

stopAds = function(e)
--  Runtime:removeEventListener("showInterstitial", showInterstitial);
--	Runtime:removeEventListener("showBannerAd", showBannerAd);
--	Runtime:removeEventListener("hideAd", hideAd);
--	Runtime:removeEventListener("no_ads", stopAds);
--  Runtime:removeEventListener("loadIntAd", loadIntAd);
--  hideAd();
end
