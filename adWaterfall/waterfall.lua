module(..., package.seeall);

local adData = _G.WATERFALL_ORDER;

local showInterstitial;
local showBannerAd;
local loadIntAd;
local hideAd;

local listAds;
local currentWaterfallIndex;

local showingAd;
local adFailed;

--local isTestFail = true;
--local testFailTimes = 1;

local function getIfUnlocked()
  local fileIO = require("utility.fileParsing");
  local data = fileIO.getUserData();  
  return (data.game_unlocked.unlocked == true) or (_G.IS_IAP_ON == false);  
end


hideAd = function()
  for i = 1, #listAds do
    listAds[i].hideAd();
  end
end

showInterstitial = function(e)
  if (showingAd == true) then return; end
  if (getIfUnlocked() == true) then return; end
  
  currentWaterfallIndex = (currentWaterfallIndex + 1);
  
  showingAd = true;
  
--  if ((isTestFail == true) and (testFailTimes >= currentWaterfallIndex)) then
--    print(testFailTimes);
--    print(currentWaterfallIndex);
    
--    adFailed();
--    return;
--  elseif ((isTestFail == true)) then
--    isTestFail = false;
--  end
  
  if (currentWaterfallIndex > #listAds) then
    currentWaterfallIndex = 1;
    showingAd = nil;
    return; --all ads failed to load, dont keep cycling
  end
  
  print("SHOWING AD : " .. currentWaterfallIndex);
--  native.showAlert("TEST", "SHOWING AD : " .. currentWaterfallIndex, {"Okay"});
  
  listAds[currentWaterfallIndex].showInterstitial();
end

showBannerAd = function(e)
  if (getIfUnlocked() == true) then return; end
  for i = 1, #listAds do
    if (listAds[i].hasBanner == true) then
      print(i .. " HAS SHOW BANNER " .. type(listAds[i].showBannerAd));
      listAds[i].showBannerAd(e.isBottom);
      return;
    end
  end
end

local function adShown(e)
  currentWaterfallIndex = 0;
  showingAd = nil;
end

adFailed = function(e)
  showingAd = nil;
  showInterstitial();
end

function initialize()
  if (getIfUnlocked() == true) then return; end
  
  listAds = {};
  currentWaterfallIndex = 0;
  
  
  for i = 1, #adData do
    local adType = adData[i].adType;
    local intID = adData[i].INT_ID;
    local bannerID = adData[i].BANNER_ID;
    
    local adRef;
    
    if (adType == "applovin") then
      adRef = require("adWaterfall.applovin");
    elseif (adType == "admob") then
      adRef = require("adWaterfall.admob");
    end
    
    if ((bannerID ~= nil) and (bannerID ~= "")) then
      print(adType .. " CONTAINS BANNERS");
      adRef.hasBanner = true;
    end
    
    if ((intID ~= nil) and (intID ~= "")) then
      print(adType .. " CONTAINS INTS");
      adRef.hasInt = true;
    end
    
    adRef.initialize(intID, bannerID);
    
    print(adType .. " HAS SHOW BANNER " .. type(adRef.showBannerAd));
    table.insert(listAds, adRef);
    
  end
  
  Runtime:addEventListener("showInterstitial", showInterstitial);
	Runtime:addEventListener("showBannerAd", showBannerAd);
	Runtime:addEventListener("hideAd", hideAd);
  Runtime:addEventListener("adShown", adShown);
  Runtime:addEventListener("adFailed", adFailed);
  
end
