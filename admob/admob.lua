module(..., package.seeall);

--publisher id
local APP_ID = _G.ADMOB_INTERSTITIALS_ID;
local BANNER_ID = _G.ADMOB_BANNER_ID;

local TIME_TIL_AUTO_CLOSE = 6000;

local admob;

local handleCloseAd;

--forward declaration
local showInterstitial = nil;
local showBannerAd = nil;
local hideAd = nil;
local stopAds = nil;
local loadIntAd = nil;

local currentBannerY;
local showIntOnLoad;
local showBannerOnLoad;

local function adListener(e)
	print("Result of AdMob Call : " .. tostring(e.response) .. " " .. tostring(e.phase));
  if ( e.phase == "init" ) then  -- Successful initialization
      print( e.provider )
--      admob.load( "banner", { adUnitId=BANNER_ID} );
      admob.load( "interstitial", { adUnitId=APP_ID, childSafe=true } )
  elseif (e.phase == "loaded") then
    if ((e.type == "banner") and (showBannerOnLoad == true)) then
      showBannerOnLoad = nil;
      print("SHOWING ON LOAD ".. tostring(currentBannerY));
--      admob.show( "banner",{y=currentBannerY} );
      currentBannerY = nil;
      
    elseif (showIntOnLoad == true) then
      showIntOnLoad = nil;
      admob.show( "interstitial" );
    end
    
--    "banner" and "interstitial".
  elseif ((e.phase == "closed")) then
    print("CLOSED " .. e.type);
    if (e.type == "banner") then
--      admob.load( "banner", { adUnitId=BANNER_ID} );
    else
      admob.load( "interstitial", { adUnitId=APP_ID, childSafe=true } )
    end
  end
  
  print("AD RESPONSES");
  print(tostring(e.type), tostring(e.phase));
end

local function getIfUnlocked()
  local fileIO = require("utility.fileParsing");
  local data = fileIO.getUserData();
  
  return (data.game_unlocked.unlocked == true);
end

function initialize()
  if (getIfUnlocked() == true) then return; end
  
  _G.ADS_INITIALIZED = true;
	admob = require( "plugin.admob" );
  admob.init( adListener, { appId=APP_ID, testMode=false } )
  
	Runtime:addEventListener("showInterstitial", showInterstitial);
	Runtime:addEventListener("showBannerAd", showBannerAd);
	Runtime:addEventListener("hideAd", hideAd);
	Runtime:addEventListener("no_ads", stopAds);
  Runtime:addEventListener("loadIntAd", loadIntAd);
end

loadIntAd = function(e)
--  if (_G.playerData.purchased == 1) then return; end
--  if (getIfUnlocked() == true) then return; end
  
--  pcall(function()
--      if (admobInterstitial.isLoaded("interstitial") == false) then
--        admobInterstitial.load("interstitial", {appId=APP_ID, testMode=false});
--      end
--  end);
end

showInterstitial = function(e)
  if (getIfUnlocked() == true) then return; end
  
  showIntOnLoad = true;
  
	if ( admob.isLoaded("interstitial") ) then
    showIntOnLoad = nil;
    admob.show( "interstitial" );
    return;
  else
    admob.load( "interstitial", { adUnitId=APP_ID, childSafe=true } )
  end
end

showBannerAd = function(e)
--  if (getIfUnlocked() == true) then return; end
  
--  local adY = 0;
--  adY = "top";
  
--  if (e.isBottom == true) then
--    adY = (display.contentHeight - display.screenOriginY);
--    adY = "bottom";
--  end
  
--  currentBannerY = adY;
--  showBannerOnLoad = true;
  
--  if ( admob.isLoaded("banner") ) then
--    showBannerOnLoad = nil;
--    print("SHOWING BANNER @ : " .. adY);
--    admob.show( "banner",{ y=adY } );
    
--    return;
--  else
--    admob.load( "banner", { adUnitId=BANNER_ID} );
--  end
  
end

hideAd = function(e)
	admob.hide();
end

stopAds = function(e)
  Runtime:removeEventListener("showInterstitial", showInterstitial);
	Runtime:removeEventListener("showBannerAd", showBannerAd);
	Runtime:removeEventListener("hideAd", hideAd);
	Runtime:removeEventListener("no_ads", stopAds);
  Runtime:removeEventListener("loadIntAd", loadIntAd);
  hideAd();
end
