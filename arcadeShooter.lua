-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scoreLib = require("score");
local scene = storyboard.newScene()
local gameGroup;

local _W = display.contentWidth;
local _H = display.contentHeight;

local ambientChannel;
local ambientFX = audio.loadStream("SOUNDS/arcade/Background/Arcade Background.mp3");

local homeFX = audio.loadSound("SOUNDS/Home_Replay_Pause_Sound Buttons.mp3");


local woodenDuckFX = audio.loadSound("SOUNDS/arcade/Wooden Duck_ Hit 1.mp3");
local bonusTargetFX = audio.loadSound("SOUNDS/arcade/Bonus_Target Duck.mp3");
local squirtGunFX = audio.loadSound("SOUNDS/arcade/Squirt Gun_Shot.mp3");

local gameOverFX = audio.loadSound("SOUNDS/GameOver.mp3");

local layerBgDucks;
local layerFrDucks;
local layerInterface;

local loadInterface, spawnDuck;

local timeNewDuck = 0;

local score, bestScore, time;

local currentScore, currentBest, currentTime;


local mRandom, mSin = math.random, math.sin;
local newImage = display.newImageRect;

local duckImages = {
	"Ducks/arcadeBlue.png",
	"Ducks/arcadePinkTarget.png",
	"Ducks/arcadeYellow.png",
	"Ducks/arcadeBlueTarget.png",
	"Ducks/arcadeYellowTarget.png",
	"Ducks/arcadePink.png",		
}

local numTab = {
	"Numbers/DuckShooter_Number_000.png",
	"Numbers/DuckShooter_Number_001.png",
	"Numbers/DuckShooter_Number_002.png",
	"Numbers/DuckShooter_Number_003.png",
	"Numbers/DuckShooter_Number_004.png",
	"Numbers/DuckShooter_Number_005.png",
	"Numbers/DuckShooter_Number_006.png",
	"Numbers/DuckShooter_Number_007.png",
	"Numbers/DuckShooter_Number_008.png",
	"Numbers/DuckShooter_Number_009.png"
};

local loadBestScore = function()
	local path = system.pathForFile("bestScore.txt", system.DocumentsDirectory);
	local file = io.open(path, "r");
	if file then
		currentBest = tonumber(file:read());
		io.close(file);
	else
		currentBest = 0;
	end
end

local saveBestScore = function()
	local path = system.pathForFile("bestScore.txt", system.DocumentsDirectory);
	local file = io.open(path, "w+");
	file:write(currentBest);
	io.close(file);
end

local function checkShot(xPos, yPos)
	
	audio.play(squirtGunFX, {channel = audio.findFreeChannel()});
	
	local splash = display.newImageRect(scene.view, "splash.png", 125, 125);
	splash.x, splash.y = xPos, yPos;
	transition.from(splash, {time = 200, xScale = 0.1, yScale = 0.1});
	transition.to(splash, {delay = 200, alpha = 0, time = 200, onComplete = splash.removeSelf});
	
	for i = 1, layerBgDucks.numChildren do
		local dX, dY = xPos-(layerBgDucks[i].contentBounds.xMin + (layerBgDucks[i].contentWidth * 0.5)), yPos-(layerBgDucks[i].contentBounds.yMin + (layerBgDucks[i].contentHeight * 0.5));
		local distance = math.sqrt(dX*dX+dY*dY);
		if distance < 60 then
			if layerBgDucks[i].name == "Ducks/arcadeBlue.png" then
				currentScore = currentScore+100;
				audio.play(woodenDuckFX, {channel = audio.findFreeChannel()});
			elseif layerBgDucks[i].name == "Ducks/arcadePinkTarget.png" then
				currentScore = currentScore+500;
				audio.play(bonusTargetFX, {channel = audio.findFreeChannel()});
			elseif layerBgDucks[i].name == "Ducks/arcadeYellow.png" then
				currentScore = currentScore+100;
				audio.play(woodenDuckFX, {channel = audio.findFreeChannel()});
			elseif layerBgDucks[i].name == "Ducks/arcadeBlueTarget.png" then
				currentScore = currentScore+500;
				audio.play(bonusTargetFX, {channel = audio.findFreeChannel()});
			elseif layerBgDucks[i].name == "Ducks/arcadeYellowTarget.png" then
				currentScore = currentScore+500;
				audio.play(bonusTargetFX, {channel = audio.findFreeChannel()});
			elseif layerBgDucks[i].name == "Ducks/arcadePink.png" then
				currentScore = currentScore+200;
				audio.play(woodenDuckFX, {channel = audio.findFreeChannel()});
			end
			scoreLib.setScore(currentScore, score)
			Runtime:removeEventListener("enterFrame", layerBgDucks[i]);
			layerBgDucks[i]:removeSelf();
--      layerFrDucks[i].isDead = true;
			break;
		end
	end
	
	for i = 1, layerFrDucks.numChildren do
		local dX, dY = xPos-(layerFrDucks[i].contentBounds.xMin + (layerFrDucks[i].contentWidth * 0.5)), yPos-(layerFrDucks[i].contentBounds.yMin + (layerFrDucks[i].contentHeight * 0.5));
		local distance = math.sqrt(dX*dX+dY*dY);
		if distance < 60 then
			if layerFrDucks[i].name == "Ducks/arcadeBlue.png" then
				currentScore = currentScore+100;
				audio.play(woodenDuckFX, {channel = audio.findFreeChannel()});
			elseif layerFrDucks[i].name == "Ducks/arcadePinkTarget.png" then
				currentScore = currentScore+500;
				audio.play(bonusTargetFX, {channel = audio.findFreeChannel()});
			elseif layerFrDucks[i].name == "Ducks/arcadeYellow.png" then
				currentScore = currentScore+100;
				audio.play(woodenDuckFX, {channel = audio.findFreeChannel()});
			elseif layerFrDucks[i].name == "Ducks/arcadeBlueTarget.png" then
				currentScore = currentScore+500;
				audio.play(bonusTargetFX, {channel = audio.findFreeChannel()});
			elseif layerFrDucks[i].name == "Ducks/arcadeYellowTarget.png" then
				currentScore = currentScore+500;
				audio.play(bonusTargetFX, {channel = audio.findFreeChannel()});
			elseif layerFrDucks[i].name == "Ducks/arcadePink.png" then
				currentScore = currentScore+200;
				audio.play(woodenDuckFX, {channel = audio.findFreeChannel()});
			end
			scoreLib.setScore(currentScore, score)
			Runtime:removeEventListener("enterFrame", layerFrDucks[i]);
			layerFrDucks[i]:removeSelf();
--      layerFrDucks[i].isDead = true;
			break;
		end
	end
end

local function pauseEverything(group)
	if group.enterFrame then
		Runtime:removeEventListener("enterFrame", group);
		group.enterFrameActive = true;
	end
	if group.touch then
		group:removeEventListener("touch", group);
		group.touchActive = true;
	end
	if group.play then
		group:pause();
		group.animationActive = true;
	end
	for i = group.numChildren, 1, -1 do
		if group[i].numChildren then
			pauseEverything(group[i])
		else
			if group[i].enterFrame then
				Runtime:removeEventListener("enterFrame", group[i]);
				group[i].enterFrameActive = true;
			end
			if group[i].touch then
				group[i]:removeEventListener("touch", group[i]);
				group[i].touchActive = true;
			end
			if group[i].play then
				group[i]:pause();
				group[i].animationActive = true;
			end
		end
	end
end

local function unpauseEverything(group)
	if group.enterFrameActive then
		Runtime:addEventListener("enterFrame", group);
		group.enterFrameActive = nil;
	end
	if group.touchActive then
		group:addEventListener("touch", group);
		group.touchActive = nil;
	end
	if group.animationActive then
		group:play();
		group.animationActive = true;
	end
	for i = group.numChildren, 1, -1 do
		if group[i].numChildren then
			unpauseEverything(group[i])
		else
			if group[i].enterFrameActive then
				Runtime:addEventListener("enterFrame", group[i]);
				group[i].enterFrameActive = nil;
			end
			if group[i].touchActive then
				group[i]:addEventListener("touch", group[i]);
				group[i].touchActive = nil;
			end
			if group[i].animationActive then
				group[i]:play();
				group[i].animationActive = nil;
			end
		end
	end
end

loadInterface = function()
	local group = layerInterface;
	local bg = display.newImageRect(group, "UI/DuckShooter_ArcadeHUD.png", 480, 80);
	bg.x, bg.y = 240, 290;
	
	score = scoreLib.initScore(100, 20, 29, 30, 6, numTab, group, 12);
	score.x, score.y = 191, 295;
	
	bestScore = scoreLib.initScore(100, 20, 29, 30, 6, numTab, group, 12);
	bestScore.x, bestScore.y = 318, 295;
	
	time = scoreLib.initScore(100, 20, 29, 30, 2, numTab, group, 12);
	time.x, time.y = 384, 295;
	
	currentTime = 47;
	scoreLib.setScore(currentTime, time);
	
	loadBestScore();
	scoreLib.setScore(currentBest, bestScore);
	
	function time:enterFrame()
		if currentScore > currentBest then
			currentBest = currentScore;
			scoreLib.setScore(currentBest, bestScore);
		end
		currentTime = currentTime-0.0167;
		scoreLib.setScore(math.floor(currentTime), time);
		if currentTime <= 0 then
			pauseEverything(scene.view);
			saveBestScore();
			local overlay = display.newRect(scene.view, 0, 0, _W, _H);
      overlay.alpha = 0.01;
      overlay.x,overlay.y = (_W * 0.5),(_H * 0.5);
			overlay:setFillColor(0, 0, 0, 150);
			function overlay:touch(event)
				return true;
			end
			overlay:addEventListener("touch", overlay);
		
			local homeButton = display.newImageRect(scene.view, "UI/DuckShooter_Home_001.png", 90, 90);
			homeButton.x, homeButton.y = (_W * 0.4), (_H * 0.5);
			function homeButton:touch(event)
				if event.phase == "ended" then
					audio.play(homeFX, {channel = audio.findFreeChannel()});
--					storyboard.gotoScene("menu", "fade");
          Runtime:dispatchEvent({name = "showInterstitial"});
          changeScene("scene_menu");
				end
				return true;
			end
			homeButton:addEventListener("touch", homeButton);

			local restartButton = display.newImageRect(scene.view, "UI/DuckShooter_Restart_001.png", 90, 90);
			restartButton.x, restartButton.y = (_W * 0.6), (_H * 0.5);
			function restartButton:touch(event)
				if event.phase == "ended" then
					audio.play(homeFX, {channel = audio.findFreeChannel()});
				--	storyboard.reloadScene();
           Runtime:dispatchEvent({name = "showInterstitial"});
					storyboard.gotoScene("restartArcadeShooter");
				end	
				return true;
			end
			restartButton:addEventListener("touch", restartButton);	
			
			local gameOverText = display.newImageRect(scene.view, "DuckShooter_GameOver.png", 800, 160);
			gameOverText.x, gameOverText.y = (_W * 0.5), (_H * 0.2);
			
			audio.play(gameOverFX, {channel = audio.findFreeChannel()});
		end
	end
	Runtime:addEventListener("enterFrame", time);
	
	local pauseButton = display.newImageRect(group, "UI/DuckShooter_Pause_001.png", 35, 35);
	pauseButton.x, pauseButton.y = 75, 250;
	function pauseButton:touch(event)
		if event.phase == "ended" then
			audio.play(homeFX, {channel = audio.findFreeChannel()});
			gamePaused = true;
			pauseEverything(scene.view);
			local overlay = display.newRect(scene.view, 0, 0, _W, _H);
      overlay.alpha = 0.01;
      overlay.x,overlay.y = (_W * 0.5),(_H * 0.5);
			overlay:setFillColor(0, 0, 0, 150);
			function overlay:touch(event)
				if event.phase == "ended" then
					self:removeSelf();
					unpauseEverything(scene.view);
					gamePaused = nil;
				end
				return true;
			end
			overlay:addEventListener("touch", overlay);
		end
		return true;
	end
	pauseButton:addEventListener("touch", pauseButton);
	
	local homeButton = display.newImageRect(group, "UI/DuckShooter_Home_001.png", 35, 35);
	homeButton.x, homeButton.y = 25, 250;
	function homeButton:touch(event)
		if event.phase == "ended" then
			audio.play(homeFX, {channel = audio.findFreeChannel()});
--			storyboard.gotoScene("menu", "fade");
      Runtime:dispatchEvent({name = "showInterstitial"});
      changeScene("scene_menu");
		end
		return true;
	end
	homeButton:addEventListener("touch", homeButton);
	
	local volume = audio.getVolume({channel = 2});
	
	local soundButton = display.newImageRect(group, "UI/DuckShooter_Sound_001.png", 35, 35);
	soundButton.x, soundButton.y = 125, 250;
  soundButton.isVisible = false;
	if volume < 1 then
		soundButton.alpha = 0.5;
	end
	function soundButton:touch(event)
		if event.phase == "began" then
			audio.play(homeFX, {channel = audio.findFreeChannel()});
			if self.alpha == 1 then
				self.alpha = 0.5;
				for i = 2, 72 do
					audio.setVolume(0, {channel = i})
				end
			else
				self.alpha = 1;
				for i = 2, 72 do
					audio.setVolume(1, {channel = i})
				end
			end
		end
		return true;
	end
	soundButton:addEventListener("touch", soundButton);
end

spawnDuck = function(group)
	local choseDuck = mRandom(1, #duckImages);
	local duck = newImage(group, duckImages[choseDuck], 40, 60);
	duck.name = duckImages[choseDuck];
	function duck:enterFrame()
		if self.xScale == 1 then
			self.x = self.x+1*4
			self.y = self.yPos+(mSin(self.x*0.1*1)*6);
			if self.x > 480 then
				Runtime:removeEventListener("enterFrame", self);
				self:removeSelf();
			end
		else 
			self.x = self.x-1*4
			self.y = self.yPos+(mSin(self.x*0.1*1)*6);
			if self.x < 0 then
				Runtime:removeEventListener("enterFrame", self);
				self:removeSelf();
			end
		end
	end
	return duck;
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
		audio.stop(1);
		ambientChannel = 1;
	audio.play(ambientFX, {channel = ambientChannel, loops = -1});
	

	currentScore, currentBest, currentTime = 0, 0, 0;

--	gameGroup = display.newGroup();
  local group = display.newGroup();
  self.view:insert(group);
--  group:insert(gameGroup);
  
	local bg = display.newImageRect(group, "Backgrounds/DuckShooter_Background_003A.png", 480, 340);
	bg.x, bg.y = 240, 170;
	function bg:touch(event)
		if event.phase == "began" then
			audio.play(squirtGunFX, {channel = audio.findFreeChannel()});
			checkShot(event.x, event.y);
		end
	end
	bg:addEventListener("touch", bg);
	
	layerBgDucks = display.newGroup();
	group:insert(layerBgDucks);
	local bgWater = display.newImageRect(group, "Backgrounds/DuckShooter_Background_003C.png", 480, 340);
	bgWater.x, bgWater.y = 240, 170;
	layerFrDucks = display.newGroup();
	group:insert(layerFrDucks);
	local frWater = display.newImageRect(group, "Backgrounds/DuckShooter_Background_003D.png", 480, 340);
	frWater.x, frWater.y = 240, 170;
	local frBg = display.newImageRect(group, "Backgrounds/DuckShooter_Background_003B.png", 480, 340);
	frBg.x, frBg.y = 240, 170;
	layerInterface = display.newGroup();
	group:insert(layerInterface);
	loadInterface();
	
	local newTime = 40;
	function group:enterFrame()
		timeNewDuck = timeNewDuck+1;
		if timeNewDuck > newTime then
			newTime = mRandom(20, 60);
			local duckBg = spawnDuck(layerBgDucks);
			duckBg.x, duckBg.y = 20, 110;
			duckBg.yPos = 110;
			Runtime:addEventListener("enterFrame", duckBg);
			local duckFr = spawnDuck(layerFrDucks);
			duckFr.x, duckFr.y = 460, 165;
			duckFr.xScale = -1;
			duckFr.yPos = 165;
			Runtime:addEventListener("enterFrame", duckFr);
			timeNewDuck = 0;
		end
	end
	Runtime:addEventListener("enterFrame", group);
  
  group.xScale,group.yScale = 2.4,2.4;
  print(group.xScale,group.yScale);
  
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	if ambientChannel then
		audio.stop(ambientChannel)
	end
	local group = self.view
	local removeAll;
	
	removeAll = function(group)
		if group.enterFrame then
			Runtime:removeEventListener("enterFrame", group);
		end
		if group.touch then
			group:removeEventListener("enterFrame", group);
			Runtime:removeEventListener("enterFrame", group);
		end		
		for i = group.numChildren, 1, -1 do
			if group[i].numChildren then
				removeAll(group[i]);
			else
				if group[i].enterFrame then
					Runtime:removeEventListener("enterFrame", group[i]);
				end
				if group[i].touch then
					group[i]:removeEventListener("enterFrame", group[i]);
					Runtime:removeEventListener("enterFrame", group[i]);
				end
			end
		end
	end
	
	removeAll(group);
	
	layerBgDucks = nil;
	layerFrDucks = nil;
	layerInterface = nil;
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene;