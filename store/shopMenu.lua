module(..., package.seeall);

local _W = display.contentWidth
local _H = display.contentHeight
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local xScale = (_W / 1136);
local yScale = (_H / 768);

--products purchasable with coins
local listCoinProducts = 
{
  --row 1
  {name = "crown", title = "crown", price = 500, description = "I'd like a cuppa' tea please!", filename = "assets/images/shop/hat1icon.png"},
  {name = "flat_top", title = "flat top", price = 500, description = "So smooth!", filename = "assets/images/shop/hat2icon.png"},
  {name = "sock_hat", title = "sock hat",price = 1000, description = "Ho, Ho, Ho!", filename = "assets/images/shop/hat3icon.png"},
  {name = "hat_mushroom_blue", title = "blue mushroom hat",price = 1000, description = "A spotted blue hat!", filename = "assets/images/shop/hat4icon.png"},
  
  --row 2
  {name = "eyepatch", title = "eyepatch",price = 2000, description = "Ahoy, Matey!", filename = "assets/images/shop/eyepatch_icon.png"},
  {name = "hat_mushroom_red", title = "red mushroom hat", price = 2000, description = "I look fabulous!", filename = "assets/images/shop/hat7icon.png"},
  {name = "hat_mushroom_purple", title = "purple mushroom hat",price = 2000, description = "Purple rain!", filename = "assets/images/shop/hat6icon.png"},
  {name = "hat_mushroom_green", title = "green mushroom hat",price = 2000, description = "I’m green with envy!", filename = "assets/images/shop/hat5icon.png"},
  
  --row 3
  {name = "mask", title = "mask",price = 2000, description = "Incognito", filename = "assets/images/shop/mask_icon.png"},
  {name = "pink_glasses", title = "pink glasses",price = 3000, description = "Pink is my favorite color!", filename = "assets/images/shop/icon_pinkglasses.png"},
  {name = "sunglasses", price = 2000, title = "sunglasses", description = "So cool!", filename = "assets/images/shop/icon_sunglasses.png"},
  { name = "thick_glasses", title = "thick glasses", price = 3000, description = "The ladies love em'", filename = "assets/images/shop/icon_thickglasses.png"},
  
  --row 4
  {name = "star_sprite_blue", title = "blue flower", price = 5000, description = "I look like a tall glass of water!", filename = "assets/images/shop/icon_blueflower.png"},
  {name = "star_sprite_green", title = "green flower",price = 5000, description = "Lemony-Lime flavored!", filename = "assets/images/shop/icon_greenflower.png"},
  {name = "star_sprite_pink", title = "red flower",price = 5000, description = "Red and ready!", filename = "assets/images/shop/icon_redflower.png"},
};

--products needing real life currency
listMoneyProducts =
{
  {name = "no_ads", title = "no ads for $0.99", retailButton = true, description = "No ads!", price = "99", filename = "assets/images/shop/blankstoreitemicon.png"},
  {name = "button_99", title = "1,500 coins for $0.99",retailButton = true,description = "1,500 coins!", price = "99", filename = "assets/images/shop/blankstoreitemicon.png"},
  {name = "button_299", title = "3,500 coins for $2.99", retailButton = true,description = "3,500 coins!", price = "299", filename = "assets/images/shop/blankstoreitemicon.png"},
  {name = "button_499", title = "12,000 coins for $4.99", retailButton = true,description = "12,000 coins!", price = "499", filename = "assets/images/shop/blankstoreitemicon.png"},
  {name = "button_999", title = "30,000 coins for $9.99", retailButton = true,description = "30,000 coins!", price = "999", filename = "assets/images/shop/blankstoreitemicon.png"},
};

local fileIO = require("fileParsing");
local userShopData;
  
local bStoreActive;

local currentButtonSelected;
local productDescription;

local listAssets;
local listImages;

local shopGroup;
local bgGroup;
local coinProductsGroup;
local moneyProductsGroup;

local setupStore = nil;
local setupFreeStore = nil;
local showAccessories = nil;
local showMoneyProducts = nil;
local handleCoinPurchaseSuccess = nil;
local setNoAdsPurchased = nil;
local destroy = nil;

local function layerGroups(group)
  shopGroup = display.newGroup();
  bgGroup = display.newGroup();
  coinProductsGroup = display.newGroup();
  moneyProductsGroup = display.newGroup();
  
  shopGroup:insert(bgGroup);
  shopGroup:insert(coinProductsGroup);
  shopGroup:insert(moneyProductsGroup);
  group:insert(shopGroup);
end

local function buttonTouchListener(e)
  if (e.phase == "ended") then
    Runtime:dispatchEvent({name = e.target.tag});
    Runtime:dispatchEvent({name = "shop_button_touch"});
  end
  
  return true;
end

local function initButtons()
--  listImages.button_accessories.tag = "accessories";
--  listImages.button_coins.tag = "buy_coins";
--  listImages.button_back.tag = "close_shop";
--  listImages.button_restore.tag = "restore_purchases";
  
--  listImages.button_accessories:addEventListener("touch", buttonTouchListener);
--  listImages.button_coins:addEventListener("touch", buttonTouchListener);
--  listImages.button_back:addEventListener("touch", buttonTouchListener);
--  listImages.button_restore:addEventListener("touch", buttonTouchListener);
end

function loadData()
  if (fileIO.getIfStoreDataExists() == false) then
      fileIO.cacheInitialStoreData(USE_FREE_STORE);
  end  
  
  --TODO uncomment when shop developed more 
--  userShopData = fileIO.getStoreData();
  
  if ((USE_FREE_STORE == false) and (_testMode == false)) then
    local storeKit = require("store.storeSystem");
    storeKit.initialize(listMoneyProducts);
  end
  
  if (_userPurchasedAds == false) then
    Runtime:addEventListener("no_ads", setNoAdsPurchased);
  else
    table.remove(listMoneyProducts, 1);
  end
end

function initialize(group)
  if (bStoreActive == true) then return; end
  
  if (listAssets == nil) then
    local assetManager = require("assetManager.assetManager");
    listAssets = assetManager.getAssets("shop");
  end
  
  bStoreActive = true;
  
  layerGroups(group);
  
  listImages = {};
--  local bgRect = display.newImage("assets/images/shop/popupoverlaybgpurple.png");
--  bgRect.width = 320;
--  bgRect.height = 480;
--  bgRect.xScale = xScale;
--  bgRect.yScale = yScale;
--  bgRect.alpha = 0.01;
--  bgRect.x = _w;
--  bgRect.y = _h;
--  bgGroup:insert(bgRect);
--  bgRect:addEventListener("touch", function(e) return true; end);
  
--  listImages.fadedBG = bgRect;
  
    for i = 1, #listAssets.images do
      local imageData = listAssets.images[i];
      
      listImages[imageData.Name] = display.newImage(bgGroup, imageData.filename, 0,0);
      listImages[imageData.Name].width = imageData.width;
      listImages[imageData.Name].height = imageData.height;
      listImages[imageData.Name].xScale = xScale;
      listImages[imageData.Name].yScale = yScale;
      listImages[imageData.Name].x = imageData.x;
      listImages[imageData.Name].y = imageData.y;
    
  end
  
  if (USE_FREE_STORE == true) then
    setupFreeStore();
  else
    setupStore();
  end
  
  initButtons();
  
--  Runtime:addEventListener("accessories", showAccessories);
--  Runtime:addEventListener("buy_coins", showMoneyProducts);
--  Runtime:addEventListener("coin_purchase_complete", handleCoinPurchaseSuccess);
--  Runtime:addEventListener("close_shop", destroy);
  
  Runtime:dispatchEvent({name = "shop_music"});
end

local function setProductDescription(strDescription)
  local options = 
  {
      parent = bgGroup,
      text = strDescription,     
      x = 0,
      y = 0,
      width = 200,    
      height = 0,    
      font = native.systemFont,   
      fontSize = 12,
      align = "center" 
  };
  
  if (productDescription) then
    display.remove(productDescription);
    productDescription = nil;
  end
  
  productDescription = display.newText(options);
  productDescription:setTextColor(0,0,0);
  productDescription.x = (_W * 0.51);
  productDescription.y = (_H * 0.25);
end

local function handleRetailPurchase(button)
  native.showAlert("Purchase", "Do you wish to purchase " .. button.title .. "?", {"No Thanks","Okay"}, function(e)
      --no thanks pressed
        if (e.index == 1) then
        
        --okay pressed
        elseif (e.index == 2) then
          Runtime:dispatchEvent({name = "purchaseItem", item = button});
        end
    end);
end

local function handleProductConfirmation(button)
  if (button.retailButton) then
      return;
  end
  
  --handle coin purchase
  local itemUnlocked = userShopData[button.name].unlocked;
  
  if (USE_FREE_STORE == true) then
    itemUnlocked = true;
  end
  
  if (itemUnlocked == false) then
    --prompt if they wish to buy
    native.showAlert("Buy Item", "Would you like to purchase this item for " .. button.price .. " coins?", {"No Thanks", "Okay"}, function(e)
        --no thanks pressed
        if (e.index == 1) then
        
        --okay pressed
        elseif (e.index == 2) then
          Runtime:dispatchEvent({name = "coin_purchase", item = button});
        end
      end);
    
  elseif (itemUnlocked == true) then
    --handle equip
    Runtime:dispatchEvent({name = "equip_accessory", item = button.name, itemTitle = button.title});
  end
  
end

local function productButtonFired(button)
  Runtime:dispatchEvent({name = "shop_button_touch"});
  
  if (currentButtonSelected == button) then
    --handle attempt to purchase/equip
    handleProductConfirmation(button);
  elseif (button.retailButton == nil) then
    setProductDescription(button.description);
    currentButtonSelected = button;
  elseif (button.retailButton) then
    handleRetailPurchase(button);
  end
end

local function productButtonTouched(e)
  if (e.phase == "ended") then
    productButtonFired(e.target);
  end
end

local function createProductIcon(data, width, height)
  local button = display.newGroup();
  local icon, price;
  icon = display.newImage(data.filename); 
  icon.width = (width or 50);
  icon.height = (height or 50);
  button:insert(icon);
  button.price = data.price;
  button.description = data.description;
  button.name = data.name;
  button.retailButton = data.retailButton;
  button.title = data.title;
  
  if ((data.price) and (data.retailButton ~= true) and (userShopData[data.name].unlocked == false) and (USE_FREE_STORE == false)) then
    price = display.newImage("assets/images/shop/"..data.price .. "p.png");
    price.width = (width or 50);
    price.height = (height or 50);
    price.x = (icon.x);
    price.y = icon.y;
    button.priceTag = price;
    button:insert(price);
  elseif ((USE_FREE_STORE == false) and (button.retailButton == true)) then
    price = display.newImage("assets/images/shop/price_"..data.price .. ".png");
    price.width = (width or 50);
    price.height = (height or 50);
    --price.x = (price.x - 15);
    price.x = icon.x;
    price.y = icon.y;
    button.priceTag = price;
    button:insert(price);
  end
  
  button.xScale = xScale;
  button.yScale = yScale;
  
  button:setReferencePoint(display.CenterReferencePoint);
  
  button:addEventListener("touch", productButtonTouched);
  
  return button;
end

local function initCoinProducts()
  local xMod, yMod = 0, 0;
  local initialX = (_W * 0.25);
  local initialY = (_H * 0.35);
  local xSpacing = (_W * 0.168);
  local ySpacing = (_H * 0.115);
  
  for i = 1, #listCoinProducts do
    local product = listCoinProducts[i];
    local button = createProductIcon(product);
    
    button.x = (initialX + (xMod * xSpacing));
    
    xMod = (xMod + 1);
    
    button.y = (initialY + (yMod * ySpacing));
    
    if (yMod == 3) then
      button.x = (button.x + (button.width * 0.5));
    end
    
    if (xMod == 4) then
      yMod = (yMod + 1);
      xMod = 0;
    end
    
    coinProductsGroup:insert(button);
  end
end

local function initMoneyProducts()
  moneyProductsGroup.isVisible = false;
  
  local yMod = 0;
  local xPos = (_W * 0.27);
  local initialY = (_H * 0.3);
  local ySpacing = (_H * 0.115);
  
  for i = 1, #listMoneyProducts do
    local product = listMoneyProducts[i];
    local button = createProductIcon(product, 50, 50);
    
    button.x = xPos;  button.y = (initialY + (yMod * ySpacing));
    yMod = (yMod + 1);
    
    local description = display.newText(product.description, 0, 0, native.systemFont, 18);
    description:setTextColor(0,0,0);
    description.x = (_W * 0.6);
    description.y = (button.y);
    
    local iconSprite = require("animations.coin_bag");
    iconSprite = iconSprite.createNewSheet();
    iconSprite.xScale = 0.1;
    iconSprite.yScale = 0.1;
    iconSprite.x = button.x;  iconSprite.y = button.y;
    
    iconSprite:play();
    
    moneyProductsGroup:insert(button);
    moneyProductsGroup:insert(description);
    moneyProductsGroup:insert(iconSprite);
  end
end

setupFreeStore = function()
  display.remove(listImages.button_accessories);
  display.remove(listImages.button_coins);
  display.remove(listImages.button_restore);
  initCoinProducts();
end

setupStore = function()
  initCoinProducts();
  initMoneyProducts();
end

showAccessories = function()
  moneyProductsGroup.isVisible = false;
  coinProductsGroup.isVisible = true;
end

showMoneyProducts = function()
  if (productDescription) then
    display.remove(productDescription);
    productDescription = nil;
  end
  
  currentButtonSelected = nil;
  moneyProductsGroup.isVisible = true;
  coinProductsGroup.isVisible = false;
end

handleCoinPurchaseSuccess = function(e)
    display.remove(e.button.priceTag);
    userShopData[e.button.name].unlocked = true;
    fileIO.saveShopData(userShopData);
end

setNoAdsPurchased = function(e)
    _userPurchasedAds = true;
    fileIO.cacheUserPurchaseAds();
    table.remove(listMoneyProducts, 1);
end

destroy = function()
  display.remove(shopGroup);
  bStoreActive = false;
  Runtime:removeEventListener("accessories", showAccessories);
  Runtime:removeEventListener("buy_coins", showMoneyProducts);
  Runtime:removeEventListener("close_shop", destroy);
  
  currentButtonSelected = nil;
end


