module(..., package.seeall);

--enter product identifiers / sku's here as follows
--needs to follow this format in order to reference id's correctly
local PRODUCT_LIST = _G.STORE_PRODUCTS;

--replace with however much this game will cost
local UNLOCK_GAME_PRICE = _G.UNLOCK_GAME_PRICE;

--initialized in init
local PRODUCT_ID_HASH_TABLE = {};

--parallel list 
local AWARD_LIST = {};
AWARD_LIST[1] = {name = "game_unlocked", awardString = "full game"};

local store;

local STORE_TYPE = system.getInfo("targetAppStore");

local fileIO = require("utility.fileParsing");

local storeType;

local loadedProductsList;

--forward declaration
local purchaseItem = nil;

local function awardPurchase(productID)
--  for i = 1, #PRODUCT_LIST do
--    if (PRODUCT_LIST[i] == productID) then
      local i = 1; --just award first product ID since game unlock is the only iap

      Runtime:dispatchEvent(AWARD_LIST[i]);
      Runtime:dispatchEvent({name = "no_ads"});
      Runtime:dispatchEvent({name = "hideAd"});
      _G.ADS_INITIALIZED = false;
      local userData = fileIO.getUserData();
      
      if (i == 1) then --all unlocked
        userData.game_unlocked.unlocked = true;
        _G.GAME_UNLOCKED = true;
      end
      
      fileIO.saveShopData(userData);
      return;
--    end
--  end
end

local function transactionCallback( event )
    local transaction = event.transaction
    
    if transaction.state == "purchased" then
        print("************** Transaction succuessful!")
        print("************** productIdentifier", transaction.productIdentifier)
        print("************** receipt", transaction.receipt)
        print("************** transactionIdentifier", transaction.identifier)
        print("************** date", transaction.date)
        
        local currentItem;
        
        if ((loadedProductsList ~= nil)) then
          currentItem = loadedProductsList[transaction.productIdentifier];
		  print("************** purchased", loadedProductsList[transaction.productIdentifier].title);
        else
          --native.showAlert("Error in Loaded Products Table", "No valid products initialized", {"Okay"});
		  if (transaction.identifier == "com_detentiongames_hiddenobjectssummerbeach_bigobjectspack") then
                native.showAlert("Complete!", "You have successfully bought: BIG Summer Beach Pack!", {"Okay"});  
            end
        end
       
        if (currentItem ~= nil) then
          native.showAlert("Complete", "You have successfully bought: " .. tostring(currentItem.title), {"Okay"});
        end

        
        awardPurchase(transaction.productIdentifier);
        
    elseif  transaction.state == "restored" then
        print("Transaction restored (from previous session)")
        print("productIdentifier", transaction.productIdentifier)
        print("receipt", transaction.receipt)
        print("transactionIdentifier", transaction.identifier)
        print("date", transaction.date)
        print("originalReceipt", transaction.originalReceipt)
        print("originalTransactionIdentifier", transaction.originalIdentifier)
        print("originalDate", transaction.originalDate)
        
        awardPurchase(transaction.productIdentifier);
        
    elseif transaction.state == "cancelled" then
        print("User cancelled transaction")

    elseif transaction.state == "failed" then
        print("Transaction failed, type:", transaction.errorType, transaction.errorString)
	
    --refunds only available in google play
    elseif ( transaction.state == "refunded" ) then
      print("refunded");
      
    else
        print("unknown event")
--        native.showAlert("Cannot Install Pack", "You've already purchased this item, please return to the main menu and tap the Restore Purchases button to unlock this app.", {"Okay"});
    end

    -- Once we are done with a transaction, call this to tell the store
    -- we are done with the transaction.
    -- If you are providing downloadable content, wait to call this until
    -- after the download completes.
    --native.showAlert("Status", "Status of transaction: " .. transaction.state, {"Okay"});
    
    store.finishTransaction( transaction )
end

local function loadProductsCallback( event )

    local validProducts = (event.products or {});
    local invalidProducts = (event.invalidProducts or {});
	
	loadedProductsList = {};
	print("******************** loaded product");
    for i = 1,#validProducts do
        local currentItem = validProducts[i];        
        loadedProductsList[currentItem.productIdentifier] = {};
        loadedProductsList[currentItem.productIdentifier].title = currentItem.title;
        loadedProductsList[currentItem.productIdentifier].description = currentItem.description;
        loadedProductsList[currentItem.productIdentifier].price = currentItem.price;
		print("******************** loaded product title : ", loadedProductsList[currentItem.productIdentifier].title);
    end
end

local function restorePurchase(e)
  print("restoring purchase");
  store.restore();
end

local function purchaseUnlock()
  native.showAlert("GET ALL LEVELS!", "1 LOW PRICE! Buy Pack & NO Ads! ALL Worldwide Spring Garden Scenes in Traditional, Chill & Adventure! Explore Easter Flower Gardens, Rose Gardens, Tulips, Flower Decorated Homes, Worldwide Gardens & more! 100's of Objects to Find! Get whole game for " .. tostring(UNLOCK_GAME_PRICE) .. "?", {"No Thanks","Okay"}, function(e)
      if (e.index == 1) then --no thanks pressed
        
      elseif (e.index == 2) then --okay pressed
        if ( "simulator" == system.getInfo("environment") ) then
          timer.performWithDelay(100, function()
            native.showAlert("Simulator", "Simulating successful purchase", {"Okay"});
            
            _G.GAME_UNLOCKED = true;
            
            local userData = fileIO.getUserData();
            userData.game_unlocked.unlocked = true;
            fileIO.saveShopData(userData);
            
            Runtime:dispatchEvent(AWARD_LIST[1]);
          end);
          
        else
          if ((storeType == "amazon") or (storeType == "google")) then
            store.purchase(PRODUCT_LIST[1]);
          else
            store.purchase({PRODUCT_LIST[1]});
          end
        end
      end
    
  end);
  
end
local function checkIfGameUnlocked()
  local userData = fileIO.getUserData();
  
  if (((userData.game_unlocked.unlocked == true)) or (_G.FREE_VERSION == true)) then
    _G.GAME_UNLOCKED = true;
    Runtime:dispatchEvent(AWARD_LIST[1]);
    Runtime:removeEventListener("buy_game_unlock", purchaseUnlock);      
  end
end

local function finishStoreInit()
  timer.performWithDelay(1000, function()
  print("******************** try to load");
    if (store.canLoadProducts == true) then
	print("******************** can load product");
      store.loadProducts(PRODUCT_LIST, loadProductsCallback);
    else
      native.showAlert("Store Failed", "Store failed to load products and will be unavailable at this time.", {"Okay"});
    end
  end);
	
  Runtime:addEventListener("buy_game_unlock", purchaseUnlock);
  
  checkIfGameUnlocked();
  
  Runtime:addEventListener("restore_purchases", restorePurchase);
end

function initializeGoogle()
  if ( "simulator" == system.getInfo("environment")) then
    Runtime:addEventListener("buy_game_unlock", purchaseUnlock);
    checkIfGameUnlocked();
    return;
  end
  
  --store = require("store");  
  store = require( "plugin.google.iap.billing" );
  storeType = "google";
  store.init("google", transactionCallback);

  
  finishStoreInit();
end

function initializeApple()
  if ( "simulator" == system.getInfo("environment")) then
    Runtime:addEventListener("buy_game_unlock", purchaseUnlock);
    checkIfGameUnlocked();
    return;
  end
  
  store = require("store");
  storeType = "apple";
  store.init(storeType, transactionCallback);
  
  finishStoreInit();
end

function initializeAmazon()  
  if ( "simulator" == system.getInfo("environment")) then
    Runtime:addEventListener("buy_game_unlock", purchaseUnlock);
    checkIfGameUnlocked();
    return;
  end
    
  store = require("plugin.amazon.iap");
  store.init(transactionCallback);
  storeType = "amazon";
  store.restore();
  
  finishStoreInit();
end

--e.item
--purchaseItem = function(e)
--  local purchaseIndex = PRODUCT_ID_HASH_TABLE[e.item.name].index;
--	store.purchase({PRODUCT_LIST[purchaseIndex]});
--end