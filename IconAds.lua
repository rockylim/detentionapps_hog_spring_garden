module(..., package.seeall);

local _W = display.contentWidth;
local _H = display.contentHeight;
local _w = (_W * 0.5);
local _h = (_H * 0.5);
local icons = {};
---- CONFIG --------
local ICON_SIZE = 130;
local BORDER_SPACE = 15;
local POSITION = "left";
local USE_BG = true;
------ ****** ------
local ICON_ADS;
if (system.getInfo("platform") == "ios") then	
	ICON_ADS = 
	{
		{
	    	IMG = "assets/images/iconads/hony.png", 
		    URL = "https://apps.apple.com/us/app/hidden-object-new-york-journey/id1538623135",
		},    				
		{
			IMG = "assets/images/iconads/hoxmas.png", 
			URL = "https://apps.apple.com/us/app/hidden-object-christmas-puzzle/id1321575063",
		},
		{
			IMG = "assets/images/iconads/hohallow.png", 
			URL = "https://apps.apple.com/us/app/hidden-object-halloween-secret/id1287373411",
		},
		{
			IMG = "assets/images/iconads/photojourney.png", 
			URL = "https://apps.apple.com/us/app/hidden-objects-photo-journey/id1529208632",
		}
	}
else
	if (_G.isAmazon == true) then
		ICON_ADS = 
		{
			{
				IMG = "assets/images/iconads/ic_ho_halloween.png",
				URL = "https://www.amazon.com/Objects-Halloween-Mystery-Haunted-Differences/dp/B075THP1MW/",
			},  
			{
				IMG = "assets/images/iconads/ic_ho_christmas.png",
				URL = "https://www.amazon.com/Hidden-Object-Christmas-Celebration-Holiday/dp/B07856KTQF",
			},
			{
				IMG = "assets/images/iconads/ic_ho_ny.png",
				URL = "https://www.amazon.com/Hidden-Object-New-York-City/dp/B076GYCBYS/",
			},
			{
				IMG = "assets/images/iconads/ic_hog_roadtrip.png",
				URL = "https://www.amazon.com/Hidden-Object-Road-Trip-USA/dp/B078VHB24T/",
			}  
		}
	else
			ICON_ADS = 
	{  		
		{
			IMG = "assets/images/iconads/photojourney.png", 
			URL = "https://apps.apple.com/us/app/hidden-objects-photo-journey/id1529208632",
		},
		{
			IMG = "assets/images/iconads/worldtravel.png", 
			URL = "https://apps.apple.com/us/app/hidden-object-travel-quest-usa/id1259328096",
		},
		{
	    	IMG = "assets/images/iconads/hony.png", 
		    URL = "https://apps.apple.com/us/app/hidden-object-new-york-journey/id1538623135",
		},  
		{
			IMG = "assets/images/iconads/hoxmas.png", 
			URL = "https://apps.apple.com/us/app/hidden-object-christmas-puzzle/id1321575063",
		}  
	}
	end
end


local function Animate(icon)
	transition.scaleTo(icon, {tag="iconads", xScale=1.02, yScale=1.02, time=math.random(200,300), onComplete=function(object)
		transition.scaleTo(object, {tag="iconads",xScale=0.98, yScale=0.98, time=math.random(500,600), onComplete=function(object)		
			transition.scaleTo(object, {tag="iconads",xScale=1, yScale=1, time=math.random(200,300), onComplete=function(object)		
				Animate(object);
			end});
		end});
	end});
end


local function Animation() 
	local totalIcon = table.getn(ICON_ADS);
	for i = 1, totalIcon do
		Animate(icons[i]);
	end
end

local function OnTap(self, event)
	system.openURL( ICON_ADS[self.index].URL );
	return true;	
end


function Init(sceneView)
	local group = display.newGroup();
	sceneView:insert(group);
	
	local space;
	local totalIcon = table.getn(ICON_ADS);
	if POSITION == "top" or POSITION == "bottom" then
		space = (_W - (ICON_SIZE * totalIcon)) / (totalIcon + 1);
	else
		space = (_H - (ICON_SIZE * totalIcon)) / (totalIcon + 1);
	end
	
	if (USE_BG == true) then
		local x,y,width,height;
		if POSITION == "top" then			
			width = _W;
			height = ICON_SIZE + BORDER_SPACE * 2;
			x = _w;
			y = height * .5;
		elseif POSITION == "bottom" then
			width = _W;
			height = ICON_SIZE + BORDER_SPACE * 2;
			x = _w;
			y = _H - (height * .5);
		elseif POSITION == "right" then
			height = _H;
			width = ICON_SIZE + BORDER_SPACE * 2;
			x = _W - (width * .5);
			y = height * .5;
		elseif POSITION == "left" then
			height = _H;
			width = ICON_SIZE + BORDER_SPACE * 2;
			x = width * .5;
			y = height * .5;
		end
		local bg = display.newRect(group, x, y, width, height);	
		bg:setFillColor(0,0,0);
		bg.alpha = 0.4;		
	end
	
	local iconHalf = ICON_SIZE * .5;
	local movingPos = space;
	local i = 1;
  	for k, v in pairs(ICON_ADS) do
  		local icon = display.newGroup();
  		group:insert(icon);
		local image = display.newImageRect(icon, v.IMG, ICON_SIZE, ICON_SIZE);				
		image.x = 0; image.y = 0;
		image.tap = OnTap;
		image:addEventListener( "tap", image )
		image.index = i;
		if POSITION == "top" then
			icon.y = iconHalf + BORDER_SPACE;
			icon.x = movingPos + iconHalf;
		elseif POSITION == "bottom" then
			icon.y = (_H - iconHalf) - BORDER_SPACE;
			icon.x = movingPos + iconHalf;
		elseif POSITION == "right" then
			icon.x = (_W - iconHalf) - BORDER_SPACE;
			icon.y = movingPos + iconHalf;
		elseif POSITION == "left" then
			icon.x = iconHalf + BORDER_SPACE;
			icon.y = movingPos + iconHalf;						
		end
		movingPos = movingPos + ICON_SIZE + space;
		
		local adTextGroup = display.newGroup();
		icon:insert(adTextGroup);
		adTextGroup.x = iconHalf - 20;
		adTextGroup.y = -iconHalf + 10;		
		local textBG = display.newRoundedRect(adTextGroup, image.x, image.y, 50, 40, 10);	
		textBG.alpha = 0.6;
		textBG:setFillColor(0,0,0);
		local text = display.newText( "AD", 0, 0, native.systemFont, 25 );
		adTextGroup:insert(text);
		
		table.insert(icons, icon);
		
		i = i+1;
	end
	
	Animation();	
end

function Destroy()
	print("destroy");
	transition.cancel( "iconads" )
	local totalIcon = table.getn(ICON_ADS);
	for i = 1, totalIcon do
		display.remove(icons[i]);
		icons[i] = nil;		
	end
	icons = nil;
	icons = {};
end

